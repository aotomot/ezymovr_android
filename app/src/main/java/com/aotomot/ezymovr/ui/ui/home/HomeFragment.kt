package com.aotomot.ezymovr.ui.ui.home

import android.content.BroadcastReceiver
import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aotomot.ezymovr.AppConstants
import com.aotomot.ezymovr.CustomAlert.AlertDialog
import com.aotomot.ezymovr.CustomAlert.OnItemClickListener
import com.aotomot.ezymovr.FirebaseUtility.EzyMovrFirebaseService
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.adapters.CompletedJobsListAdapter
import com.aotomot.ezymovr.adapters.InProgressJobsListAdapter
import com.aotomot.ezymovr.adapters.UpcomingJobsListAdapter
import com.aotomot.ezymovr.adapters.WaitingJobsListAdapter
import com.aotomot.ezymovr.extFunctions.gone
import com.aotomot.ezymovr.extFunctions.visible
import com.aotomot.ezymovr.models.Jobs
import com.aotomot.ezymovr.models.PostLocationUpdate
import com.aotomot.ezymovr.ui.UserProfileActivity
import com.bumptech.glide.Glide
import com.google.android.gms.location.*
import com.google.android.material.bottomnavigation.BottomNavigationView
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class HomeFragment : Fragment() {

    private var upcomingJobs: MutableList<Jobs> = mutableListOf()
    private var completedJobs: MutableList<Jobs> = mutableListOf()
    private var inProgressJobs: MutableList<Jobs> = mutableListOf()
    private var waitingJobs: MutableList<Jobs> = mutableListOf()
    private var expiredJobs: MutableList<Jobs> = mutableListOf()
    private lateinit var inProgressListLayout: RecyclerView
    private lateinit var waitingListLayout: RecyclerView
    private lateinit var upcomingJobDesc: TextView
    private lateinit var completedJobDesc: TextView
    private lateinit var upcomingJobListSize: TextView
    private lateinit var completedJobListSize: TextView
    private lateinit var upcomingLayout: CardView
    private lateinit var completeLayout: CardView
    private lateinit var inProgressLayout: CardView
    private lateinit var UpcomingList: RecyclerView
    private lateinit var completedList: RecyclerView
    private lateinit var userImg: ImageView
    private lateinit var strDate: Date
    private lateinit var startLayout : ConstraintLayout
    private lateinit var stopLayout : ConstraintLayout
    private lateinit var pauseLayout : ConstraintLayout
    var sdf = SimpleDateFormat("dd/MM/yyyy HH:mm")
    private lateinit var homeViewModel: HomeViewModel
    private var loading :ProgressBar? = null
    private var startTime = 0L
    private val myHandler = Handler()
    var timeInMillies = 0L
    var timeSwap = 0L
    var finalTime = 0L
    private var is_paused = false
    var jobList : Jobs = Jobs()
    private lateinit var layoutManager : LinearLayoutManager
    private lateinit var mAdapter: UpcomingJobsListAdapter
    private lateinit var mAdapterCompleted: CompletedJobsListAdapter
    private lateinit var mAdapterInProgress: InProgressJobsListAdapter
    private lateinit var mAdapterWaiting: WaitingJobsListAdapter

    // FusedLocationProviderClient - Main class for receiving location updates.
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    // LocationRequest - Requirements for the location updates, i.e., how often you
// should receive updates, the priority, etc.
    private lateinit var locationRequest: LocationRequest

    // LocationCallback - Called when FusedLocationProviderClient has a new Location.
    private lateinit var locationCallback: LocationCallback

    // Used only for local storage of the last known location. Usually, this would be saved to your
// database, but because this is a simplified sample without a full database, we only need the
// last location to create a Notification if the user navigates away from the app.
    private var currentLocation: Location? = null

    var broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            Log.d("Firebase 1","New Firebase Token Received")
            AppConstants.mDataForDeviceRegistrationRequest.deviceToken = AppConstants.FirebaseToken
            homeViewModel.RegisterDevice()
        }
    }
    var broadcastReceiverNotification = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            Log.d("Firebase 8","New Notification Received")
            homeViewModel.fetchAlertsNotification()
        }
    }
    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager.getInstance(activity!!.applicationContext)
            .unregisterReceiver(broadcastReceiver)

        LocalBroadcastManager.getInstance(activity!!.applicationContext)
            .unregisterReceiver(broadcastReceiverNotification)

        val removeTask = fusedLocationProviderClient.removeLocationUpdates(locationCallback)

    }
    protected inline fun <VM : ViewModel> viewModelFactory(crossinline f: () -> VM) =
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(aClass: Class<T>):T = f() as T
        }

    override fun onResume() {
        super.onResume()
        homeViewModel.fetchjobsList()
        homeViewModel.fetchAlertsNotification()
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity!!)
        locationRequest = LocationRequest().apply {
            // Sets the desired interval for active location updates. This interval is inexact. You
            // may not receive updates at all if no location sources are available, or you may
            // receive them less frequently than requested. You may also receive updates more
            // frequently than requested if other applications are requesting location at a more
            // frequent interval.
            //
            // IMPORTANT NOTE: Apps running on Android 8.0 and higher devices (regardless of
            // targetSdkVersion) may receive updates less frequently than this interval when the app
            // is no longer in the foreground.
            interval = TimeUnit.SECONDS.toMillis(60)

            // Sets the fastest rate for active location updates. This interval is exact, and your
            // application will never receive updates more frequently than this value.
            fastestInterval = TimeUnit.SECONDS.toMillis(30)

            // Sets the maximum time when batched location updates are delivered. Updates may be
            // delivered sooner than this interval.
            maxWaitTime = TimeUnit.MINUTES.toMillis(2)

            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)

                if (locationResult?.lastLocation != null) {

                    // Normally, you want to save a new location to a database. We are simplifying
                    // things a bit and just saving it as a local variable, as we only need it again
                    // if a Notification is created (when user navigates away from app).
                    currentLocation = locationResult.lastLocation
                    val locUpdate = PostLocationUpdate(locationResult.lastLocation.latitude, locationResult.lastLocation.longitude)
                    homeViewModel.postLocation(locUpdate)

                } else {
                    Log.d(TAG, "Location information isn't available.")
                }
            }
        }

        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        homeViewModel = ViewModelProvider(this,viewModelFactory {
            HomeViewModel(
                activity!! as AppCompatActivity
            )
        }).get(HomeViewModel::class.java)

        val root = inflater.inflate(R.layout.fragment_home, container, false)
        LocalBroadcastManager.getInstance(activity!!.applicationContext).registerReceiver(broadcastReceiver, IntentFilter(EzyMovrFirebaseService.TOKEN_RECEIVED))
        LocalBroadcastManager.getInstance(activity!!.applicationContext).registerReceiver(broadcastReceiverNotification, IntentFilter(EzyMovrFirebaseService.MESSAGE_RECEIVED))
        loading = root.findViewById(R.id.loading)
        userImg = root.findViewById(R.id.user_img) as ImageView
        UpcomingList = root.findViewById(R.id.upcoming_list) as RecyclerView
        completedList = root.findViewById(R.id.completed_list) as RecyclerView
        inProgressListLayout = root.findViewById(R.id.in_progress_layout) as RecyclerView
        waitingListLayout = root.findViewById(R.id.waiting_layout) as RecyclerView
        /*inProgressLayout = root.findViewById(R.id.in_progress_layout) as CardView*/
        completeLayout = root.findViewById(R.id.completedlayout) as CardView
        upcomingLayout = root.findViewById(R.id.upcominglayout) as CardView
        upcomingJobListSize = root.findViewById(R.id.up_nos) as TextView
        completedJobListSize = root.findViewById(R.id.completed_nos) as TextView
        upcomingJobDesc = root.findViewById(R.id.up_desc) as TextView
        completedJobDesc = root.findViewById(R.id.completed_desc) as TextView
        root.findViewById<TextView>(R.id.name).text = "Hello ".plus(AppConstants.driver.firstName)
        if(!AppConstants.driver.driverImage.url.isBlank()){
            Glide.with(this).asBitmap().load(AppConstants.driver.driverImage.url).into(root.findViewById<ImageView>(R.id.user_img))
        }

        root.findViewById<ImageView>(R.id.user_img).setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                val intent = Intent(activity, UserProfileActivity::class.java)
                startActivity(intent)
            }

        })
        completedList.gone()
        loading?.visible()


        inProgressListLayout.layoutManager = LinearLayoutManager(this.context)
        mAdapterInProgress = InProgressJobsListAdapter(mutableListOf(),context,activity as AppCompatActivity,savedInstanceState,homeViewModel)
        inProgressListLayout.adapter = mAdapterInProgress

        waitingListLayout.layoutManager = LinearLayoutManager(this.context)
        mAdapterWaiting = WaitingJobsListAdapter(mutableListOf(),context,activity as AppCompatActivity,homeViewModel,savedInstanceState)
        waitingListLayout.adapter = mAdapterWaiting

        layoutManager = LinearLayoutManager(this.context)
        UpcomingList.layoutManager = layoutManager
        mAdapter = UpcomingJobsListAdapter(mutableListOf(),context,activity as AppCompatActivity,savedInstanceState,homeViewModel)
        UpcomingList.adapter = mAdapter

        completedList.layoutManager = LinearLayoutManager(this.context)
        mAdapterCompleted = CompletedJobsListAdapter(mutableListOf(),context,activity as AppCompatActivity,savedInstanceState)
        completedList.adapter = mAdapterCompleted

        homeViewModel.error.observe(viewLifecycleOwner, Observer {
            Log.d("","")
        })
        homeViewModel.completeJob.observe(viewLifecycleOwner, Observer {
            homeViewModel.fetchjobsList()
        })
        homeViewModel.completeJobResult.observe(viewLifecycleOwner, Observer {
           // loading.gone()
            AlertDialog.showDialogWithAlertHeaderSingleButton(activity,
                "Error",
                "Something went wrong. Try again.",
                object: OnItemClickListener {
                    override fun onItemClick(o: Any?, position: Int) {
                        if(position == 0){
                            //AlertDialog.
                        }
                    }

                })
        })
        homeViewModel.declineJob.observe(viewLifecycleOwner, Observer {
            homeViewModel.fetchjobsList()
        })
        homeViewModel.acceptJob.observe(viewLifecycleOwner, Observer {
            homeViewModel.fetchjobsList()
        })
        homeViewModel.pauseJob.observe(viewLifecycleOwner, Observer {
            homeViewModel.fetchjobsList()
        })
        homeViewModel.startJob.observe(viewLifecycleOwner, Observer {
            homeViewModel.fetchjobsList()
        })
         homeViewModel.jobList.observe(viewLifecycleOwner, Observer {
            //textView.text = it
             loading?.gone()
             AppConstants.JobsLists.clear()
             AppConstants.JobsLists.addAll(it.filter { jobs -> !jobs.declined }.toMutableList())
             Log.d("","")

             populateJobs(AppConstants.JobsLists)
             if(inProgressJobs.size > 0 ){
                 inProgressListLayout.visible()
             }
             if(waitingJobs.size > 0){
                 waitingListLayout.visible()
             }

             upcomingJobListSize.text = upcomingJobs.size.toString()
             upcomingJobDesc.text = "You have ".plus(upcomingJobs.size.toString()).plus(" Upcoming Jobs")

             completedJobListSize.text = completedJobs.size.toString()
             completedJobDesc.text = "You have ".plus(completedJobs.size.toString()).plus(" Completed Jobs")
             if(upcomingJobs.size == 0) {
                 completedJobListSize.text = completedJobs.size.toString()
                 completedJobDesc.text = "You have ".plus(completedJobs.size.toString()).plus(" Completed Jobs")
                 completedList.visible()
                 //UpcomingList.gone()
                 completeLayout.setCardBackgroundColor(activity?.getColor(R.color.selected)!!)
                 upcomingLayout.setCardBackgroundColor(Color.WHITE)
             }else{
                 UpcomingList.visible()
                 completedList.gone()
                 upcomingLayout.setCardBackgroundColor(activity?.getColor(R.color.selected)!!)
                 completeLayout.setCardBackgroundColor(Color.WHITE)
             }
        })
        homeViewModel.notifications_LiveData.observe(viewLifecycleOwner, Observer {
            val nav_view = activity!!.findViewById<BottomNavigationView>(R.id.nav_view)
            val menuItemId: Int = nav_view.getMenu().getItem(2).getItemId() //2 menu item index.

            val alertLists = it.filter { !it.status.equals("READ") }
            if(alertLists.size > 0) {
                val badgeDrawable = nav_view.getOrCreateBadge(menuItemId)
                badgeDrawable.setVisible(true)
                badgeDrawable.setNumber(alertLists.size)
            }

        })
        upcomingLayout.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                completedList.gone()
                UpcomingList.visible()
                upcomingLayout.setCardBackgroundColor(activity?.getColor(R.color.selected)!!)
                completeLayout.setCardBackgroundColor(Color.WHITE)
            }

        })

        completeLayout.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                UpcomingList.gone()
                completedList.visible()
                completeLayout.setCardBackgroundColor(activity?.getColor(R.color.selected)!!)
                upcomingLayout.setCardBackgroundColor(Color.WHITE)
            }

        })

        return root
    }

    private val updateTimerMethod: Runnable = object : Runnable {
        override fun run() {
            timeInMillies = SystemClock.uptimeMillis() - startTime
            finalTime = timeSwap + timeInMillies
            /**
             * display from backend to the timer
             */
            val seconds = (finalTime / 1000).toInt() % 60
            val minutes = (finalTime / (1000 * 60) % 60).toInt()
            val hours = (finalTime / (1000 * 60 * 60)).toInt()
            /*in_progresss_timer?.text = (String.format("%02d", hours)
                .plus(":")
                .plus(String.format("%02d", minutes))
                .plus(":" )
                .plus( String.format("%02d", seconds))
            )*/
            myHandler.postDelayed(this, 0)
        }
    }
    private fun populateInProgres(jobLists : MutableList<Jobs>): Int {
        val inprogress = jobLists.filter { jobs -> jobs.startDateActual != 0L && !jobs.completed }
        /*if(inprogress.isEmpty())
            return 0

        this.jobList = inprogress.get(0)
        Glide.with(this)
            .asBitmap()
            .load(jobList.driverImageUrl)
            .into(userImg)

        name?.setText("Hello " + jobList.driver)

        if (jobList.endDate != null) {
            try {
                strDate = sdf.parse(AppConstants.Epoch2DateStringTimesss(jobList.endDate.toString()))
            } catch (e: ParseException) {
                e.printStackTrace()
            }
        }

        job_number?.text = "#".plus(jobList.id.toString())
        in_progresss_timer?.setText(jobList.timer.timer)
        if (jobList.timer.running.equals("true")) {
            job_status_text?.text = "in-progress"
            val times = jobList.timer.timer.split(":")

            *//**CONVERT TIME TO SECOND *//*
            val second =
                times[0].toInt() * 60 * 60 + times[1].toInt() * 60 + times[2]
                    .toInt()

            *//**CONVERT TIME TO millisecond *//*
            val millisecond = second * 1000
            startTime = SystemClock.uptimeMillis() - millisecond
            myHandler.postDelayed(updateTimerMethod, 0)
            startLayout.visibility = View.GONE
            pauseLayout.visibility = View.VISIBLE
            stopLayout.visibility = View.VISIBLE

        } else {
            job_status_text?.text = "Break"
            val times = jobList.timer.timer.split(":")
            val mseconds = times[2].toInt()
            val mminutes = times[1].toInt()
            val mhours = times[0].toInt()
            in_progresss_timer?.text = (String.format("%02d", mhours)
                .plus(":")
                .plus(String.format("%02d", mminutes))
                .plus(":" )
                .plus( String.format("%02d", mseconds))
            )
            startLayout?.visibility = View.VISIBLE
            pauseLayout.visibility = View.GONE
            stopLayout?.visibility = View.VISIBLE
        }*/

        return inprogress.size

    }

    private fun populateJobs(jobLists : MutableList<Jobs>) {
        //val upcoming = jobLists.filter { jobs -> jobs.startDateActual == 0L && !jobs.completed && jobs.endDate == 0L }.toMutableList()

        upcomingJobs.clear()
        waitingJobs.clear()
        completedJobs.clear()
        inProgressJobs.clear()
        expiredJobs.clear()

        for (position in 0..jobLists.size.minus(1)) {

            var strDate: Date? = null
            val sdf = SimpleDateFormat(context!!.getString(R.string.sdf_string))
            val joinDatesdf = SimpleDateFormat(context!!.getString(R.string.start_date_sdf))
            try {
                strDate =
                    sdf.parse(AppConstants.Epoch2DateStringTimesss(jobLists?.get(position)?.endDate.toString())!!)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            if (jobLists?.get(position)!!.startDateActual.compareTo(0).equals(0)
                && !jobList.completed
                && strDate != null
                && Date().before(strDate) && !jobLists?.get(position)!!.accepted) {
                //waiting
                upcomingJobs.add(jobLists?.get(position))
            } else if (jobLists?.get(position)!!.completed) {
                //complete
                completedJobs.add(jobLists?.get(position))
            } else if (!jobLists?.get(position)!!.startDateActual.compareTo(0).equals(0)) {
                //in-progress
                inProgressJobs.add(jobLists?.get(position))
            } else {
                if (jobLists?.get(position)?.endDate != null && !jobLists?.get(position)!!.endDate.compareTo(
                        0
                    ).equals(0) && Date().after(strDate)
                ) {
                    //expired
                    expiredJobs.add(jobLists?.get(position))
                    completedJobs.add(jobLists?.get(position))
                } else {
                    //upcoming
                    upcomingJobs.add(jobLists?.get(position))
                }
            }
        }
        mAdapterCompleted.updateData(completedJobs)
        mAdapter.updateData(upcomingJobs)
        mAdapterInProgress.updateData(inProgressJobs)
        mAdapterWaiting.updateData(waitingJobs)
    }

    fun populateCompleted(jobLists : MutableList<Jobs>): Int {
        val completed = jobLists.filter { jobs -> jobs.completed }.toMutableList()
        mAdapterCompleted.updateData(completed)
        return completed.size
    }

}
