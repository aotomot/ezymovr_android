package com.aotomot.ezymovr.ui.ui.notifications

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.work.ListenableWorker
import com.aotomot.ezymovr.Repository.CommonRepository
import com.aotomot.ezymovr.WebService.WebServiceAPIFactory
import com.aotomot.ezymovr.extFunctions.domain
import com.aotomot.ezymovr.extFunctions.getAppSharedPrefs
import com.aotomot.ezymovr.extFunctions.userID
import com.aotomot.ezymovr.models.AlertNotification
import kotlinx.coroutines.launch
import okhttp3.ResponseBody

class NotificationsViewModel(private val mParentActivity : AppCompatActivity) : ViewModel() {

    private val repository : CommonRepository =
        CommonRepository(
            WebServiceAPIFactory.webServicebApi,
            mParentActivity,mParentActivity.getAppSharedPrefs().domain()
        )

   private val _notifications_LiveData = MutableLiveData<MutableList<AlertNotification>>()
    val notifications_LiveData : LiveData<MutableList<AlertNotification>>
        get() = _notifications_LiveData

    private val _markAsRead_LiveData = MutableLiveData<ResponseBody>()
    val markAsRead_LiveData : LiveData<ResponseBody>
        get() = _markAsRead_LiveData

    val _error = MutableLiveData<String>()
    val error :LiveData<String>
        get() = _error
    /*init {
        fetchAlertsNotification()
    }*/

    fun fetchAlertsNotification(){
         viewModelScope.launch {
            val response = repository.getNotificationByDevice(mParentActivity.getAppSharedPrefs().userID().toString())
            when (response) {
                is com.aotomot.ezymovr.WebService.Result.Success<*> -> _notifications_LiveData.postValue(
                    (response.data as MutableList<AlertNotification>)
                )
                is com.aotomot.ezymovr.WebService.Result.Error -> _error.postValue(response.exception.toString())
            }
        }
    }

    fun markAsRead(notificationID : String){

        viewModelScope.launch {
            val response = repository.markRead(mParentActivity.getAppSharedPrefs().userID().toString(), notificationID)
            when (response) {
                is com.aotomot.ezymovr.WebService.Result.Success<*> -> _markAsRead_LiveData.postValue(
                    (response.data as ResponseBody)
                )
                is com.aotomot.ezymovr.WebService.Result.Error -> _error.postValue(response.exception.toString())
            }
        }
    }
}