package com.aotomot.ezymovr.ui

import android.animation.AnimatorSet
import android.animation.ValueAnimator
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.adapters.onBoardingSlider
import com.aotomot.ezymovr.extFunctions.getAppSharedPrefs
import com.aotomot.ezymovr.extFunctions.gone
import com.aotomot.ezymovr.extFunctions.isonBoardingShown
import com.aotomot.ezymovr.extFunctions.visible
import com.aotomot.ezymovr.ui.login.LoginActivity
import com.aotomot.ezymovr.viewModels.OnBoardingViewModel
import com.google.android.material.button.MaterialButton
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_on_boarding.*

class OnBoardingActivity : AppCompatActivity() {

    private var mTotalOnBoardingPages: Int = 0
    private lateinit var mViewModel: OnBoardingViewModel
    private lateinit var mAdapter: onBoardingSlider
    protected inline fun <VM : ViewModel> viewModelFactory(crossinline f: () -> VM) =
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(aClass: Class<T>):T = f() as T
        }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_on_boarding)
        this.getAppSharedPrefs().isonBoardingShown(true)
        mViewModel = ViewModelProvider(this,viewModelFactory {
            OnBoardingViewModel(
                this
            )
        }).get(OnBoardingViewModel::class.java)

        /*if (!BuildConfig.DEBUG)
            Fabric.with(this, Crashlytics())
*/

        val viewPager = findViewById<View>(R.id.viewPager) as ViewPager
        val getStarted = findViewById<View>(R.id.get_started) as MaterialButton
       // val skipText = findViewById<View>(R.id.skiptext) as TextView

        mAdapter = onBoardingSlider(this, mutableListOf())
        viewPager.adapter =mAdapter

        val tabLayout = findViewById(R.id.tab_layout) as TabLayout
        tabLayout.setupWithViewPager(viewPager, true)

        mViewModel.onBoarding_LiveData.observe(this, Observer {
            mTotalOnBoardingPages = it.size
            it.sortByDescending { it.listOrder }
            it.reverse()
            mAdapter.updateData(it)
            onboarding_progressBar?.gone()
        })
        mViewModel.error.observe(this, Observer {
            onboarding_progressBar?.gone()
        })
        getStarted.setOnClickListener(object : View.OnClickListener{
            /**
             * Called when a view has been clicked.
             *
             * @param v The view that was clicked.
             */
            override fun onClick(v: View?) {
               // mViewModel.RegisterDevice()
                val intent = Intent(this@OnBoardingActivity, LoginActivity::class.java)
                startActivity(intent)
                finishAffinity()
            }

        })
        /*skipText.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                val intent = Intent(this@OnBoardingActivity, LoginActivity::class.java)
                startActivity(intent)
                finishAffinity()
            }})*/

        viewPager?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }
            override fun onPageSelected(position: Int) {
                if(position === mTotalOnBoardingPages.minus(1)) {
                    getStarted.visible()
                    easeIn(getStarted, 50f, 300)

                    tabLayout.gone()
                    /*skipText.setText(getString(R.string.onboarding_get_started))
                    skipText.setTypeface(Typeface.SANS_SERIF, Typeface.BOLD)*/

                }else{
                    getStarted.gone()
                    tabLayout.visible()
                    /*skipText.setText(getString(R.string.onboarding_skip))
                    skipText.setTypeface(Typeface.SANS_SERIF, Typeface.NORMAL)
                    skipText.gone()*/
                }

            }

        })
    }

    private fun easeIn(view: View, fromY: Float, duration: Long) {
        val animatorSet = AnimatorSet()
        val valueAnimatorY = ValueAnimator.ofFloat(fromY, 0f, 0f)
        valueAnimatorY.addUpdateListener { animation ->
            view.translationY = animation.animatedValue as Float
        }
        animatorSet.playTogether(valueAnimatorY)
        animatorSet.duration = duration
        animatorSet.start()
    }
}
