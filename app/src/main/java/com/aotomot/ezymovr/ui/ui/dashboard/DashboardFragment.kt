package com.aotomot.ezymovr.ui.ui.dashboard

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aotomot.ezymovr.AppConstants
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.adapters.AlertNotificationListAdapter
import com.aotomot.ezymovr.adapters.CalenderListAdapter
import com.aotomot.ezymovr.extFunctions.gone
import com.aotomot.ezymovr.extFunctions.visible
import com.aotomot.ezymovr.models.Jobs
import com.aotomot.ezymovr.ui.login.LoginActivity
import com.applandeo.materialcalendarview.CalendarView
import com.applandeo.materialcalendarview.EventDay
import com.applandeo.materialcalendarview.listeners.OnCalendarPageChangeListener
import kotlinx.android.synthetic.main.fragment_dashboard.*
import kotlinx.android.synthetic.main.fragment_notifications.*
import kotlinx.android.synthetic.main.fragment_notifications.loading_ui
import kotlinx.coroutines.Job
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class DashboardFragment : Fragment(){

    private lateinit var emptyListLayout: ConstraintLayout
    private lateinit var calView: CalendarView
    private lateinit var dashboardViewModel: DashboardViewModel
    private lateinit var calenderList: RecyclerView
    private lateinit var layoutManager : LinearLayoutManager
    private lateinit var mAdapter: CalenderListAdapter
    private var currentMonth = 0

    protected inline fun <VM : ViewModel> viewModelFactory(crossinline f: () -> VM) =
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(aClass: Class<T>):T = f() as T
        }

    override fun onResume() {
        super.onResume()
        dashboardViewModel.fetchjobsList()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        dashboardViewModel = ViewModelProvider(this,viewModelFactory {
            DashboardViewModel(activity!! as AppCompatActivity)
        }).get(DashboardViewModel::class.java)

        val root = inflater.inflate(R.layout.fragment_dashboard, container, false)
        emptyListLayout = root.findViewById<ConstraintLayout>(R.id.empty_list_layut) as ConstraintLayout
        calView = root.findViewById<CalendarView>(R.id.calendarView) as CalendarView
        val today = Calendar.getInstance()
        calView.setDate(today)

        layoutManager = LinearLayoutManager(this.context)
        mAdapter = CalenderListAdapter(activity as AppCompatActivity, mutableListOf())
        calenderList = root.findViewById<RecyclerView>(R.id.calender_list)
        calenderList.layoutManager = layoutManager
        calenderList.adapter = mAdapter

        dashboardViewModel.jobList.observe(viewLifecycleOwner, Observer {
            //textView.text = it
            if (loading_ui?.visibility == View.VISIBLE)
                loading_ui?.visibility = View.GONE

            AppConstants.JobsLists.clear()
            AppConstants.JobsLists.addAll(it.filter { jobs -> !jobs.declined }.toMutableList())
            Log.d("","")
            calView.selectedDates = getSelectedDays(AppConstants.JobsLists)!!

            currentMonth = (Calendar.getInstance()).get(Calendar.MONTH)
            updateCalenderList(setCurrentMonthList(calView.currentPageDate.get(Calendar.MONTH),AppConstants.JobsLists))

        })

       /* val dtlong = 1585504327000
       // val dt = SimpleDateFormat("dd/mm/yyyy").format(Date(dtlong)).
        val later = Calendar.getInstance()
        later.time = Date(dtlong)
        val calendars = ArrayList<Calendar>()
        calendars.add(later)
        calView.selectedDates = calendars*/

        calView.setOnForwardPageChangeListener(object : OnCalendarPageChangeListener{
            override fun onChange() {
                Log.d("","")
                updateCalenderList(setCurrentMonthList(calView.currentPageDate.get(Calendar.MONTH),AppConstants.JobsLists))
            }
        })

        calView.setOnPreviousPageChangeListener(object : OnCalendarPageChangeListener{
            override fun onChange() {
                Log.d("","")
                updateCalenderList(setCurrentMonthList(calView.currentPageDate.get(Calendar.MONTH),AppConstants.JobsLists))
            }
        })
        //calView.setSwipeEnabled(false)
        /*val events: MutableList<EventDay> = ArrayList()

        val calendar = Calendar.getInstance()
        events.add(EventDay(calendar, R.drawable.ic_cal_expired, Color.parseColor("#228B22")))
        events.add(EventDay(calendar, R.drawable.ic_cal_progess, Color.parseColor("#228B22")))

        calView.setEvents(events)*/
        return root
    }

    private fun updateCalenderList(lists : MutableList<Jobs>){
        if(lists.isEmpty()){
            calenderList.gone()
            emptyListLayout.visible()
        }else{
            emptyListLayout.gone()
            calenderList.visible()
            mAdapter.update(lists)
        }
    }
    private fun setCurrentMonthList(month :Int,lists : MutableList<Jobs>) : MutableList<Jobs>{
        var updatedLists = mutableListOf<Jobs>()
        for(position in 0..lists.size.minus(1)) {
            val date = Calendar.getInstance()
            val dt = Date(lists?.get(position)?.startDate)
            date.time = dt

            if (date.get(Calendar.MONTH) == month) {
                updatedLists.add(lists.get(position))
            }
        }
        return updatedLists
    }
    private fun getEventDays(lists : MutableList<Jobs>): MutableList<EventDay> {
        val calendars = mutableListOf<Calendar>()
        val events: MutableList<EventDay> = ArrayList()
        for (i in 0..lists.size.minus(1)) {
            val calendar = Calendar.getInstance()
            val dt = Date(lists.get(i).startDate)
            calendar.time = dt
            //calendar.add(Calendar.DAY_OF_MONTH, i)
            //calendars.add(calendar)
            events.add(EventDay(calendar, R.drawable.ic_cal_expired, Color.parseColor("#228B22")))
        }
        return events
    }

    private fun getSelectedDays(lists : MutableList<Jobs>): List<Calendar>? {
        val calendars = mutableListOf<Calendar>()
        for (i in 0..lists.size.minus(1)) {
            val calendar = Calendar.getInstance()
            val dt = Date(lists.get(i).startDate)
            calendar.time = dt
            val month = calendar.get(Calendar.MONTH)
            calendars.add(calendar)
        }
        return calendars
    }



}
