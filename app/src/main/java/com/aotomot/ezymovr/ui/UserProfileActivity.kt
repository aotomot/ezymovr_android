package com.aotomot.ezymovr.ui

import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.system.Os.close
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.cardview.widget.CardView
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.aotomot.ezymovr.AppConstants
import com.aotomot.ezymovr.BuildConfig
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.adapters.UserProfileViewModel
import com.aotomot.ezymovr.extFunctions.getAppSharedPrefs
import com.aotomot.ezymovr.extFunctions.password
import com.aotomot.ezymovr.extFunctions.username
import com.aotomot.ezymovr.models.Driver
import com.aotomot.ezymovr.ui.login.LoginActivity
import com.bumptech.glide.Glide
import com.google.android.material.button.MaterialButton
import io.github.inflationx.viewpump.ViewPumpContextWrapper

class UserProfileActivity : AppCompatActivity() {
    var nameTitle: TextView? = null
    var change_pass_layout: RelativeLayout? = null
    var email: TextView? = null
    var user_img: ImageView? = null
    var phoneNumber: TextView? = null
    var driverData: Driver? = null

    private lateinit var mViewModel: UserProfileViewModel
    protected inline fun <VM : ViewModel> viewModelFactory(crossinline f: () -> VM) =
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(aClass: Class<T>):T = f() as T
        }

   override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        nameTitle = findViewById(R.id.nameTitle)
        phoneNumber = findViewById(R.id.phoneNumber)
        change_pass_layout = findViewById(R.id.change_pass_layout)
        email = findViewById(R.id.email)
        user_img = findViewById(R.id.user_img)
        user_img!!.setOnClickListener(object :View.OnClickListener{
           override fun onClick(v: View?) {
               onCardviewProfileClicked()
           }

        })
       findViewById<CardView>(R.id.changePasswordCard).setOnClickListener(object :View.OnClickListener{
           override fun onClick(v: View?) {
               changePasswordCardClicked()
           }
       })
       findViewById<CardView>(R.id.termAndConditionCard).setOnClickListener(object :View.OnClickListener{
           override fun onClick(v: View?) {
               onTAndCClicked()
           }
       })
       findViewById<CardView>(R.id.appFeedbackCard).setOnClickListener(object :View.OnClickListener{
           override fun onClick(v: View?) {
               onappFeedbackClicked()
           }

       })
       findViewById<MaterialButton>(R.id.logout).setOnClickListener(object :View.OnClickListener{
           override fun onClick(v: View?) {
               onLogoutClicked()
           }

       })
        setSupportActionBar(toolbar)
        getSupportActionBar()?.setDisplayHomeAsUpEnabled(false)
        getSupportActionBar()?.setDisplayShowTitleEnabled(false)
        driverData = AppConstants.driver
       setUI()
       mViewModel = ViewModelProvider(this, viewModelFactory {
           UserProfileViewModel(this)
       }).get(UserProfileViewModel::class.java)
        //AppConstant.TrackingFragmentOrActivity(AnalyticsConstant.profileScreen)
        /*NetworkManager.getInstance().getDriverByUserId(
            ModelManager.getInstance().getLogin().getUserId(),
            ModelManager.getInstance().getLogin().getAuthorization()
        )*/
    }

    fun onCardviewProfileClicked() {
        startActivity(Intent(this, EditUserProfileActivity::class.java))
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
    }

    fun changePasswordCardClicked() {
        val intent = Intent(this, ChangePasswordActivity::class.java)
        val options: ActivityOptions = ActivityOptions
            .makeSceneTransitionAnimation(this, change_pass_layout, "robot")
        // start the new activity
        //startActivity(intent, options.toBundle());
        startActivity(intent)
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
    }

    fun onTAndCClicked() {
        //startActivity(new Intent(this, UserProfileTermAndConditionActivity.class));
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse("https://www.ezymovr.com") // only used based on your example.
        val title = "Please Select a browser to open"
        val chooser = Intent.createChooser(intent, title)
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(chooser)
        }
    }

    /*@OnClick(R.id.alert_icon)
    public void onAlertIconClicked() {
        startActivity(new Intent(this, NotificationActivity.class));
    }*/
    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == 1) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        /*val editItem = menu.findItem(1)
        if (editItem == null) {
            menu.add(0, 1, 0, "")
                .setIcon(R.drawable.close)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS)
        }*/
        return super.onPrepareOptionsMenu(menu)
    }

    fun onLogoutClicked() {
        //ModelManager.getInstance().removeUserLoginData()
        getAppSharedPrefs().password("")
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    fun setUI() {
        nameTitle?.setText(driverData?.firstName + " " + driverData?.lastName)
        email?.setText(driverData?.email)
        /*DriverContact dContact  = ModelManager.getInstance().getDriverContactById(driverData.getContactId().toString());
        phoneNumber.setText(dContact.getContactNumber());*/
        // toolbar_title.setText("Hello, \n" + driverData.getFirstName() + " " + driverData.getLastName());
        /*if (ModelManager.getInstance().getDriverImage() != null && !ModelManager.getInstance()
                .getDriverImage().getUrl().equals("")
        ) {
            Picasso.get().load(ModelManager.getInstance().getDriverImage().getUrl())
                .into(imageListItem)
        }*/
        if(!AppConstants.driver.driverImage.url.isBlank()){
            Glide.with(this).asBitmap().load(AppConstants.driver.driverImage.url).into(findViewById(R.id.user_img))
        }
        /*if (ModelManager.getInstance().getDriverImage() != null && !ModelManager.getInstance().getDriverImage().getUrl().equals("")) {
            Picasso.get().load(ModelManager.getInstance().getDriverImage().getUrl()).into(userIcon);
        }*/
    }

    val deviceName: String
        get() {
            val manufacturer = Build.MANUFACTURER
            val model = Build.MODEL
            return if (model.startsWith(manufacturer)) {
                model
            } else {
                "$manufacturer $model"
            }
        }

    fun composeEmail(addresses: String, subject: String?, body: String?, context: Context) {
        val intent = Intent(Intent.ACTION_SENDTO)
        intent.data = Uri.parse("mailto:$addresses") // only email apps should handle this
        intent.putExtra(Intent.EXTRA_TEXT, body)
        intent.putExtra(Intent.EXTRA_SUBJECT, subject)
        if (intent.resolveActivity(context.packageManager) != null) {
            context.startActivity(intent)
        }
    }

   fun onappFeedbackClicked() {
        var version: String? = null
        try {
            val pInfo: PackageInfo = getPackageManager().getPackageInfo(getPackageName(), 0)
            version = pInfo.versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        val Appname = "App Name : " + getString(R.string.app_name)
        val AppVersion = "App Version : $version"
        val OsVersion = "OS Version : " + Build.VERSION.SDK_INT
        val DeviceName = "Device Name : $deviceName"
        val Domain =
            "Domain : " + AppConstants.DOMAIN_NAME
        val SendFrom = "Send From my Android"
        composeEmail(
            "tech@ezymovr.com",
            "App Feedback (" + Domain.replace(".ezymovr.com", "").replace("https://", "") + ")",
            """
                $Appname
                $AppVersion
                $OsVersion
                $DeviceName

                $SendFrom

                $Domain
                """.trimIndent(),
            this
        )
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(base!!))
    }

   override fun onPause() {
        super.onPause()
      //  EventBus.getDefault().unregister(this)
    }

    override fun onStop() {
        super.onStop()
      //  EventBus.getDefault().unregister(this)
    }

    override fun onResume() {
        super.onResume()
        /*NetworkManager.getInstance().getDriverByUserId(
            ModelManager.getInstance().getLogin().getUserId(),
            ModelManager.getInstance().getLogin().getAuthorization()
        )*/
    }


}