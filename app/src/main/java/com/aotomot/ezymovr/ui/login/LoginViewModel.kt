package com.aotomot.ezymovr.ui.login

import android.util.Log
import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aotomot.ezymovr.AppConstants
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.Repository.CommonRepository
import com.aotomot.ezymovr.WebService.WebServiceAPIFactory
import com.aotomot.ezymovr.models.ChangePassword
import com.aotomot.ezymovr.models.ChangePasswordModel
import com.aotomot.ezymovr.models.Driver
import com.aotomot.ezymovr.models.LoginResponse
import kotlinx.coroutines.launch
import java.util.regex.Matcher
import java.util.regex.Pattern


class LoginViewModel(private val loginRepository: CommonRepository,private val driverRepository: CommonRepository) : ViewModel() {

    lateinit var driverRepository1: CommonRepository
    lateinit var loginRepository1: CommonRepository

    private val _loginForm = MutableLiveData<LoginFormState>()
    val loginFormState: LiveData<LoginFormState> = _loginForm

    private val _loginResult = MutableLiveData<LoginResponse>()
    val loginResult: LiveData<LoginResponse> = _loginResult

    private val _loginResultError = MutableLiveData<String>()
    val loginResultError: LiveData<String> = _loginResultError

    private val _driverResult = MutableLiveData<Driver>()
    val driverResult: LiveData<Driver> = _driverResult

    private val _driverResultError = MutableLiveData<String>()
    val driverResultError: LiveData<String> = _driverResultError

    private val _updateResult = MutableLiveData<ChangePassword>()
    val updateResult : LiveData<ChangePassword>
        get() = _updateResult

    private val _updateResultError = MutableLiveData<String>()
    val updateResultError: LiveData<String> = _updateResultError

    fun login(companyName: String, username: String, password: String) {
        // can be launched in a separate asynchronous job
        AppConstants.DOMAIN_NAME = companyName
        driverRepository1 =  CommonRepository(WebServiceAPIFactory.webServiceApi_driver, null, companyName)
        loginRepository1 =  CommonRepository(WebServiceAPIFactory.webServicebApi, null, companyName)
        viewModelScope.launch {
            val response = loginRepository1.loginFromWeb(username, password)

            when (response) {
                is com.aotomot.ezymovr.WebService.Result.Success<*> -> _loginResult.postValue(response.data as LoginResponse)
                is com.aotomot.ezymovr.WebService.Result.Error -> _loginResultError.postValue(response.exception.toString())
            }

        }
    }

    fun fetchDriverDatails(userId : String) {
        // can be launched in a separate asynchronous job
        viewModelScope.launch {

            val response = driverRepository1.getDriverByUserID(userId)

            when (response) {
                is com.aotomot.ezymovr.WebService.Result.Success<*> -> _driverResult.postValue(response.data as Driver)
                is com.aotomot.ezymovr.WebService.Result.Error -> _driverResultError.postValue(response.exception.toString())
            }

        }
    }

    fun loginDataChanged(username: String, password: String) {
        if (!isUserNameValid(username)) {
            _loginForm.value = LoginFormState(usernameError = R.string.invalid_username)
        } else {
            //_loginForm.value = LoginFormState(isDataValid = true)
            _loginForm.value = LoginFormState(usernameError = null)
        }
    }

    fun loginDataChangedPass(password: String) {
        if (!isPasswordValid(password)) {
            _loginForm.value = LoginFormState(passwordError = R.string.invalid_password)
        } else {
            _loginForm.value = LoginFormState(isDataValid = true)
        }
    }

    fun loginDataNewChangedPass(password: String) {
        if (!isNewPasswordValid(password)) {
            _loginForm.value = LoginFormState(passwordError = R.string.invalid_password)
        } else {
            _loginForm.value = LoginFormState(isDataValid = true)
        }
    }

    fun loginDataChangedComapnyName(companyName: String) {
        if (!isCompanyNameValid(companyName)) {
            _loginForm.value = LoginFormState(companyNameError = R.string.invalid_company_name)
        } /*else {
            _loginForm.value = LoginFormState(isDataValid = true)
        }*/
    }

    // A placeholder username validation check
    private fun isUserNameValid(username: String): Boolean {
        return if (username.contains('@')) {
            Patterns.EMAIL_ADDRESS.matcher(username).matches()
        } else {
            username.isNotBlank()
        }
    }

    // A placeholder password validation check
    private fun isPasswordValid(password: String): Boolean {
        return password.isNotBlank()
    }

    private fun isNewPasswordValid(password: String): Boolean {
        return !(password.isNullOrBlank() || !isValidPassword(password))
    }
    fun isValidPassword(password: String?): Boolean {
        val pattern: Pattern
        val matcher: Matcher

        /**
         * REGEX WITH 1 CAPITAL 1 NUMBER AND 1 UNIX SYMBOL SUCH AS "~!@#$%*()`^&+="
         */
        val PASSWORD_PATTERN =
            "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[~!@#$%*()`^&+=])(?=\\S+$).{4,}$"
        /**
         * REGEX WITH 1 CAPITAL 1 NUMBER
         */
//        final String PASSWORD_PATTERN = "^.*(?=.{4,10})(?=.*\\d)(?=.*[a-zA-Z]).*$";
        pattern = Pattern.compile(PASSWORD_PATTERN)
        matcher = pattern.matcher(password)
        return matcher.matches()
    }

    private fun isCompanyNameValid(companyName: String): Boolean {
        return companyName.isNotBlank()
    }
    fun UpdatePassword(newPass: ChangePasswordModel, userID :String,companyName: String){

        viewModelScope.launch {
            loginRepository1 =  CommonRepository(WebServiceAPIFactory.webServicebApi, null,companyName )
            val response = loginRepository1.UpdatePassword(newPass,userID)
            when (response) {
                is com.aotomot.ezymovr.WebService.Result.Success<*> -> _updateResult.postValue(response.data as ChangePassword)
                is com.aotomot.ezymovr.WebService.Result.Error -> _updateResultError.postValue(response.exception.toString())
            }
            Log.d("","")
        }
    }
}
