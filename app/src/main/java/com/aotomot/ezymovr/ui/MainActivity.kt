package com.aotomot.ezymovr.ui

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.SystemClock
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.aotomot.ezymovr.CustomAlert.AlertDialog
import com.aotomot.ezymovr.CustomAlert.OnItemClickListener
import com.aotomot.ezymovr.FirebaseUtility.EzyMovrFirebaseService.Companion.MESSAGE_RECEIVED
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.extFunctions.visible
import com.google.android.material.bottomnavigation.BottomNavigationView


class MainActivity : AppCompatActivity() {

    var broadcastReceiver = object :BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            AlertDialog.showDialogWithoutAlertHeaderSingleButton(this@MainActivity,intent.getStringExtra("message"),
                object : OnItemClickListener {
                override fun onItemClick(o: Any?, position: Int) {
                    if (position == 0) {
                    } else {
                    }
                }
            })
        }
    }
    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager.getInstance(this)
            .unregisterReceiver(broadcastReceiver)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        navView.setupWithNavController(findNavController(R.id.nav_host_fragment))
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, IntentFilter(MESSAGE_RECEIVED))
    }


}
