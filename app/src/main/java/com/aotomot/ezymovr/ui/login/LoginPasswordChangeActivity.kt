package com.aotomot.ezymovr.ui.login

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aotomot.ezymovr.AppConstants
import com.aotomot.ezymovr.CustomAlert.OnItemClickListener
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.extFunctions.*
import com.aotomot.ezymovr.models.ChangePasswordModel
import com.aotomot.ezymovr.ui.MainActivity
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputLayout


class LoginPasswordChangeActivity : AppCompatActivity() {

    private lateinit var loginViewModel: LoginViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_change_password)
        val oldPassword = findViewById(R.id.oldPassword) as TextInputLayout
        val newPassword = findViewById(R.id.newPassword) as TextInputLayout
        val update = findViewById(R.id.get_started) as MaterialButton
        val loading = findViewById<ProgressBar>(R.id.loading)
        update.isEnabled = false
        loginViewModel = ViewModelProviders.of(this, LoginViewModelFactory(this))
            .get(LoginViewModel::class.java)

        loginViewModel.loginFormState.observe(this, Observer {
            val loginState = it ?: return@Observer

            // disable login button unless both username / password is valid
            update.isEnabled = loginState.isDataValid

            if (loginState.usernameError != null) {
                oldPassword.error = getString(loginState.usernameError)
            }else
                oldPassword.error = null

            if (loginState.passwordError != null) {
                newPassword.error = getString(loginState.passwordError)
            }
        })
        loginViewModel.updateResult.observe(this, Observer {
            loginViewModel.login(getAppSharedPrefs().domain(),getAppSharedPrefs().username(), newPassword.editText?.text.toString())
        })
        loginViewModel.updateResultError.observe(this, Observer {
            com.aotomot.ezymovr.CustomAlert.AlertDialog.showDialogWithAlertHeaderSingleButton(this, "Update Failed", "Please try again later", object :
                OnItemClickListener {
                override fun onItemClick(o: Any?, position: Int) {
                    getAppSharedPrefs().password(oldPassword.editText?.text.toString())
                    finish();
                }
            })
        })
        loginViewModel.loginResult.observe(this, Observer {
            getAppSharedPrefs().userID(it.userId)
            getAppSharedPrefs().AuthToken(it.authorization)
            //getAppSharedPrefs().username(username.editText?.text.toString())
            getAppSharedPrefs().password(newPassword.editText?.text.toString())
            //getAppSharedPrefs().domain(companyName.editText?.text.toString())
            AppConstants.Authorization = it.authorization
            loginViewModel.fetchDriverDatails(it.userId.toString())
        })
        loginViewModel.loginResultError.observe(this, Observer {
            loading.gone()
            update.isEnabled = true
            oldPassword.isEnabled = true
            newPassword.isEnabled = true
            showLoginFailed()
        })
        loginViewModel.driverResult.observe(this, Observer {
            loading.gone()
            setResult(Activity.RESULT_OK)
            AppConstants.driver = it
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            //Complete and destroy login activity once successful
            finish()
        })
        loginViewModel.driverResultError.observe(this, Observer {
            loading.gone()
            update.isEnabled = true
            showLoginFailed()
        })

        oldPassword.editText?.afterTextChanged {
            loginViewModel.loginDataChanged(oldPassword.editText?.text.toString(), oldPassword.editText?.text.toString())
        }

        newPassword.apply {
            editText?.afterTextChanged {
                loginViewModel.loginDataNewChangedPass(editText?.text.toString())
            }

            editText?.setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_DONE -> {
                        loading.visible()
                        update.isEnabled = false
                        oldPassword.isEnabled = false
                        newPassword.isEnabled = false
                        val newPass = ChangePasswordModel(oldPassword.editText?.text.toString(), newPassword.editText?.text.toString())
                        loading.visible()
                        loginViewModel.UpdatePassword(newPass,getAppSharedPrefs().userID().toString(),getAppSharedPrefs().domain())
                    }
                }
                false
            }

            update.setOnClickListener {

                loading.visible()
                update.isEnabled = false
                oldPassword.isEnabled = false
                newPassword.isEnabled = false
                val newPass = ChangePasswordModel(oldPassword.editText?.text.toString(), newPassword.editText?.text.toString())
                loginViewModel.UpdatePassword(newPass,getAppSharedPrefs().userID().toString(),getAppSharedPrefs().domain())
                loading.visible()
            }
        }
    }
    private fun showLoginFailed() {
        // Initialize a new instance of
        val builder = AlertDialog.Builder(this)

        // Set the alert dialog title
        builder.setTitle("Login Failed")

        // Display a message on alert dialog
        builder.setMessage("Please enter the right login details.")

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("OK"){dialog, which ->
            dialog.dismiss()
        }
        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()

        // Display the alert dialog on app interface
        dialog.show()
    }

}
