package com.aotomot.ezymovr.ui.login

import android.os.Bundle
import android.view.inputmethod.EditorInfo
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.aotomot.ezymovr.CustomAlert.AlertDialog
import com.aotomot.ezymovr.CustomAlert.OnItemClickListener
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.Repository.CommonRepository
import com.aotomot.ezymovr.WebService.WebServiceAPIFactory
import com.aotomot.ezymovr.extFunctions.domain
import com.aotomot.ezymovr.extFunctions.getAppSharedPrefs
import com.aotomot.ezymovr.extFunctions.visible
import com.aotomot.ezymovr.viewmodels.ForgotPassViewModel
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_forgot_password.*

class ForgotPasswordActivity : AppCompatActivity() {

    private lateinit var forgotViewModel: ForgotPassViewModel
    lateinit var email: TextInputLayout
    lateinit var domain: TextInputLayout
    lateinit var send: MaterialButton
    lateinit var repository : CommonRepository
    lateinit var driverRepository : CommonRepository
    protected inline fun <VM : ViewModel> viewModelFactory(crossinline f: () -> VM) =
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(aClass: Class<T>):T = f() as T
        }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        repository = CommonRepository(WebServiceAPIFactory.webServicebApi, this, this.getAppSharedPrefs().domain())
        driverRepository =  CommonRepository(WebServiceAPIFactory.webServiceApi_driver, this,this.getAppSharedPrefs().domain())
        forgotViewModel = ViewModelProvider(this,viewModelFactory {
            ForgotPassViewModel(
                repository,driverRepository
            )
        }).get(ForgotPassViewModel::class.java)

        forgotViewModel.resetResult.observe(this, Observer {
            AlertDialog.showDialogWithoutAlertHeaderSingleButton(this,
               it.message,
                object : OnItemClickListener {
                    override fun onItemClick(o: Any?, position: Int) {
                        finish()
                    }

                })
        })
        email = findViewById<TextInputLayout>(R.id.email)
        domain = findViewById<TextInputLayout>(R.id.domain)

        send = findViewById<MaterialButton>(R.id.send)
        send.isEnabled = false
        send?.setOnClickListener {
            if (!email.editText?.text.toString().isBlank()) {
                forgotViewModel.resetPassword(email.editText?.text.toString(),domain.editText?.text.toString())
            }else{
                // Initialize a new instance of
               AlertDialog.showDialogWithAlertHeaderSingleButton(this,"Blank Email","Please enter valid email",object : OnItemClickListener {
                   override fun onItemClick(o: Any?, position: Int) {
                   }
               })

            }
        }

        forgotViewModel.forgotPassFormState.observe(this, Observer {
            val loginState = it ?: return@Observer

            // disable login button unless both username / password is valid
            send.isEnabled = loginState.isDataValid

            if (loginState.usernameError != null) {
                email.error = getString(loginState.usernameError)
            }else
                email.error = null


        })

        domain.editText?.afterTextChanged {
            forgotViewModel.loginDataChangedComapnyName(domain.editText?.text.toString())
        }
        email.apply {
            editText?.afterTextChanged {
                forgotViewModel.loginDataChanged(
                    email.editText?.text.toString(),
                    email.editText?.text.toString()
                )
            }

            editText?.setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_DONE -> {
                        loading.visible()
                        email.isEnabled = false
                        forgotViewModel.resetPassword(email.editText?.text.toString(),domain.editText?.text.toString())
                    }
                }
                false
            }
        }

    }
}
