package com.aotomot.ezymovr.ui

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.viewModelScope
import com.aotomot.ezymovr.AppConstants
import com.aotomot.ezymovr.BuildConfig
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.Repository.CommonRepository
import com.aotomot.ezymovr.WebService.Result
import com.aotomot.ezymovr.WebService.WebServiceAPIFactory
import com.aotomot.ezymovr.extFunctions.*
import com.aotomot.ezymovr.models.Driver
import com.aotomot.ezymovr.models.LoginResponse
import com.aotomot.ezymovr.ui.login.LoginActivity
import com.aotomot.homeworld.models.DeviceRegistrationRequest
import com.aotomot.homeworld.models.DeviceRegistrationResponse
import com.google.android.datatransport.runtime.backends.BackendResponse.ok
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.FirebaseApp
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import kotlinx.coroutines.*
import kotlinx.coroutines.NonCancellable.cancel

class SplashActivity : AppCompatActivity() {
    private val _loginResult = MutableLiveData<LoginResponse>()
    private val _loginResultError = MutableLiveData<String>()
    private val _driverResult = MutableLiveData<Driver>()
    private val _driverResultError = MutableLiveData<String>()
    val MY_PERMISSIONS_REQUEST_INTERNET = 100;
    val CHECK_ONLINE_STATUS = 2;
    val CHECK_PERMISSIONS = 1;
    val SPLASH_DELAY : Long= 1000
    val splashCoroutine = CoroutineScope(Dispatchers.Main)
    val onBoardingCoroutine = CoroutineScope(Dispatchers.IO)
    lateinit var loginRepository : CommonRepository
    lateinit var driverRepository: CommonRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        if(!BuildConfig.DEBUG)
            FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true)
        if(!getAppSharedPrefs().domain().equals("")) {
            loginRepository = CommonRepository(
                WebServiceAPIFactory.webServicebApi,
                this,
                getAppSharedPrefs().domain()
            )
            driverRepository = CommonRepository(
                WebServiceAPIFactory.webServiceApi_driver,
                this,
                getAppSharedPrefs().domain()
            )
        }
        checkOnlineStatus()
        FirebaseApp.initializeApp(this);
        val sharedPref = this?.getSharedPreferences(
            getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        val defaultValue:Boolean = resources.getBoolean(R.bool.isActive_default_key)
        val isActive: Boolean = sharedPref.getBoolean(getString(R.string.isActive_key), defaultValue)
        //checkAppPermissions()
        FirebaseInstanceId.getInstance().instanceId
            .addOnSuccessListener(this,
                OnSuccessListener<InstanceIdResult> { instanceIdResult ->
                    AppConstants.FirebaseToken = instanceIdResult.token
                    this.getAppSharedPrefs().FirebaseToken(instanceIdResult.token)
                    Log.d("Firebase 2"," = " + AppConstants.FirebaseToken)
                    AppConstants.mDataForDeviceRegistrationRequest =
                        DeviceRegistrationRequest(
                            this.getAppSharedPrefs().uuid(),
                            "ANDROID",
                            getDeviceName()!!,
                            getDeviceOS(),
                            AppConstants.BASE_URL,
                            getVersionName(), instanceIdResult.token, true,getAppSharedPrefs().userID()
                        )
                    if(!getAppSharedPrefs().domain().equals("")) {
                        if (!AppConstants.FirebaseToken.isNullOrBlank()) {
                            RegisterDevice()
                        }
                    }
                })

        val isOnBoardingShow = this.getAppSharedPrefs().isonBoardingShown()
        splashCoroutine.launch {
            delay(SPLASH_DELAY)
            if(!isOnBoardingShow) {
                val intent = Intent(this@SplashActivity, OnBoardingActivity::class.java)
                startActivity(intent)
                finish()
            }else{
                if(getAppSharedPrefs().username().isEmpty() || getAppSharedPrefs().domain().isEmpty() || getAppSharedPrefs().password().isEmpty()) {
                    val intent = Intent(this@SplashActivity, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                }else {
                    login(getAppSharedPrefs().domain(), getAppSharedPrefs().username(), getAppSharedPrefs().password())
                    /*val intent = Intent(this@SplashActivity, MainActivity::class.java)
                    startActivity(intent)
                    finish()*/
                }
            }
        }
        _loginResult.observe(this@SplashActivity, Observer {
            getAppSharedPrefs().userID(it.userId)
            getAppSharedPrefs().AuthToken(it.authorization)
            AppConstants.Authorization = it.authorization
            fetchDriverDatails(it.userId.toString())
        })

        _loginResultError.observe(this@SplashActivity, Observer {
            showLoginFailed()
        })
        _driverResult.observe(this@SplashActivity, Observer {
            setResult(Activity.RESULT_OK)
            AppConstants.driver = it
            val intent = Intent(this@SplashActivity, MainActivity::class.java)
            startActivity(intent)
            //Complete and destroy login activity once successful
            finish()
        })

        _driverResultError.observe(this@SplashActivity, Observer {
            showLoginFailed()
        })
    }
    private fun showLoginFailed() {
        // Initialize a new instance of
        val builder = AlertDialog.Builder(this@SplashActivity)

        // Set the alert dialog title
        builder.setTitle("Login Failed")

        // Display a message on alert dialog
        builder.setMessage("Please enter the right login details.")

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("OK"){dialog, which ->
            val intent = Intent(this@SplashActivity, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()

        // Display the alert dialog on app interface
        dialog.show()
    }
    fun RegisterDevice(){
        CoroutineScope(Dispatchers.IO).launch {
            if ((!getAppSharedPrefs().isDeviceRegistered()) || (getAppSharedPrefs().deviceRegisteredID() == 0 ) ) {
                if (!AppConstants.mDataForDeviceRegistrationRequest.deviceToken.isNullOrBlank()) {
                    val response =
                        loginRepository.addDeviceToServer(AppConstants.mDataForDeviceRegistrationRequest)
                    when (response) {
                        is Result.Success<*> -> {
                            getAppSharedPrefs().isDeviceRegistered(true)
                            getAppSharedPrefs().deviceRegisteredID((response.data as DeviceRegistrationResponse).id)
                            AppConstants.registeredDeviceID = (response.data as DeviceRegistrationResponse).id
                            Log.d("Firebase 3","Registered Device")
                        }
                        //is Result.Error -> _error.postValue(response.exception.toString())
                    }
                }
            }else{
                //update Device
                if (!AppConstants.mDataForDeviceRegistrationRequest.deviceToken.isNullOrBlank()) {
                    val response =
                        loginRepository.updateDevice(getAppSharedPrefs().deviceRegisteredID().toString(),
                            AppConstants.mDataForDeviceRegistrationRequest)
                    when (response) {
                        is Result.Success<*> -> {Log.d("Firebase","Updated FRegistered Device")
                        }
                        //is Result.Error -> _error.postValue(response.exception.toString())
                    }
                }
            }
        }
    }
    fun fetchDriverDatails(userId : String) {
        // can be launched in a separate asynchronous job
        CoroutineScope(Dispatchers.IO).launch {
            val response = driverRepository.getDriverByUserID(userId)

            when (response) {
                is com.aotomot.ezymovr.WebService.Result.Success<*> -> _driverResult.postValue(response.data as Driver)
                is com.aotomot.ezymovr.WebService.Result.Error -> _driverResultError.postValue(response.exception.toString())
            }

        }
    }
    fun login(companyName: String, username: String, password: String) {
        // can be launched in a separate asynchronous job
        AppConstants.DOMAIN_NAME = companyName
        CoroutineScope(Dispatchers.IO).launch {
            val response = loginRepository.login(username, password)

            when (response) {
                is com.aotomot.ezymovr.WebService.Result.Success<*> -> _loginResult.postValue(response.data as LoginResponse)
                is com.aotomot.ezymovr.WebService.Result.Error -> _loginResultError.postValue(response.exception.toString())
            }

        }
    }
    override fun onPause() {
        splashCoroutine.cancel()
        super.onPause()
    }
    fun getDeviceOS () : String{
        var deviceOs : String
        try {
            deviceOs = "ANDROID OS " + Build.VERSION.SDK_INT
        } catch (e: NoSuchFieldError) {
            e.printStackTrace()
            deviceOs = "ANDROID OS " + Build.VERSION.SDK_INT
        }
        return deviceOs
    }
    fun getDeviceName(): String? {
        val manufacturer = Build.MANUFACTURER
        val model = Build.MODEL
        return if (model.startsWith(manufacturer)) {
            return model.capitalize()
        } else {
            return  manufacturer.capitalize() + " " + model
        }
    }
    fun getVersionName():String{
        var versionName :String = ""
        try {
            versionName =
                getPackageManager().getPackageInfo(getPackageName(), 0).versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        return versionName
    }
    override fun onResume() {
        super.onResume()
        checkOnlineStatus()
    }
    fun checkOnlineStatus(){
        if(!isOnline()){
            createAlertDialog(
                R.string.offline_message,
                R.string.offline,CHECK_ONLINE_STATUS);
        }
    }

    fun isOnline(): Boolean {
        val cm =
            this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return cm?.isDefaultNetworkActive
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_INTERNET -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    createAlertDialog(
                        R.string.enable_internet_message,
                        R.string.permissions,CHECK_PERMISSIONS);
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }
    fun createAlertDialog(messId: Int, titleId: Int, positiveAction: Int?){
        val builder: AlertDialog.Builder? = let {
            AlertDialog.Builder(it)
        }

// 2. Chain together various setter methods to set the dialog characteristics
        builder?.setMessage(messId)
            ?.setTitle(titleId)
        builder?.apply {
            setPositiveButton(
                R.string.ok,
                DialogInterface.OnClickListener { dialog, id ->
                    // User clicked OK button
                    when (positiveAction){
                        CHECK_PERMISSIONS -> checkOnlineStatus()
                        CHECK_ONLINE_STATUS -> checkOnlineStatus()
                    }
                })
            setNegativeButton(
                R.string.cancel,
                DialogInterface.OnClickListener { dialog, id ->
                    // User cancelled the dialog
                    finish();
                })
        }
// 3. Get the <code><a href="/reference/android/app/AlertDialog.html">AlertDialog</a></code> from <code><a href="/reference/android/app/AlertDialog.Builder.html#create()">create()</a></code>
        val dialog: AlertDialog? = builder?.create()
    }
}
