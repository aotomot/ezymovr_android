package com.aotomot.ezymovr.ui

import android.Manifest
import android.annotation.SuppressLint
import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil.setContentView
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.aotomot.ezymovr.AppConstants
import com.aotomot.ezymovr.CustomAlert.AlertDialog
import com.aotomot.ezymovr.CustomAlert.OnItemClickListener
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.extFunctions.gone
import com.aotomot.ezymovr.models.Driver
import com.aotomot.ezymovr.models.ImagePUTTitleComment
import com.aotomot.ezymovr.ui.EditUserProfileActivity
import com.aotomot.ezymovr.utils.ScalingUtilities
import com.aotomot.ezymovr.viewmodels.SendPhotoViewModel
import com.bumptech.glide.Glide
import com.google.android.material.button.MaterialButton
import com.mindorks.paracamera.Camera
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.*
import java.text.SimpleDateFormat
import java.util.*

class EditUserProfileActivity : AppCompatActivity() {
    var profile_image: ImageView? = null

    var firstNameEditText: EditText? = null
    var lastNameEditText: EditText? = null

    var PhoneNumberEditText: EditText? = null

    var LicenceNumberEditText: EditText? = null

    var LicenceExpiryEditText: EditText? = null

    var TruckRegoEditText: EditText? = null

    var TruckRegoExpiryEditText: EditText? = null

    var updateBtn: Button? = null
    private lateinit var mViewModel: SendPhotoViewModel
    var progress_wheel: ProgressBar? = null
    private var pictureActionIntent: Intent? = null
    var driverData: Driver? = null
    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }
    protected inline fun <VM : ViewModel> viewModelFactory(crossinline f: () -> VM) =
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(aClass: Class<T>):T = f() as T
        }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_user_profile)
        mViewModel = ViewModelProvider(this, viewModelFactory {
            SendPhotoViewModel(this)
        }).get(SendPhotoViewModel::class.java)
        driverData = AppConstants.driver
        profile_image = findViewById<ImageView>(R.id.profile_image)
        firstNameEditText = findViewById<EditText>(R.id.firstNameEditText)
        lastNameEditText = findViewById<EditText>(R.id.LastNameEditText)
        PhoneNumberEditText = findViewById<EditText>(R.id.PhoneNumberEditText)
        LicenceNumberEditText = findViewById<EditText>(R.id.LicenceNumberEditText)
        LicenceExpiryEditText = findViewById<EditText>(R.id.LicenceExpiryEditText)
        TruckRegoEditText =  findViewById<EditText>(R.id.TruckRegoEditText)
        TruckRegoExpiryEditText = findViewById<EditText>(R.id.TruckRegoExpiryEditText)
        updateBtn = findViewById<MaterialButton>(R.id.updateBtn)
        progress_wheel = findViewById<ProgressBar>(R.id.progress_wheel)

        profile_image?.setOnClickListener (object : View.OnClickListener{
            override fun onClick(v: View?) {
                uploadPopUp()
            }
            //
        })
        mViewModel.postImageREsult.observe(this, androidx.lifecycle.Observer {
            progress_wheel?.setVisibility(View.GONE)
        })
        mViewModel.updateDriverDetails.observe(this, androidx.lifecycle.Observer {
            AlertDialog.showDialogWithAlertHeaderSingleButton(this,"Update Successfull","",object :OnItemClickListener{
                override fun onItemClick(o: Any?, position: Int) {
                    finish()
                }

            })
        })
        mViewModel.updateDriverDetailsError.observe(this, androidx.lifecycle.Observer {
            AlertDialog.showDialogWithAlertHeaderSingleButton(this,"Update Error","Something went wrong \n Please try again later",object :OnItemClickListener{
                override fun onItemClick(o: Any?, position: Int) {
                    finish()
                }

            })
        })
        //AppConstant.TrackingFragmentOrActivity(AnalyticsConstant.editProfileScreen)
        setview()
        updateBtn!!.setOnClickListener (object : View.OnClickListener{
            override fun onClick(v: View?) {
                try {
                    val requestFile =
                        RequestBody.create(MediaType.parse("multipart/form-data"), file)
                    decodeFile(file!!.name, 110, 110)
                    val body =
                        MultipartBody.Part.createFormData("file", file!!.name, requestFile)
                    /*NetworkManager.getInstance().postUploadImage(
                        ModelManager.getInstance().getLogin().getAuthorization(),
                        body
                    )*/
                    mViewModel.postImage(body,"")
                    progress_wheel?.setVisibility(View.VISIBLE)
                } catch (e: Exception) {
                }
                updateDriverDetails()
            }
            //
        })
        progress_wheel?.setVisibility(View.GONE)
    }

    fun setview() {
        firstNameEditText!!.setText(driverData!!.firstName)
        lastNameEditText!!.setText(driverData!!.lastName)
        PhoneNumberEditText!!.setText(driverData!!.contact.contactNumber)
        LicenceNumberEditText!!.setText(driverData!!.email)
        LicenceExpiryEditText!!.gone()
        TruckRegoEditText!!.gone()
        TruckRegoExpiryEditText!!.gone()
        firstNameEditText!!.isEnabled = true
        lastNameEditText!!.isEnabled = true
        PhoneNumberEditText!!.isEnabled = true
        LicenceNumberEditText!!.isEnabled = false

        if(!AppConstants.driver.driverImage.url.isBlank()){
            Glide.with(this).asBitmap().load(AppConstants.driver.driverImage.url).into(findViewById(R.id.profile_image))
        }

       /* if (ModelManager.getInstance().getDriverImage() != null && !ModelManager.getInstance()
                .getDriverImage().getUrl().equals("")
        ) {
            Picasso.get().load(ModelManager.getInstance().getDriverImage().getUrl())
                .into(profile_image)
        }*/
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    fun onProfileImageClicked() {
        uploadPopUp()
    }

    fun updateDriverDetails() {
        val newDriverDetails = AppConstants.driver
        newDriverDetails.firstName = firstNameEditText!!.text.toString()
        newDriverDetails.lastName = lastNameEditText!!.text.toString()
        newDriverDetails.contact.contactNumber = PhoneNumberEditText!!.text.toString()

        mViewModel.UpdateDriver(AppConstants.driver.id.toString(),newDriverDetails)
//        NetworkManager.getInstance().updateDriverDetails(ModelManager.getInstance().getLogin().getAuthorization(),ModelManager.getInstance().getLogin().getUserId()+"",model);
    }

    fun uploadPopUp() {
        AlertDialog.showDialogWithAlertHeaderThreeButton(
            this,
            "Upload Image",
            "Upload Pictures Option",
            object : OnItemClickListener {
                override fun onItemClick(o: Any?, position: Int) {
                    if (position == 0) {
                        //camera
                        if (checkAndRequestPermissions(this@EditUserProfileActivity,this@EditUserProfileActivity)) {
                            launchCameraOption()
                        }
                    } else {

                        //gallery
                        if (checkAndRequestPermissions(this@EditUserProfileActivity,this@EditUserProfileActivity)) {
                            pictureActionIntent = Intent(Intent.ACTION_GET_CONTENT, null)
                            pictureActionIntent!!.type = "image/*"
                            pictureActionIntent!!.putExtra("return-data", true)
                            startActivityForResult(
                                pictureActionIntent,
                                GALLERY_PICTURE
                            )
                        }
                    }
                }
            })
    }

    /**
     * Launches camera
     */
    var camera: Camera? = null
    private fun launchCameraOption() {
        camera = Camera.Builder()
            .resetToCorrectOrientation(true) // it will rotate the camera bitmap to the correct orientation from meta data
            .setTakePhotoRequestCode(1)
            .setDirectory("pics")
            .setName("ali_" + System.currentTimeMillis())
            .setImageFormat(Camera.IMAGE_JPEG)
            .setCompression(75)
            .setImageHeight(1000) // it will try to achieve this height as close as possible maintaining the aspect ratio;
            .build(this)
        try {
            camera!!.takePicture()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        //        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    private var imageUri: Uri? = null
    var file: File? = null
    private var bitmap: Bitmap? = null

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Camera.REQUEST_TAKE_PHOTO) {
            try {
                val bitmap = camera!!.cameraBitmap
                profile_image!!.setImageBitmap(bitmap)
                /**
                 * prepare file to send to backend
                 */
                val timeStamp =
                    SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
                val imageFileName = "JPEG_" + timeStamp + "_"
                val storageDir =
                    Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES
                    )
                try {
                    file = File.createTempFile(
                        imageFileName,  /* prefix */
                        ".jpg",  /* suffix */
                        storageDir /* directory */
                    )
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                var fos: FileOutputStream? = null
                //Convert bitmap to byte array
                val bos = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80 /*ignored for PNG*/, bos)
                val bitmapdata = bos.toByteArray()

                //write the bytes in file
                try {
                    fos = FileOutputStream(file)
                    fos.write(bitmapdata)
                    fos.flush()
                    fos.close()
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            } catch (e: Exception) {
            }
        } else if (requestCode == CAMERA_REQUEST) {
            if (data!!.hasExtra("data")) {
                bitmap = data!!.extras!!["data"] as Bitmap?
                val timeStamp =
                    SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
                val imageFileName = "JPEG_" + timeStamp + "_"
                val storageDir =
                    Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES
                    )
                try {
                    file = File.createTempFile(
                        imageFileName,  /* prefix */
                        ".jpg",  /* suffix */
                        storageDir /* directory */
                    )
                } catch (e: IOException) {
                    e.printStackTrace()
                }


                //Convert bitmap to byte array
                val bos = ByteArrayOutputStream()
                bitmap!!.compress(Bitmap.CompressFormat.JPEG, 90 /*ignored for PNG*/, bos)
                val bitmapdata = bos.toByteArray()

                //write the bytes in file
                var fos: FileOutputStream? = null
                try {
                    fos = FileOutputStream(file)
                    fos.write(bitmapdata)
                    fos.flush()
                    fos.close()
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                profile_image!!.setImageBitmap(bitmap)
            }
        } else if (requestCode == GALLERY_PICTURE) {
            var fos: FileOutputStream? = null
            imageUri = data!!.data
            var exif: ExifInterface? = null
            try {
                val inputStream: InputStream =
                    getContentResolver().openInputStream(imageUri!!)!!
                exif = ExifInterface(inputStream)

//                exif = new ExifInterface(getPaths(this, imageUri));
            } catch (e: IOException) {
                e.printStackTrace()
            }
            val orientation = exif!!.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED
            )
            try {
                bitmap = getThumbnail(imageUri)
            } catch (e: IOException) {
                e.printStackTrace()
            }
            var rotateBitmap: Bitmap? = null
            val matrix = Matrix()
            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                matrix.setRotate(90f)
                rotateBitmap = Bitmap.createBitmap(
                    bitmap!!,
                    0,
                    0,
                    bitmap!!.width,
                    bitmap!!.height,
                    matrix,
                    true
                )
                bitmap = rotateBitmap
                profile_image!!.setImageBitmap(bitmap)
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                matrix.setRotate(180f)
                rotateBitmap = Bitmap.createBitmap(
                    bitmap!!,
                    0,
                    0,
                    bitmap!!.width,
                    bitmap!!.height,
                    matrix,
                    true
                )
                bitmap = rotateBitmap
                profile_image!!.setImageBitmap(bitmap)
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                matrix.setRotate(270f)
                rotateBitmap = Bitmap.createBitmap(
                    bitmap!!,
                    0,
                    0,
                    bitmap!!.width,
                    bitmap!!.height,
                    matrix,
                    true
                )
                bitmap = rotateBitmap
                profile_image!!.setImageBitmap(bitmap)
            } else {
                profile_image!!.setImageBitmap(bitmap)
            }
            /**
             * prepare file to send to backend
             */
            val timeStamp =
                SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
            val imageFileName = "JPEG_" + timeStamp + "_"
            val storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES
            )
            try {
                file = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",  /* suffix */
                    storageDir /* directory */
                )
            } catch (e: IOException) {
                e.printStackTrace()
            }

            //Convert bitmap to byte array
            val bos = ByteArrayOutputStream()
            bitmap!!.compress(Bitmap.CompressFormat.JPEG, 80 /*ignored for PNG*/, bos)
            val bitmapdata = bos.toByteArray()
            //write the bytes in file
            try {
                fos = FileOutputStream(file)
                fos.write(bitmapdata)
                fos.flush()
                fos.close()
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        } else if (requestCode == REQUEST_ID_CAMERA_PERMISSIONS) {
            launchCameraOption()
        }
    }

    @Throws(IOException::class)
    fun getThumbnail(uri: Uri?): Bitmap? {
        val THUMBNAIL_SIZE = 768
        var input: InputStream = getContentResolver().openInputStream(uri!!)!!
        val onlyBoundsOptions = BitmapFactory.Options()
        onlyBoundsOptions.inJustDecodeBounds = true
        onlyBoundsOptions.inDither = true // optional
        onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888 // optional
        BitmapFactory.decodeStream(input, null, onlyBoundsOptions)
        input.close()
        if (onlyBoundsOptions.outWidth == -1
            || onlyBoundsOptions.outHeight == -1
        ) return null
        val originalSize =
            if (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) onlyBoundsOptions.outHeight else onlyBoundsOptions.outWidth

        var ratio :Double = 0.0
        if(originalSize > THUMBNAIL_SIZE)
            ratio = (originalSize / THUMBNAIL_SIZE).toDouble()
        else
            ratio = 1.0

        /*val ratio =
            (if (originalSize > THUMBNAIL_SIZE) originalSize / THUMBNAIL_SIZE else 1.0).toDouble()*/
        val bitmapOptions = BitmapFactory.Options()
        bitmapOptions.inSampleSize =
            getPowerOfTwoForSampleRatio(ratio)
        bitmapOptions.inDither = true // optional
        bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888 // optional
        input = getContentResolver().openInputStream(uri!!)!!
        val bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions)
        input.close()
        return bitmap
    }

    private fun decodeFile(
        path: String,
        DESIREDWIDTH: Int,
        DESIREDHEIGHT: Int
    ): String {
        var strMyImagePath: String? = null
        var scaledBitmap: Bitmap? = null
        try {
            // Part 1: Decode image
            val unscaledBitmap: Bitmap = ScalingUtilities.decodeFile(
                path,
                DESIREDWIDTH,
                DESIREDHEIGHT,
                ScalingUtilities.ScalingLogic.FIT
            )
            scaledBitmap =
                if (!(unscaledBitmap.width <= DESIREDWIDTH && unscaledBitmap.height <= DESIREDHEIGHT)) {
                    // Part 2: Scale image
                    ScalingUtilities.createScaledBitmap(
                        unscaledBitmap,
                        DESIREDWIDTH,
                        DESIREDHEIGHT,
                        ScalingUtilities.ScalingLogic.FIT
                    )
                } else {
                    unscaledBitmap.recycle()
                    return path
                }

            // Store to tmp file
            val extr =
                Environment.getExternalStorageDirectory().toString()
            val mFolder = File("$extr/TMMFOLDER")
            if (!mFolder.exists()) {
                mFolder.mkdir()
            }
            val s = "tmp.png"
            val f = File(mFolder.absolutePath, s)
            strMyImagePath = f.absolutePath
            var fos: FileOutputStream? = null
            try {
                fos = FileOutputStream(f)
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 75, fos)
                fos.flush()
                fos.close()
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            scaledBitmap.recycle()
        } catch (e: Throwable) {
        }
        return strMyImagePath ?: path
    }



    /*fun onEventMainThread(event: BasicEvent) {
        if (event === BasicEvent.UPLOAD_IMAGE_SUCCESS) {
            val assignToJobModel = AssignToJobModel(
                ModelManager.getInstance().getUploadImage().getId(),
                ModelManager.getInstance().getDriver().getId().toString() + ""
            )
            NetworkManager.getInstance().assignImageToDriver(
                ModelManager.getInstance().getLogin().getAuthorization(),
                assignToJobModel
            )
        }
        if (event === BasicEvent.PATCH_IMAGE_ASSIGN_SUCCESS) {
            val model = UpdateUserProfileAssign(
                false,
                true,
                "",
                "",
                "Profile photo",
                0,
                ModelManager.getInstance().getLogin().getUserId()
                ,
                ModelManager.getInstance().getDriver().getId().toString() + ""
            )
            NetworkManager.getInstance().updateDriverImage(
                ModelManager.getInstance().getLogin().getAuthorization(),
                ModelManager.getInstance().getUploadImage().getId(),
                model
            )
            NetworkManager.getInstance().getDriverByUserId(
                ModelManager.getInstance().getLogin().getUserId(),
                ModelManager.getInstance().getLogin().getAuthorization()
            )
        }
        if (event === BasicEvent.DRIVER_SUCCESS) {
            progress_wheel.setVisibility(View.GONE)
            finish()
        }
        if (event === BasicEvent.UPLOAD_IMAGE_FAILED) {
            if (!AppConstant.haveNetworkConnection(this)) {
                AlertDialog.showDialogWithAlertHeaderSingleButton(this,
                    "No Internet!",
                    "Please try again !",
                    object : OnItemClickListener() {
                        fun onItemClick(o: Any?, position: Int) {
                            try {
                                val requestFile = RequestBody.create(
                                    MediaType.parse("multipart/form-data"),
                                    file
                                )
                                val body = MultipartBody.Part.createFormData(
                                    "file",
                                    file!!.name,
                                    requestFile
                                )
                                NetworkManager.getInstance().postUploadImage(
                                    ModelManager.getInstance().getLogin().getAuthorization(), body
                                )
                                progress_wheel.setVisibility(View.VISIBLE)
                            } catch (e: Exception) {
                            }
                            updateDriverDetails()
                        }
                    })
            }
        }
    }*/

    companion object {
        protected const val CAMERA_REQUEST = 1110
        protected const val GALLERY_PICTURE = 1120
        private const val REQUEST_ID_CAMERA_PERMISSIONS = 2011
        private fun getPowerOfTwoForSampleRatio(ratio: Double): Int {
            val k = Integer.highestOneBit(Math.floor(ratio).toInt())
            return if (k == 0) 1 else k
        }

        fun checkAndRequestPermissions(context: Context?, activity: AppCompatActivity): Boolean {
            val ExtstorePermission: Int = ContextCompat.checkSelfPermission(
                context!!,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
            val cameraPermission: Int = ContextCompat.checkSelfPermission(
                context!!,
                Manifest.permission.CAMERA
            )
            val listPermissionsNeeded: MutableList<String> =
                ArrayList()
            if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.CAMERA)
            }
            if (ExtstorePermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(
                    activity, listPermissionsNeeded.toTypedArray(),
                    REQUEST_ID_CAMERA_PERMISSIONS
                )
                return false
            }
            return true
        }

        fun getPaths(context: Context, uri: Uri): String? {

            //check here to KITKAT or new version
            val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT

            // DocumentProvider
            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {

                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split = docId.split(":").toTypedArray()
                    val type = split[0]
                    if ("primary".equals(type, ignoreCase = true)) {
                        return Environment.getExternalStorageDirectory()
                            .toString() + "/" + split[1]
                    }
                } else if (isDownloadsDocument(uri)) {
                    val id = DocumentsContract.getDocumentId(uri)
                    val contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://<span id=\"IL_AD1\" class=\"IL_AD\">downloads</span>/public_downloads"),
                        java.lang.Long.valueOf(id)
                    )
                    return getDataColumn(
                        context,
                        contentUri,
                        null,
                        null
                    )
                } else if (isMediaDocument(uri)) {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split = docId.split(":").toTypedArray()
                    val type = split[0]
                    var contentUri: Uri? = null
                    if ("image" == type) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    } else if ("video" == type) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                    } else if ("audio" == type) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                    }
                    val selection = "_id=?"
                    val selectionArgs = arrayOf(
                        split[1]
                    )
                    return getDataColumn(
                        context,
                        contentUri,
                        selection,
                        selectionArgs
                    )
                }
            } else if ("content".equals(uri.scheme, ignoreCase = true)) {

                // Return the remote address
                return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(
                    context,
                    uri,
                    null,
                    null
                )
            } else if ("file".equals(uri.scheme, ignoreCase = true)) {
                return uri.path
            }
            return null
        }

        fun getDataColumn(
            context: Context, uri: Uri?, selection: String?,
            selectionArgs: Array<String>?
        ): String? {
            var cursor: Cursor? = null
            val column = "_data"
            val projection = arrayOf(
                column
            )
            try {
                cursor = context.contentResolver.query(
                    uri!!, projection, selection, selectionArgs,
                    null
                )
                if (cursor != null && cursor.moveToFirst()) {
                    val index = cursor.getColumnIndexOrThrow(column)
                    return cursor.getString(index)
                }
            } finally {
                cursor?.close()
            }
            return null
        }

        fun isExternalStorageDocument(uri: Uri): Boolean {
            return "com.android.externalstorage.documents" == uri.authority
        }

        fun isDownloadsDocument(uri: Uri): Boolean {
            return "com.android.providers.downloads.documents" == uri.authority
        }

        fun isMediaDocument(uri: Uri): Boolean {
            return "com.android.providers.media.documents" == uri.authority
        }

        fun isGooglePhotosUri(uri: Uri): Boolean {
            return "com.google.android.apps.photos.content" == uri.authority
        }
    }
}