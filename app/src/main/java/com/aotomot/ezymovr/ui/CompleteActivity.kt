package com.aotomot.ezymovr.ui

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.aotomot.ezymovr.AppConstants
import com.aotomot.ezymovr.CustomAlert.AlertDialog
import com.aotomot.ezymovr.CustomAlert.OnItemClickListener
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.adapters.AttachListAdapter
import com.aotomot.ezymovr.adapters.EventAdapter
import com.aotomot.ezymovr.adapters.TimerAdapter
import com.aotomot.ezymovr.extFunctions.gone
import com.aotomot.ezymovr.extFunctions.returnDashIfEmpty
import com.aotomot.ezymovr.extFunctions.visible
import com.aotomot.ezymovr.models.AttachmentItem
import com.aotomot.ezymovr.models.Jobs
import com.aotomot.ezymovr.viewmodels.CompleteViewModel
import com.aotomot.homeworld.models.HomeDirections
import com.bumptech.glide.Glide
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.google.android.material.button.MaterialButton
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class CompleteActivity : AppCompatActivity() {

    private lateinit var job: Jobs
    private lateinit var googleMap: GoogleMap
    private var jobStatus: String = ""
    private lateinit var mTimerAdapter: TimerAdapter
    private lateinit var timerlist: RecyclerView
    private lateinit var mViewModel: CompleteViewModel
    private var jobID: Int = 0
    private lateinit var attachlist: RecyclerView
    private lateinit var mAdapter: AttachListAdapter
    private lateinit var mapView:MapView
    private lateinit var mEventAdapter: EventAdapter
    private lateinit var eventlist: RecyclerView
    private var bAttachmentFetched: Boolean = false
    private lateinit var totalTimeText: TextView
    var map: GoogleMap? = null

    protected inline fun <VM : ViewModel> viewModelFactory(crossinline f: () -> VM) =
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(aClass: Class<T>):T = f() as T
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_complete)
        jobID = intent.getIntExtra("jobID",0)
        jobStatus = intent.getStringExtra("status")!!

        mViewModel = ViewModelProvider(this, viewModelFactory {
            CompleteViewModel(this)
        }).get(CompleteViewModel::class.java)

        mViewModel.fetchjobAttachmentList(jobID.toString())
        mViewModel.jobList.observe(this, Observer {
            job = it.filter { it: Jobs -> it.id == jobID}.single()
            mTimerAdapter.update(job.history)
            if(job.timer.events.size >0)
                mEventAdapter.update(job.timer.events?.toMutableList())

            //populateJobs(job)
            SetUpView()

            if(!bAttachmentFetched)
                mViewModel.fetchjobAttachmentList(jobID.toString())


        })
        mViewModel.jobAttachmentList.observe(this, Observer {
            bAttachmentFetched = true
            mAdapter.update(it!!)
            attachlist.visible()
        })
        mViewModel.error.observe(this, Observer {
            attachlist.gone()
        })
        mViewModel.declineJob.observe(this, Observer {
            mViewModel.fetchjobsList()
        })
        mViewModel.declineJobResult.observe(this, Observer {
            AlertDialog.showDialogWithAlertHeaderSingleButton(this,
                "Error",
                "Something went wrong. Try again.",
                object: OnItemClickListener{
                    override fun onItemClick(o: Any?, position: Int) {
                        if(position == 0){
                            mViewModel.fetchjobsList()
                        }
                    }

                })
        })
        mViewModel.addTime.observe(this, Observer {
            mViewModel.fetchjobsList()
            Log.d("","")
        })
        mViewModel.addTimeResult.observe(this, Observer {
           //loading.gone()
            AlertDialog.showDialogWithAlertHeaderSingleButton(this,
                "Error",
                "Something went wrong. Try again.",
                object: OnItemClickListener {
                    override fun onItemClick(o: Any?, position: Int) {
                        if(position == 0){
                            //addTime.text = 00
                        }
                    }

                })
        })
        mViewModel.directions.observe(this, Observer {
            setPolylinesAndMarkers(it)
        })
        job = AppConstants.JobsLists.filter { it.id == jobID }.single()
        var jobDate : Date? = null
        var jobTime : Date? = null
        val joinDatesdf = SimpleDateFormat(getString(R.string.start_only_date_sdf))
        val joinTimedf = SimpleDateFormat(getString(R.string.start_time_sdf))
        try {
            jobDate = joinDatesdf.parse(AppConstants.Epoch2DateStringTimeStartOnlyDate(job.startDate.toString())!!)
            jobTime = joinTimedf.parse(AppConstants.Epoch2DateStringTimeStartTime(job.startDate.toString())!!)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        if(jobStatus.equals("Expired")) {
            with(findViewById<TextView>(R.id.job_status)){
                text = jobStatus
                setTextColor(getColor(R.color.expired))
            }
        }
        if(!job.clientObj?.phone?.contactNumber.toString().isNullOrEmpty()){
            findViewById<ImageView>(R.id.call_btn).gone()
        }else{
            findViewById<ImageView>(R.id.call_btn).setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    onTelephoneClicked(job.clientObj?.phone?.contactNumber.toString())
                }
            })

        }
        findViewById<TextView>(R.id.job_no).text = job.id.toString()
        findViewById<TextView>(R.id.startdate_text).text = joinDatesdf.format(jobDate!!)
        findViewById<TextView>(R.id.starttime_text).text = joinTimedf.format(jobTime!!)
        findViewById<TextView>(R.id.cust_name_text).text = (job.clientObj?.contacts?.get(0)?.name?.returnDashIfEmpty())
        findViewById<TextView>(R.id.cust_phone_text).text = (job.clientObj?.contacts?.get(0)?.contactNumber?.returnDashIfEmpty())
        findViewById<TextView>(R.id.cust_pick_text).text = job.pickupLocation.name.returnDashIfEmpty()
        findViewById<TextView>(R.id.cust_deliver_text).text = job.deliveryLocation.name.returnDashIfEmpty()
        findViewById<TextView>(R.id.cust_notes_text).text = (job.note.returnDashIfEmpty())
        findViewById<TextView>(R.id.timer).text = job.timer.timer
        totalTimeText = findViewById<TextView>(R.id.cust_total_text)
        totalTimeText.text = job.timer.timer
        findViewById<TextView>(R.id.bedroom_text).text = job.additionalInformation.type
        if (!job.additionalInformation.helpers.isNullOrEmpty())
            findViewById<TextView>(R.id.helper_text).text = job.additionalInformation.helpers.plus( " ").plus("helpers")
        else
            findViewById<TextView>(R.id.helper_text).text = "--"
        findViewById<MaterialButton>(R.id.get_started).setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                val intent = Intent(this@CompleteActivity, UploadDocumentActivity::class.java)
                intent.putExtra("jobId", jobID)
                startActivity(intent)
            }
        })
        findViewById<ImageView>(R.id.user_img).setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                val intent = Intent(this@CompleteActivity, UserProfileActivity::class.java)
                startActivity(intent)
            }

        })
        val addTime = findViewById<TextView>(R.id.add_timer)
        val saveAddTime = findViewById<Button>(R.id.save_add_time)
        val currentTime = findViewById<TextView>(R.id.add_timer).text.toString()
        findViewById<ImageView>(R.id.plus_img).setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                val st1 = StringTokenizer(addTime.text.toString(), ":")
                var j = 0;
                var value = mutableListOf<Int>()
                // iterate through tokens
                for (index in 0..st1.countTokens().minus(1)) {
                    value.add(index,Integer.parseInt(st1.nextToken()))
                }

                // call time add method with current hour, minute and minutesToAdd,
                // return added time as a string
                val dateRevisedEstimatedDeliveryTime = addTime(value[0], value[1], 15);
                addTime.text = dateRevisedEstimatedDeliveryTime
                saveAddTime.isEnabled = true
            }

        })
        findViewById<ImageView>(R.id.minus_img).setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                val st1 = StringTokenizer(addTime.text.toString(), ":")
                var j = 0;
                var value = mutableListOf<Int>()
                // iterate through tokens
                for (index in 0..st1.countTokens().minus(1)) {
                    value.add(index,Integer.parseInt(st1.nextToken()))
                }

                // call time add method with current hour, minute and minutesToAdd,
                // return added time as a string
                val dateRevisedEstimatedDeliveryTime = minusTime(value[0], value[1], 15);
                addTime.text = dateRevisedEstimatedDeliveryTime
                saveAddTime.isEnabled = true
            }

        })

        saveAddTime.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                val st1 = StringTokenizer(addTime.text.toString(), ":")
                var totalminutes = (Integer.parseInt(st1.nextToken()) * 60)
                totalminutes = totalminutes + Integer.parseInt(st1.nextToken())
                //loading.visible()
                mViewModel.AddTime(jobID.toString(),totalminutes,"",true)
                saveAddTime.isEnabled=false
                addTime.text = "00:00"
            }

        })


        if(!AppConstants.driver.driverImage.url.isBlank()){
            Glide.with(this).asBitmap().load(AppConstants.driver.driverImage.url).into(findViewById(R.id.user_img))
        }
        timerlist = findViewById<RecyclerView>(R.id.timer_list)
        mTimerAdapter = TimerAdapter(this, job.history)
        timerlist.layoutManager = LinearLayoutManager(this)
        timerlist.adapter = mTimerAdapter

        mAdapter = AttachListAdapter(this, emptyList<AttachmentItem>().toMutableList())
        attachlist = findViewById<RecyclerView>(R.id.attach_list)
        attachlist.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        attachlist.adapter = mAdapter

        eventlist = findViewById<RecyclerView>(R.id.event_list)
        mEventAdapter = EventAdapter(this, job.timer.events.toMutableList())
        eventlist.layoutManager = LinearLayoutManager(this)
        eventlist.adapter = mEventAdapter

        mapView = findViewById<MapView>(R.id.mapView2)
        setMap(mapView,savedInstanceState)


    }
    fun setPolylinesAndMarkers(mDirections : HomeDirections){
        if((mDirections != null) && (this.map!! != null)) {
            var polyline: LatLng
            val mPolylineOptions = PolylineOptions()
            val builder = LatLngBounds.Builder()
            if(mDirections.routes.size == 0){
                return
            }
            val selectedRoute = mDirections.routes.get(0)
            with(selectedRoute) {

                for (indexLegs in 0..legs.size.minus(1)) {
                    //Add Moarkers for Star & End Location
                    with(legs[indexLegs]) {
                        /* map!!.addMarker(MarkerOptions().position(LatLng(d.lat, startLocation.lng)))*//*.setIcon(
                            BitmapDescriptorFactory.fromResource(com.google.android.gms.maps.R.drawable.icon_current_marker))*//*
                        map!!.addMarker(MarkerOptions().position(LatLng(endLocation.lat, endLocation.lng)))*//*.setIcon(
                            BitmapDescriptorFactory.fromResource(com.google.android.gms.maps.R.drawable.icon_dest_marker))*//*

                        builder.include(LatLng(startLocation.lat, startLocation.lng))
                        builder.include(LatLng(endLocation.lat, endLocation.lng))*/
                        for (indexSteps in 0..legs[indexLegs].steps.size.minus(1)) {
                            with(legs[indexLegs].steps[indexSteps]) {
                                //Add to Polylin Options
                                mPolylineOptions.add(LatLng(startLocation.lat, startLocation.lng))
                                mPolylineOptions.add(LatLng(endLocation.lat, endLocation.lng))

                                //Add to Bounds
                                builder.include(LatLng(startLocation.lat, startLocation.lng))
                                builder.include(LatLng(endLocation.lat, endLocation.lng))
                            }
                        }
                    }
                }
            }
            //Draw Polyline
            val polylines = this.map!!.addPolyline(mPolylineOptions)
            polylines.color = android.graphics.Color.RED
            polylines.isGeodesic = true
            polylines.width = 12F

            //Include in the dispaly screen
            /*with(builder.build()) {

                val width = context!!.resources.displayMetrics.widthPixels
                val height = context.resources.displayMetrics.heightPixels
                val padding =
                    (width * 0.01).toInt() // offset from edges of the map 10% of screen
                val cu =
                    CameraUpdateFactory.newLatLngBounds(this, width, height, padding)
               // map!!.isMyLocationEnabled = true
               // map!!.animateCamera(CameraUpdateFactory.newLatLngZoom(pickupLatLong, 10.5f))
            }*/

        }

    }
    var mPermission = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun onTelephoneClicked(number :String) {
        //AppConstant.TrackingEvent(AnalyticsConstant.call, AnalyticsConstant.userAction, AnalyticsConstant.callClickLabel)
        AlertDialog.showDialogWithHeaderTwoButtonForCall(this, "Call", number) { o, position ->
            if (position == 0) {
            } else {
                if (ContextCompat.checkSelfPermission(this@CompleteActivity, mPermission[0]) !== PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this@CompleteActivity, mPermission, 900)
                } else {
                    val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + number))
                    startActivity(intent)
                }
            }
        }
    }
    fun addTime(hour: Int, minute: Int, minutesToAdd: Int): String? {
        val calendar: Calendar = GregorianCalendar(1990, 1, 1, hour, minute)
        calendar.add(Calendar.MINUTE, minutesToAdd)
        val sdf = SimpleDateFormat("HH:mm")
        return sdf.format(calendar.time)
    }

    fun minusTime(hour: Int, minute: Int, minutesToAdd: Int): String? {

        val calendar: Calendar = GregorianCalendar(1990, 1, 1, hour, minute)
        calendar.add(Calendar.MINUTE, -minutesToAdd)
        val sdf = SimpleDateFormat("HH:mm")
        return sdf.format(calendar.time)
    }
    private fun setMap(map: MapView, savedInstanceState: Bundle?) {

        map.onCreate(savedInstanceState)
        map.onResume()
        map.getMapAsync(OnMapReadyCallback { googleMap ->
            this.googleMap = googleMap
            this.googleMap!!.setMapType(GoogleMap.MAP_TYPE_NORMAL)
            MapsInitializer.initialize(this)
            if (ActivityCompat.checkSelfPermission(
                    this!!,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) !== PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this!!,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) !== PackageManager.PERMISSION_GRANTED
            ) {

                return@OnMapReadyCallback
            }

            //this.googleMap!!.setMyLocationEnabled(true)
            this.googleMap!!.setMapType(GoogleMap.MAP_TYPE_NORMAL)
            val builder = LatLngBounds.Builder()
            val pickupLoc = Location("pickup")
            with (pickupLoc){
                latitude = job.pickupLocation!!.lat!!
                longitude = job.pickupLocation!!.lon!!
            }
            val deliveryLoc= Location("delivery")
            with (deliveryLoc){
                latitude = job.deliveryLocation!!.lat!!
                longitude = job.deliveryLocation!!.lon!!
            }
            val origin = pickupLoc.latitude.toString().plus(",").plus(pickupLoc.longitude.toString())
            val destination = deliveryLoc.latitude.toString().plus(",").plus(deliveryLoc.longitude.toString())
            // fetchDirections(origin,destination,"")
            val deliveryLatLong =
                LatLng(
                    deliveryLoc.latitude,
                    deliveryLoc.longitude
                )
            val deliveryTitle =
                "Delivery to : " + job.deliveryLocation!!.name
            if (deliveryTitle != null) {
                val deliveryMarkerOptions = MarkerOptions()
                    .title("To")
                    .icon(bitmapDescriptorFromVector(this,R.drawable.ic_marker))
                    .position(deliveryLatLong)
                val dd: Marker = this.googleMap!!.addMarker(deliveryMarkerOptions)
                builder.include(deliveryMarkerOptions.position)
                //dd.showInfoWindow();
            }
            val pickupLatLong =
                LatLng(
                    pickupLoc.latitude,
                    pickupLoc.longitude
                )


            val pickupTitle = "Pickup from : " + job.pickupLocation!!.name
            if (pickupTitle != null) {
                val pickupMarkerOptions = MarkerOptions()
                    .title("From")
                    .icon(bitmapDescriptorFromVector(this,R.drawable.ic_marker))
                    .position(pickupLatLong)
                builder.include(pickupMarkerOptions.position)
                val mm: Marker = this.googleMap!!.addMarker(pickupMarkerOptions)
                // mm.showInfoWindow();
            }



            val builderBounds = LatLngBounds.Builder()
            builderBounds.include(pickupLatLong)
            builderBounds.include(deliveryLatLong)
            val bounds = builder.build()
            val width = resources.displayMetrics.widthPixels
            val height = resources.displayMetrics.heightPixels
            val padding =
                (width * 0.25).toInt() // offset from edges of the map 10% of screen
            val cu =
                CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding)

            this.googleMap!!.animateCamera(cu);
            //this.googleMap!!.setMyLocationEnabled(true)
            //this.googleMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(pickupLatLong, 10.5f))
            this.googleMap!!.setOnMapClickListener(GoogleMap.OnMapClickListener {
                val intent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(
                        "http://maps.google.com/maps?saddr=" + pickupLoc.latitude
                            .toString() + "," + pickupLoc.longitude
                            .toString() + "&daddr=" + deliveryLoc.longitude
                            .toString() + "," + deliveryLoc.longitude
                    )
                )
                startActivity(intent)
            })
        })
    }
    private fun bitmapDescriptorFromVector(context: Context, @DrawableRes vectorDrawableResourceId: Int): BitmapDescriptor? {
        val background =
            ContextCompat.getDrawable(context, R.drawable.ic_marker)
        background!!.setBounds(0, 0, background.intrinsicWidth, background.intrinsicHeight)
        val vectorDrawable = ContextCompat.getDrawable(context, vectorDrawableResourceId)
        vectorDrawable!!.setBounds(
            23,
            23,
            vectorDrawable.intrinsicWidth + 23,
            vectorDrawable.intrinsicHeight + 23
        )
        val bitmap = Bitmap.createBitmap(
            background.intrinsicWidth,
            background.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        // background.draw(canvas)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }
    fun SetUpView(){
        val timeText: String = job.timer.timer
        val times = timeText.split(":").toTypedArray()

        /**CONVERT TIME TO SECOND */
        val second = times[0].toInt() * 60 * 60 + times[1]
            .toInt() * 60 + times[2].toInt()

        /**CONVERT TIME TO millisecond */
        var millisecond = second * 1000

        var addmilli = (job.additionalTimeOnJob * 60) * 1000

        millisecond = millisecond + addmilli
        var startTime = SystemClock.uptimeMillis() - millisecond
        var timeInMillies = SystemClock.uptimeMillis() - startTime
        timeInMillies = SystemClock.uptimeMillis() - startTime
        val finalTime = timeInMillies
        val seconds = (finalTime / 1000).toInt() % 60
        val minutes = (finalTime / (1000 * 60) % 60).toInt()
        val hours = (finalTime / (1000 * 60 * 60)).toInt()
        totalTimeText.text = (String.format("%02d", hours) + ":" + String.format("%02d", minutes) + ":" + String.format("%02d", seconds))

    }
}



