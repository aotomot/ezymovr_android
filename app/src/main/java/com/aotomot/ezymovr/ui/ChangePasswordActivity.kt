package com.aotomot.ezymovr.ui

import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.aotomot.ezymovr.CustomAlert.AlertDialog
import com.aotomot.ezymovr.CustomAlert.OnItemClickListener
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.extFunctions.getAppSharedPrefs
import com.aotomot.ezymovr.extFunctions.password
import com.aotomot.ezymovr.extFunctions.userID
import com.aotomot.ezymovr.models.ChangePasswordModel
import com.aotomot.ezymovr.viewmodels.ChangePasswordViewModel
import com.aotomot.ezymovr.viewmodels.CompleteViewModel
import com.google.android.material.button.MaterialButton

class ChangePasswordActivity : AppCompatActivity() {
    /*@BindView(R.id.oldPasswordFloat)
    TextFieldBoxes oldPasswordFloat;*/
    var oldPasswordEditText: EditText? = null
    private lateinit var mViewModel: ChangePasswordViewModel
var passwordEditText: EditText? = null

    var confirmpasswordEditText: EditText? = null
    var update : MaterialButton? = null
    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }
    protected inline fun <VM : ViewModel> viewModelFactory(crossinline f: () -> VM) =
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(aClass: Class<T>):T = f() as T
        }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        mViewModel = ViewModelProvider(this, viewModelFactory {
            ChangePasswordViewModel(this)
        }).get(ChangePasswordViewModel::class.java)
        oldPasswordEditText = findViewById(R.id.oldPasswordEditText)
        passwordEditText = findViewById(R.id.passwordEditText)
        confirmpasswordEditText = findViewById(R.id.confirmEditText)
        update = findViewById(R.id.updateBtn)
        update!!.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                updateBtnClicked()
            }

        })
        mViewModel.updateResult.observe(this, Observer {
            this.getAppSharedPrefs().password(confirmpasswordEditText!!.getText().toString())
            AlertDialog.showDialogWithAlertHeaderSingleButton(this, "Update Success", it.message, object : OnItemClickListener {
                override fun onItemClick(o: Any?, position: Int) {
                    finish();
                }
            })
        })
        mViewModel.error.observe(this, Observer {
            AlertDialog.showDialogWithAlertHeaderSingleButton(this, "Update Failed", "Please try again later", object : OnItemClickListener {
                override fun onItemClick(o: Any?, position: Int) {
                    finish();
                }
            })
        })
        //AppConstant.TrackingFragmentOrActivity(AnalyticsConstant.resetPasswordScreen);
    }

    /*@OnClick(R.id.backButton)
    public void onBackButtonClicked() {
        onBackPressed();
    }*/

    fun updateBtnClicked() {
        if (confirmpasswordEditText!!.text.toString() == passwordEditText!!.text
                .toString()
        ) {
            val newPass = ChangePasswordModel(
                oldPasswordEditText!!.text.toString(),
                confirmpasswordEditText!!.text.toString()
            )
            mViewModel.UpdatePassword(newPass,getAppSharedPrefs().userID().toString())
           /* if (AppConstant.isValidPassword(confirmpasswordEditText!!.text.toString())) {
                NetworkManager.getInstance().changePassword(
                    ModelManager.getInstance().getLogin().getAuthorization(),
                    ChangePasswordModel(
                        oldPasswordEditText!!.text.toString(),
                        confirmpasswordEditText!!.text.toString()
                    ),
                    ModelManager.getInstance().getLogin().getUserId().toString() + ""
                )
            } else {
                confirmpasswordEditText!!.error =
                    "Password needs to be 8 characters long with 1 capital, 1 number and 1 unique character."
            }*/
        } else {
            confirmpasswordEditText!!.error = "New Password not matching Confirm Password"
        }
    } /*public void onEventMainThread(BasicEvent event) {
        */
    /*if (event == BasicEvent.CHANGE_PASSWORD_SUCCESS) {
            ModelManager.getInstance().removeUserLoginData();
            UserLogin userLogin = new UserLogin();
            userLogin.setPassword(confirmpasswordEditText.getText().toString());
            userLogin.setUsername(ModelManager.getInstance().getDriver().getEmail());
            ModelManager.getInstance().insertLoginData(userLogin);
            AlertDialog.showDialogWithAlertHeaderSingleButton(this, "Update Success", ModelManager.getInstance().getChangePassword().getMessage(), new OnItemClickListener() {
                @Override
                public void onItemClick(Object o, int position) {
                    finish();
                }
            });
        }

        if (event == BasicEvent.CHANGE_PASSWORD_FAILED_400) {
            if (ModelManager.getInstance().getChangePassword() != null) {
                AlertDialog.showDialogWithAlertHeaderSingleButton(this, "Update Failed", ModelManager.getInstance().getChangePassword().getMessage(), new OnItemClickListener() {
                    @Override
                    public void onItemClick(Object o, int position) {

                    }
                });
            } else {
                AlertDialog.showDialogWithAlertHeaderSingleButton(this, "Update Failed", "Please try again later", new OnItemClickListener() {
                    @Override
                    public void onItemClick(Object o, int position) {

                    }
                });
            }
        }

        if (event == BasicEvent.CHANGE_PASSWORD_FAILED) {
            AlertDialog.showDialogWithAlertHeaderSingleButton(this, "Update Failed", ModelManager.getInstance().getChangePassword().getMessage(), new OnItemClickListener() {
                @Override
                public void onItemClick(Object o, int position) {

                }
            });
        }*/
    /*
    }*/
}