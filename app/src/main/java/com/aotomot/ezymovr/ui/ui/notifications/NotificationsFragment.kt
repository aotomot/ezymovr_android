package com.aotomot.ezymovr.ui.ui.notifications

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.adapters.AlertNotificationListAdapter
import com.aotomot.ezymovr.extFunctions.gone
import com.aotomot.ezymovr.extFunctions.visible
import com.aotomot.ezymovr.ui.ui.home.HomeViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.fragment_notifications.*

class NotificationsFragment : Fragment() {

    private lateinit var notificationsViewModel: NotificationsViewModel
    private lateinit var alertList: RecyclerView
    private lateinit var layoutManager : LinearLayoutManager
    private lateinit var mAdapter: AlertNotificationListAdapter

    protected inline fun <VM : ViewModel> viewModelFactory(crossinline f: () -> VM) =
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(aClass: Class<T>):T = f() as T
        }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        notificationsViewModel = ViewModelProvider(this, viewModelFactory {
                NotificationsViewModel(requireActivity() as AppCompatActivity)
            }).get(NotificationsViewModel::class.java)

        val root = inflater.inflate(R.layout.fragment_notifications, container, false)

        layoutManager = LinearLayoutManager(this.context)
        mAdapter = AlertNotificationListAdapter(activity as AppCompatActivity, mutableListOf())
        alertList = root.findViewById<RecyclerView>(R.id.listview)
        alertList.layoutManager = layoutManager
        alertList.adapter = mAdapter


        return root
    }
    override fun onResume() {
        super.onResume()
        notificationsViewModel.fetchAlertsNotification()
        notificationsViewModel.notifications_LiveData.observe(this, Observer {
            if (loading_ui?.visibility == View.VISIBLE)
                loading_ui?.visibility = View.GONE


            (it.sortedWith(compareBy({ it.createdDate }))).reversed()
            if(it.size>0) {
                showNoAlerts(false,"")
                mAdapter.update( (it.sortedWith(compareBy({ it.createdDate }))).reversed().toMutableList())
                for (index in 0..it.size.minus(1))
                    notificationsViewModel.markAsRead(it.get(index).id.toString())


                val nav_view = activity!!.findViewById<BottomNavigationView>(R.id.nav_view)
                val menuItemId: Int = nav_view.getMenu().getItem(2).getItemId() //2 menu item index.
                nav_view.getBadge(menuItemId)?.let { badgeDrawable ->
                    if (badgeDrawable.isVisible)  // check whether the item showing badge
                        nav_view.removeBadge(menuItemId)  //  remove badge notification
                }


            }
            else
                showNoAlerts(true,"")

        })

        notificationsViewModel.error.observe(this, Observer {
            if (loading_ui?.visibility == View.VISIBLE)
                loading_ui?.visibility = View.GONE

            if(it.equals("Internet Error"))
                showNoAlerts(true,"Seems you are Offline.")
            else
                showNoAlerts(true,"")
        })
    }

    fun showNoAlerts(noAlerts : Boolean, msg :String?){
        if(noAlerts){
            alert_icons.visible()

            if(!msg.isNullOrBlank())
                emptyView.setText(msg)

            emptyView.visible()
        }else{
            alert_icons.gone()
            emptyView.gone()
        }

    }
}
