package com.aotomot.ezymovr.ui

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.graphics.Rect
import android.media.ExifInterface
import android.net.Uri
import androidx.lifecycle.Observer
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView.OnEditorActionListener
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.models.AssignToJobModel
import com.aotomot.ezymovr.models.ImagePUTTitleComment
import com.aotomot.ezymovr.viewmodels.CompleteViewModel
import com.aotomot.ezymovr.viewmodels.SendPhotoViewModel
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.*
import java.text.SimpleDateFormat
import java.util.*

class SendGalleryPhotoActivity : AppCompatActivity() {
    private var uploadedImageID: Int = 0
    private lateinit var mViewModel: SendPhotoViewModel
    private lateinit var commentEt: EditText
    private lateinit var title: EditText
    private lateinit var submitButton: Button
    private lateinit var bgImg: ImageView
    private lateinit var isAlbum: String
    private lateinit var imageUri: Uri
    private lateinit var alertDialog: AlertDialog
    private lateinit var bitmap: Bitmap
    lateinit var file: File
    var jobId: Long = 0
    private lateinit var loader: RelativeLayout
    lateinit var buttonCancel: Button

    enum class ScalingLogic {
        CROP, FIT
    }
    protected inline fun <VM : ViewModel> viewModelFactory(crossinline f: () -> VM) =
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(aClass: Class<T>):T = f() as T
        }
    @SuppressLint("NewApi", "WrongThread")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.send_gallery_photo_activity)
        mViewModel = ViewModelProvider(this, viewModelFactory {
            SendPhotoViewModel(this)
        }).get(SendPhotoViewModel::class.java)
        loader = findViewById(R.id.loader)
        initializeViews()
        buttonCancel = findViewById(R.id.buttonCancel)
        jobId = intent.getLongExtra("jobId", 0)
        val b = intent.extras
        if (b != null) {
            isAlbum = b.getString("album")!!
            var fos: FileOutputStream? = null
            if (isAlbum != null && isAlbum == "yes") {
                /**
                 * gallery
                 */
                imageUri = b["imageUri"] as Uri
                var exif: ExifInterface? = null
                try {
                    val inputStream =
                        contentResolver.openInputStream(imageUri!!)
                    exif = ExifInterface(inputStream)
                    //                    try (InputStream inputStream = getContentResolver().openInputStream(imageUri)) {
//                        exif = new ExifInterface(inputStream);
//                        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }

//                    exif = new ExifInterface(getPaths(this, imageUri));
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                val orientation = exif!!.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED
                )
                try {
                    bitmap = getThumbnail(imageUri)!!
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                var rotateBitmap: Bitmap? = null
                val matrix = Matrix()
                if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                    matrix.setRotate(90f)
                    rotateBitmap = Bitmap.createBitmap(
                        bitmap!!,
                        0,
                        0,
                        bitmap!!.width,
                        bitmap!!.height,
                        matrix,
                        true
                    )
                    bitmap = rotateBitmap
                    bgImg!!.setImageBitmap(bitmap)
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                    matrix.setRotate(270f)
                    rotateBitmap = Bitmap.createBitmap(
                        bitmap!!,
                        0,
                        0,
                        bitmap!!.width,
                        bitmap!!.height,
                        matrix,
                        true
                    )
                    bitmap = rotateBitmap
                    bgImg!!.setImageBitmap(bitmap)
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                    matrix.setRotate(180f)
                    rotateBitmap = Bitmap.createBitmap(
                        bitmap!!,
                        0,
                        0,
                        bitmap!!.width,
                        bitmap!!.height,
                        matrix,
                        true
                    )
                    bitmap = rotateBitmap
                    bgImg!!.setImageBitmap(bitmap)
                } else {
                    bgImg!!.setImageBitmap(bitmap)
                }
                /**
                 * prepare file to send to backend
                 */
                val timeStamp =
                    SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
                val imageFileName = "JPEG_" + timeStamp + "_"
                val storageDir =
                    Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES
                    )
                try {
                    file = File.createTempFile(
                        imageFileName,  /* prefix */
                        ".jpg",  /* suffix */
                        storageDir /* directory */
                    )
                } catch (e: IOException) {
                    e.printStackTrace()
                }

                //Convert bitmap to byte array
                val bos = ByteArrayOutputStream()
                bitmap!!.compress(Bitmap.CompressFormat.JPEG, 80 /*ignored for PNG*/, bos)
                val bitmapdata = bos.toByteArray()
                //write the bytes in file
                try {
                    fos = FileOutputStream(file)
                    fos.write(bitmapdata)
                    fos.flush()
                    fos.close()
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            } else {
                /**
                 * camera
                 */

                //gets the file path from the intent
                imageUri = b["imageUri"] as Uri
                val path = getPath(imageUri)

                //loads the file
                bitmap = BitmapFactory.decodeFile(path)
                bgImg!!.setImageBitmap(bitmap)
                /**
                 * prepare file to send to backend
                 */
                val timeStamp =
                    SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
                val imageFileName = "JPEG_" + timeStamp + "_"
                val storageDir =
                    Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES
                    )
                try {
                    file = File.createTempFile(
                        imageFileName,  /* prefix */
                        ".jpg",  /* suffix */
                        storageDir /* directory */
                    )
                } catch (e: IOException) {
                    e.printStackTrace()
                }

                //Convert bitmap to byte array
                val bos = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80 /*ignored for PNG*/, bos)
                val bitmapdata = bos.toByteArray()

                //write the bytes in file
                try {
                    fos = FileOutputStream(file)
                    fos.write(bitmapdata)
                    fos.flush()
                    fos.close()
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
            submitButton!!.setOnClickListener {
                loader.setVisibility(View.VISIBLE)
                if (imageUri != null) {
                    if (intent.getBooleanExtra("signature", false)) {
                        mViewModel.signJob(jobId.toString(),commentEt.text.toString())
                        //NetworkManager.getInstance().signJob(ModelManager.getInstance().getLogin().getAuthorization(), jobId + "", commentEt.getText().toString());
                    }
                    val requestFile =
                        RequestBody.create(MediaType.parse("multipart/form-data"), file)
                    val body =
                        MultipartBody.Part.createFormData("file", file!!.name, requestFile)
                    mViewModel.postImage(body,commentEt.text.toString())
                    // NetworkManager.getInstance().postUploadImage(ModelManager.getInstance().getLogin().getAuthorization(), body);
                }
            }
        }
        this.buttonCancel.setOnClickListener(View.OnClickListener { finish() })
        mViewModel.signResult.observe(this, Observer{
            Log.d("","")
        })
        mViewModel.postImageREsult.observe(this, Observer{

            val body = ImagePUTTitleComment(commentEt.text.toString(),title.text.toString(),jobId.toString())
            uploadedImageID = it.id
            mViewModel.postImageDetails(jobId.toString(),body)
        })
        mViewModel.putImageResult.observe(this, Observer{

            val body = AssignToJobModel(uploadedImageID.toString(),jobId.toString())
            mViewModel.assignToJob(body)
        })
        mViewModel.assignToJobResult.observe(this, Observer{

           onBackPressed()
        })
        mViewModel.error.observe(this, Observer{

           Log.d("","")
        })
    }

    private fun getPath(selectedImage: Uri?): String {
        val filePathColumn =
            arrayOf(MediaStore.Images.Media.DATA)
        val cursor = contentResolver.query(
            selectedImage!!, filePathColumn, null, null, null
        )
        var filePath = ""
        var columnIndex = 0
        if (cursor!!.moveToFirst()) {
            columnIndex = cursor.getColumnIndex(filePathColumn[0])
            filePath = cursor.getString(columnIndex)
            cursor.close()
        }
        return filePath
    }

    @Throws(IOException::class)
    fun getThumbnail(uri: Uri?): Bitmap? {
        val THUMBNAIL_SIZE = 768
        var input = contentResolver.openInputStream(uri!!)
        val onlyBoundsOptions = BitmapFactory.Options()
        onlyBoundsOptions.inJustDecodeBounds = true
        onlyBoundsOptions.inDither = true // optional
        onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888 // optional
        BitmapFactory.decodeStream(input, null, onlyBoundsOptions)
        input!!.close()
        if (onlyBoundsOptions.outWidth == -1
            || onlyBoundsOptions.outHeight == -1
        ) return null
        val originalSize =
            if (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) onlyBoundsOptions.outHeight else onlyBoundsOptions.outWidth
        var ratio :Double = 0.0
        if(originalSize > THUMBNAIL_SIZE)
            ratio = (originalSize / THUMBNAIL_SIZE).toDouble()
        else
            ratio = 1.0

        val bitmapOptions = BitmapFactory.Options()
        bitmapOptions.inSampleSize =
            getPowerOfTwoForSampleRatio(ratio)
        bitmapOptions.inDither = true // optional
        bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888 // optional
        input = contentResolver.openInputStream(uri)
        val bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions)
        input!!.close()
        return bitmap
    }

    fun closeClicked(vw: View?) {
        finish()
    }

    private fun initializeViews() {
        commentEt = findViewById(R.id.editTextSendPhoto)
        title = findViewById(R.id.title)
        if (intent.getBooleanExtra(
                "signature",
                false
            ) || intent.getBooleanExtra("signatureOnprog", false)
        ) {
            title.setText("Signature")
            val currentTime = Calendar.getInstance().time
            val date =
                SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.getDefault())
                    .format(Date())
            commentEt.setText(date)
        } else {
            title.setText("")
            commentEt.setText("")
        }
        title.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                val `in` =
                    getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

                // NOTE: In the author's example, he uses an identifier
                // called searchBar. If setting this code on your EditText
                // then use v.getWindowToken() as a reference to your
                // EditText is passed into this callback as a TextView
                `in`.hideSoftInputFromWindow(
                    title.getApplicationWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS
                )

                // Must return true here to consume event
                return@OnEditorActionListener true
            }
            false
        })
        commentEt.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                val `in` =
                    getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

                // NOTE: In the author's example, he uses an identifier
                // called searchBar. If setting this code on your EditText
                // then use v.getWindowToken() as a reference to your
                // EditText is passed into this callback as a TextView
                `in`.hideSoftInputFromWindow(
                    commentEt
                        .getApplicationWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS
                )


                // Must return true here to consume event
                return@OnEditorActionListener true
            }
            false
        })
        submitButton = findViewById(R.id.buttonSendPhoto)
        bgImg = findViewById(R.id.imageViewBg)
    }

    override fun onPause() {
       /* if (alertDialog != null) {
            if(alertDialog.isShowing)
                alertDialog.dismiss()
        }*/
        super.onPause()
        //EventBus.getDefault().unregister(this);
    }

    public override fun onStop() {
        super.onStop()
        //EventBus.getDefault().unregister(this);
    }

    public override fun onResume() {
        super.onResume()
        //EventBus.getDefault().register(this);
    } /*public void onEventMainThread(BasicEvent event) {

        if (event == BasicEvent.SIGN_JOB_SUCCESS) {
            if (getIntent().getBooleanExtra("fromcomplete", false)) {
                try {
                    CompleteWorkDetailActivity.setSignButtonDisabled();
                } catch (Exception e) {

                }
            } else {
                try {
                    NewOnProgressFragment.setSignButtonDisabled();
                } catch (Exception e) {

                }
            }
        }
        if (event == BasicEvent.UPLOAD_IMAGE_SUCCESS) {
            ImagePUTTitleComment imagePUTTitleComment = new ImagePUTTitleComment(commentEt.getText().toString(), title.getText().toString(), jobId + "");
            NetworkManager.getInstance().putImageTitleAndComment(ModelManager.getInstance().getLogin().getAuthorization(), ModelManager.getInstance().getUploadImage().getId(), imagePUTTitleComment);
        }

        if (event == BasicEvent.PUT_IMAGE_SUCCESS) {
            AssignToJobModel assignToJobModel = new AssignToJobModel(ModelManager.getInstance().getUploadImage().getId(), jobId + "");
            NetworkManager.getInstance().assignImageToJob(ModelManager.getInstance().getLogin().getAuthorization(), assignToJobModel);
        }

        if (event == BasicEvent.PATCH_IMAGE_ASSIGN_SUCCESS) {
            loader.setVisibility(View.GONE);
            com.aotomot.dms.Util.CustomAlert.AlertDialog.showDialogWithAlertHeaderSingleButton(this, "Upload Success", "Document file has been successfully submitted", new OnItemClickListener() {
                @Override
                public void onItemClick(Object o, int position) {

                    if (getIntent().getBooleanExtra("signatureOnprog", false) || getIntent().getBooleanExtra("signature", false)) {

                    } else {
//                        SharedPreferences.Editor finishedImageUpload = getSharedPreferences("IMAGE_UPLOAD_DONE", Context.MODE_PRIVATE).edit();
//                        finishedImageUpload.putBoolean("imageUploadFinished", true);
//                        finishedImageUpload.commit();
                    }
                    buttonCancel.performClick();
                }
            });
        }
    }*/

    companion object {
        fun getPaths(context: Context, uri: Uri): String? {

            //check here to KITKAT or new version
            val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT

            // DocumentProvider
            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {

                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split = docId.split(":").toTypedArray()
                    val type = split[0]
                    if ("primary".equals(type, ignoreCase = true)) {
                        return Environment.getExternalStorageDirectory()
                            .toString() + "/" + split[1]
                    }
                } else if (isDownloadsDocument(uri)) {
                    val id = DocumentsContract.getDocumentId(uri)
                    val contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://<span id=\"IL_AD1\" class=\"IL_AD\">downloads</span>/public_downloads"),
                        java.lang.Long.valueOf(id)
                    )
                    return getDataColumn(
                        context,
                        contentUri,
                        null,
                        null
                    )
                } else if (isMediaDocument(uri)) {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split = docId.split(":").toTypedArray()
                    val type = split[0]
                    var contentUri: Uri? = null
                    if ("image" == type) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    } else if ("video" == type) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                    } else if ("audio" == type) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                    }
                    val selection = "_id=?"
                    val selectionArgs = arrayOf(
                        split[1]
                    )
                    return getDataColumn(
                        context,
                        contentUri,
                        selection,
                        selectionArgs
                    )
                }
            } else if ("content".equals(uri.scheme, ignoreCase = true)) {

                // Return the remote address
                return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(
                    context,
                    uri,
                    null,
                    null
                )
            } else if ("file".equals(uri.scheme, ignoreCase = true)) {
                return uri.path
            }
            return null
        }

        fun getDataColumn(
            context: Context, uri: Uri?, selection: String?,
            selectionArgs: Array<String>?
        ): String? {
            var cursor: Cursor? = null
            val column = "_data"
            val projection = arrayOf(
                column
            )
            try {
                cursor = context.contentResolver.query(
                    uri!!, projection, selection, selectionArgs,
                    null
                )
                if (cursor != null && cursor.moveToFirst()) {
                    val index = cursor.getColumnIndexOrThrow(column)
                    return cursor.getString(index)
                }
            } finally {
                cursor?.close()
            }
            return null
        }

        fun isExternalStorageDocument(uri: Uri): Boolean {
            return "com.android.externalstorage.documents" == uri.authority
        }

        fun isDownloadsDocument(uri: Uri): Boolean {
            return "com.android.providers.downloads.documents" == uri.authority
        }

        fun isMediaDocument(uri: Uri): Boolean {
            return "com.android.providers.media.documents" == uri.authority
        }

        fun isGooglePhotosUri(uri: Uri): Boolean {
            return "com.google.android.apps.photos.content" == uri.authority
        }

        /**
         * Calculates source rectangle for scaling bitmap
         */
        fun calculateSrcRect(
            srcWidth: Int,
            srcHeight: Int,
            dstWidth: Int,
            dstHeight: Int,
            scalingLogic: ScalingLogic
        ): Rect {
            return if (scalingLogic == ScalingLogic.CROP) {
                val srcAspect = srcWidth.toFloat() / srcHeight.toFloat()
                val dstAspect = dstWidth.toFloat() / dstHeight.toFloat()
                if (srcAspect > dstAspect) {
                    val srcRectWidth = (srcHeight * dstAspect).toInt()
                    val srcRectLeft = (srcWidth - srcRectWidth) / 2
                    Rect(
                        srcRectLeft,
                        0,
                        srcRectLeft + srcRectWidth,
                        srcHeight
                    )
                } else {
                    val srcRectHeight = (srcWidth / dstAspect).toInt()
                    val scrRectTop = (srcHeight - srcRectHeight) / 2
                    Rect(
                        0,
                        scrRectTop,
                        srcWidth,
                        scrRectTop + srcRectHeight
                    )
                }
            } else {
                Rect(0, 0, srcWidth, srcHeight)
            }
        }

        private fun getPowerOfTwoForSampleRatio(ratio: Double): Int {
            val k = Integer.highestOneBit(Math.floor(ratio).toInt())
            return if (k == 0) 1 else k
        }
    }
}