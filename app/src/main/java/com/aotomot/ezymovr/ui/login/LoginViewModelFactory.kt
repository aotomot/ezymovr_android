package com.aotomot.ezymovr.ui.login

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.aotomot.ezymovr.Repository.CommonRepository
import com.aotomot.ezymovr.WebService.WebServiceAPIFactory
import com.aotomot.ezymovr.extFunctions.domain
import com.aotomot.ezymovr.extFunctions.getAppSharedPrefs
import com.aotomot.ezymovr.ui.data.LoginDataSource
import com.aotomot.ezymovr.ui.data.LoginRepository

/**
 * ViewModel provider factory to instantiate LoginViewModel.
 * Required given LoginViewModel has a non-empty constructor
 */
class LoginViewModelFactory(private val context : AppCompatActivity) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            return LoginViewModel(
                CommonRepository(
                    WebServiceAPIFactory.webServicebApi,
                    context,
                    context.getAppSharedPrefs().domain()
                ),
                CommonRepository(
                    WebServiceAPIFactory.webServiceApi_driver,
                    context,
                    context.getAppSharedPrefs().domain()
                )
            ) as T

            /*return LoginViewModel(
                loginRepository = LoginRepository(
                    dataSource = LoginDataSource()
                )
            ) as T*/
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
