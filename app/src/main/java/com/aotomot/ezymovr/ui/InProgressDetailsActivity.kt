package com.aotomot.ezymovr.ui

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.aotomot.ezymovr.AppConstants
import com.aotomot.ezymovr.CustomAlert.AlertDialog
import com.aotomot.ezymovr.CustomAlert.OnItemClickListener
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.adapters.AttachListAdapter
import com.aotomot.ezymovr.adapters.EventAdapter
import com.aotomot.ezymovr.adapters.TimerAdapter
import com.aotomot.ezymovr.extFunctions.gone
import com.aotomot.ezymovr.extFunctions.returnDashIfEmpty
import com.aotomot.ezymovr.extFunctions.visible
import com.aotomot.ezymovr.models.AttachmentItem
import com.aotomot.ezymovr.models.JobHistory
import com.aotomot.ezymovr.models.Jobs
import com.aotomot.ezymovr.viewmodels.CompleteViewModel
import com.aotomot.homeworld.models.HomeDirections
import com.bumptech.glide.Glide
import com.github.gcacace.signaturepad.views.SignaturePad
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.google.android.material.button.MaterialButton
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.ByteArrayOutputStream
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class InProgressDetailsActivity : AppCompatActivity() {

    private var isCompleted: Boolean = false
    private lateinit var decline_bottom_button: MaterialButton
    private var bAttachmentFetched: Boolean = false
    private lateinit var jobStatusText: TextView
    private lateinit var totalTimeText: TextView
    private lateinit var job_no: TextView
    private lateinit var startdate_text: TextView
    private lateinit var starttime_text: TextView
    private lateinit var bedroom_text: TextView
    private lateinit var helper_text: TextView
    private lateinit var cust_name_text: TextView
    private lateinit var cust_phone_text: TextView
    private lateinit var cust_pick_text: TextView
    private lateinit var cust_deliver_text: TextView
    private lateinit var cust_notes_text: TextView
    private lateinit var saveText: TextView
    private lateinit var bedroom: TextView
    private lateinit var helper: TextView
    private lateinit var get_started: MaterialButton
    private lateinit var mapView: MapView
    private lateinit var signature_pad: SignaturePad

    private lateinit var job: Jobs
    private lateinit var jobTimer: String
    private lateinit var sign_layout: ConstraintLayout
    private lateinit var constraintLayout4: ConstraintLayout
    private lateinit var accept_decline_layout: ConstraintLayout
    private lateinit var loading: ProgressBar
    private lateinit var timer : TextView
    private lateinit var totalTimeLayout: ConstraintLayout
    private lateinit var start_stop_layout : ConstraintLayout
    private lateinit var startLayout : ConstraintLayout
    private lateinit var stopLayout : ConstraintLayout
    private lateinit var pauseLayout : ConstraintLayout
    private var jobStatus: String = ""
    private lateinit var mTimerAdapter: TimerAdapter
    private lateinit var mEventAdapter: EventAdapter
    private lateinit var timerlist: RecyclerView
    private lateinit var mViewModel: CompleteViewModel
    private var jobID: Int = 0
    private lateinit var attachlist: RecyclerView
    private lateinit var eventlist: RecyclerView
    private lateinit var mAdapter: AttachListAdapter
    private var startTime = 0L
    private val myHandler = Handler()
    var timeInMillies = 0L
    var timeSwap = 0L
    var finalTime = 0L
    private var is_paused = false
    var sdf = SimpleDateFormat("dd MMM yyyy h:mm a")

    private val updateTimerMethod: Runnable = object : Runnable {
        override fun run() {
            timeInMillies = SystemClock.uptimeMillis() - startTime
            finalTime = timeSwap + timeInMillies
            /**
             * display from backend to the timer
             */
            val seconds = (finalTime / 1000).toInt() % 60
            val minutes = (finalTime / (1000 * 60) % 60).toInt()
            val hours = (finalTime / (1000 * 60 * 60)).toInt()
            timer.text = (String.format("%02d", hours) + ":" + String.format("%02d", minutes) + ":" + String.format("%02d", seconds))
            myHandler.postDelayed(this, 0)
        }
    }
    //private lateinit var mViewModel: InProgressDetailsViewModel
    var map: GoogleMap? = null
    protected inline fun <VM : ViewModel> viewModelFactory(crossinline f: () -> VM) =
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(aClass: Class<T>):T = f() as T
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_in_progress_details)
        mViewModel = ViewModelProvider(this, viewModelFactory {
            CompleteViewModel(this)
        }).get(CompleteViewModel::class.java)
        mViewModel.fetchjobsList()
        jobID = intent.getIntExtra("jobID",0)
        jobStatus = intent.getStringExtra("status")!!
        jobTimer = intent.getStringExtra("timer")!!
        loading = findViewById<ProgressBar>(R.id.in_progress_loading)
        loading.visible()
        jobStatusText = findViewById<TextView>(R.id.job_status)
        saveText = findViewById<TextView>(R.id.saveText)
        job = AppConstants.JobsLists.filter { it.id == jobID }.single()
        job.timer.timer = jobTimer
        sign_layout = findViewById<ConstraintLayout>(R.id.signLayout)
        job_no = findViewById<TextView>(R.id.job_no)
        totalTimeText = findViewById<TextView>(R.id.cust_total_text)
        startdate_text = findViewById<TextView>(R.id.startdate_text)
        starttime_text = findViewById<TextView>(R.id.starttime_text)
        bedroom_text = findViewById<TextView>(R.id.bedroom_text)
        helper_text = findViewById<TextView>(R.id.helper_text)
        cust_name_text = findViewById<TextView>(R.id.cust_name_text)
        cust_phone_text = findViewById<TextView>(R.id.cust_phone_text)
        cust_pick_text = findViewById<TextView>(R.id.cust_pick_text)
        cust_deliver_text = findViewById<TextView>(R.id.cust_deliver_text)
        cust_notes_text = findViewById<TextView>(R.id.cust_notes_text)
        bedroom = findViewById<TextView>(R.id.bedroom_text)
        helper = findViewById<TextView>(R.id.helper_text)
        get_started = findViewById<MaterialButton>(R.id.get_started)
        timer = findViewById<TextView>(R.id.timer)
        start_stop_layout = findViewById<ConstraintLayout>(R.id.start_stop_layout)
        totalTimeLayout = findViewById<ConstraintLayout>(R.id.cust_total_layout)
        startLayout = findViewById<ConstraintLayout>(R.id.startLayout)
        stopLayout = findViewById<ConstraintLayout>(R.id.stopLayout)
        pauseLayout = findViewById<ConstraintLayout>(R.id.pauseLayout)
        timerlist = findViewById<RecyclerView>(R.id.timer_list)
        attachlist = findViewById<RecyclerView>(R.id.attach_list)
        eventlist = findViewById<RecyclerView>(R.id.event_list)
        mapView = findViewById<MapView>(R.id.mapView2)
        constraintLayout4 = findViewById<ConstraintLayout>(R.id.start_stop_layout)
        decline_bottom_button = findViewById<MaterialButton>(R.id.decline_bottom_btn)
        signature_pad = findViewById(R.id.signature_pad)


        mAdapter = AttachListAdapter(this, emptyList<AttachmentItem>().toMutableList())
        attachlist.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        attachlist.adapter = mAdapter

        mTimerAdapter = TimerAdapter(this, JobHistory())
        timerlist.layoutManager = LinearLayoutManager(this)
        timerlist.adapter = mTimerAdapter

        mEventAdapter = EventAdapter(this, mutableListOf())
        eventlist.layoutManager = LinearLayoutManager(this)
        eventlist.adapter = mEventAdapter
        val addTime = findViewById<TextView>(R.id.add_timer)
        val saveAddTime = findViewById<Button>(R.id.save_add_time)
        val currentTime = findViewById<TextView>(R.id.add_timer).text.toString()
        findViewById<ImageView>(R.id.plus_img).setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                val st1 = StringTokenizer(addTime.text.toString(), ":")
                var j = 0;
                var value = mutableListOf<Int>()
                // iterate through tokens
                for (index in 0..st1.countTokens().minus(1)) {
                    value.add(index,Integer.parseInt(st1.nextToken()))
                }

                // call time add method with current hour, minute and minutesToAdd,
                // return added time as a string
                val dateRevisedEstimatedDeliveryTime = addTime(value[0], value[1], 15);
                addTime.text = dateRevisedEstimatedDeliveryTime
                saveAddTime.isEnabled = true
            }

        })
        findViewById<ImageView>(R.id.minus_img).setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                val st1 = StringTokenizer(addTime.text.toString(), ":")
                var j = 0;
                var value = mutableListOf<Int>()
                // iterate through tokens
                for (index in 0..st1.countTokens().minus(1)) {
                    value.add(index,Integer.parseInt(st1.nextToken()))
                }

                // call time add method with current hour, minute and minutesToAdd,
                // return added time as a string
                val dateRevisedEstimatedDeliveryTime = minusTime(value[0], value[1], 15);
                addTime.text = dateRevisedEstimatedDeliveryTime
                saveAddTime.isEnabled = true
            }

        })

        saveAddTime.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                val st1 = StringTokenizer(addTime.text.toString(), ":")
                var totalminutes = (Integer.parseInt(st1.nextToken()) * 60)
                totalminutes = totalminutes + Integer.parseInt(st1.nextToken())
                loading.visible()
                mViewModel.AddTime(jobID.toString(),totalminutes,"",true)
                saveAddTime.isEnabled=false
                addTime.text = "00:00"
            }

        })

        accept_decline_layout = findViewById<ConstraintLayout>(R.id.accept_decline_layout)
        if(!AppConstants.driver.driverImage.url.isBlank()){
            Glide.with(this).asBitmap().load(AppConstants.driver.driverImage.url).into(findViewById(R.id.user_img))
        }


        mViewModel.fetchjobAttachmentList(jobID.toString())
        mViewModel.jobList.observe(this, Observer {
            job = it.filter { it: Jobs -> it.id == jobID}.single()
            mTimerAdapter.update(job.history)
            if(job.timer.events.size >0)
                mEventAdapter.update(job.timer.events?.toMutableList())

            populateJobs(job)
            SetUpView()

            if(!bAttachmentFetched)
                mViewModel.fetchjobAttachmentList(jobID.toString())

            AppConstants.JobsLists.clear()
            AppConstants.JobsLists.addAll(it.filter { jobs -> !jobs.declined }.toMutableList())

        })
        mViewModel.jobAttachmentList.observe(this, Observer {
            bAttachmentFetched = true
            mAdapter.update(it!!)
            attachlist.visible()
            loading.gone()
        })
        mViewModel.error.observe(this, Observer {
            attachlist.gone()
            loading.gone()
        })
        mViewModel.acceptJob.observe(this, Observer {
            /*jobStatusText.text = "Accepted"
            accept_decline_layout.gone()
            decline_bottom_button.visible()*/
            loading.visible()
            mViewModel.fetchjobsList()
            /*val intent = Intent(this, CompleteActivity::class.java)
            intent.putExtra("jobID", jobID)
            intent.putExtra("status","Completed" )
            startActivity(intent)*/

        })
        mViewModel.addTime.observe(this, Observer {
            mViewModel.fetchjobsList()
            Log.d("","")
        })
        mViewModel.addTimeResult.observe(this, Observer {
            loading.gone()
            AlertDialog.showDialogWithAlertHeaderSingleButton(this,
                "Error",
                "Something went wrong. Try again.",
                object: OnItemClickListener{
                    override fun onItemClick(o: Any?, position: Int) {
                        if(position == 0){
                            //addTime.text = 00
                        }
                    }

                })
        })
        mViewModel.completeJob.observe(this, Observer {
            mViewModel.fetchjobsList()
            /*val intent = Intent(this, CompleteActivity::class.java)
            intent.putExtra("jobID", jobID)
            intent.putExtra("status","Completed" )
            startActivity(intent)*/
            onBackPressed()
        })
        mViewModel.completeJobResult.observe(this, Observer {
            loading.gone()
            AlertDialog.showDialogWithAlertHeaderSingleButton(this,
                "Error",
                "Something went wrong. Try again.",
            object: OnItemClickListener{
                override fun onItemClick(o: Any?, position: Int) {
                    if(position == 0){
                        //AlertDialog.
                    }
                }

            })
        })
        mViewModel.startJob.observe(this, Observer {
            mViewModel.fetchjobsList()
        })
        mViewModel.declineJob.observe(this, Observer {
            loading.gone()
            finish()
            //mViewModel.fetchjobsList()
        })
        mViewModel.declineJobResult.observe(this, Observer {
            loading.gone()
            AlertDialog.showDialogWithAlertHeaderSingleButton(this,
                "Error",
                "Something went wrong. Try again.",
                object: OnItemClickListener{
                    override fun onItemClick(o: Any?, position: Int) {
                        if(position == 0){
                            mViewModel.fetchjobsList()
                        }
                    }

                })
        })
        mViewModel.pauseJob.observe(this, Observer {
            mViewModel.fetchjobsList()
        })

        mViewModel.directions.observe(this, Observer {
            setPolylinesAndMarkers(it)
        })
        //SetUpView()
        accept_decline_layout.findViewById<MaterialButton>(R.id.accept_btn).setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                /*AppConstant.TrackingEvent(
                    AnalyticsConstant.document,
                    AnalyticsConstant.userAction,
                    AnalyticsConstant.uploadDocumentClicked
                )*/
                loading.visible()
                mViewModel.AcceptJob(jobID.toString())
               // mViewModel.AcceptJob(jobID.toString())
                //finish()
            }

        })
        accept_decline_layout.findViewById<MaterialButton>(R.id.decline_btn).setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                /*AppConstant.TrackingEvent(
                    AnalyticsConstant.document,
                    AnalyticsConstant.userAction,
                    AnalyticsConstant.uploadDocumentClicked
                )*/
                loading.visible()
                mViewModel.DeclineJob(jobID.toString())
                finish()
            }

        })
        decline_bottom_button.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                /*AppConstant.TrackingEvent(
                    AnalyticsConstant.document,
                    AnalyticsConstant.userAction,
                    AnalyticsConstant.uploadDocumentClicked
                )*/
                loading.visible()
                mViewModel.DeclineJob(jobID.toString())
                finish()
            }

        })


        get_started.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                    /*AppConstant.TrackingEvent(
                        AnalyticsConstant.document,
                        AnalyticsConstant.userAction,
                        AnalyticsConstant.uploadDocumentClicked
                    )*/
                val intent = Intent(this@InProgressDetailsActivity, UploadDocumentActivity::class.java)
                    intent.putExtra("jobId", jobID)
                    startActivity(intent)
                //finish()
            }

        })

        findViewById<ImageView>(R.id.user_img).setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                val intent = Intent(this@InProgressDetailsActivity, UserProfileActivity::class.java)
                startActivity(intent)
            }

        })

        startLayout?.setOnClickListener(
            object : View.OnClickListener {
                override fun onClick(view: View) {
                    // AppConstants.TrackingEvent(AnalyticsConstant.job,AnalyticsConstant.pause, AnalyticsConstant.pauseJobClicked + jobList.getId())
                    if(job.startDateActual.compareTo(0).equals(0)) {
                        AlertDialog.showDialogWithoutHeaderTwoButtonLaunchWake(
                            this@InProgressDetailsActivity,
                            "Please Sign to start the timer.",
                            object : OnItemClickListener {
                                override fun onItemClick(o: Any?, position: Int) {
                                    if (position == 0) {
                                        /*AppConstant.TrackingEvent(
                                    AnalyticsConstant.job,
                                    AnalyticsConstant.continueJob,
                                    AnalyticsConstant.continueJobClicked + jobList.getId()
                                )*/
                                        is_paused = false
                                        loading.visible()
                                        sign_layout.visible()
                                    } else {
                                    }
                                }
                            })
                    }else{

                        AlertDialog.showDialogWithoutHeaderTwoButtonLaunchWake(
                            this@InProgressDetailsActivity,
                            "Please confirm you wish to start the timer.",
                            object : OnItemClickListener {
                                override fun onItemClick(o: Any?, position: Int) {
                                    if (position == 0) {
                                        /*AppConstant.TrackingEvent(
                                    AnalyticsConstant.job,
                                    AnalyticsConstant.continueJob,
                                    AnalyticsConstant.continueJobClicked + jobList.getId()
                                )*/
                                        mViewModel.startJob(jobID.toString(), "")
                                        startLayout.setVisibility(View.GONE)
                                        stopLayout.setVisibility(View.VISIBLE)
                                        pauseLayout.setVisibility(View.VISIBLE)
                                        //completeJobLayout.setVisibility(View.GONE);
                                        val timeText: String = job.timer.timer
                                        val times = timeText.split(":").toTypedArray()

                                        /**CONVERT TIME TO SECOND */
                                        val second = times[0].toInt() * 60 * 60 + times[1]
                                            .toInt() * 60 + times[2].toInt()

                                        /**CONVERT TIME TO millisecond */
                                        val millisecond = second * 1000
                                        startTime = SystemClock.uptimeMillis() - millisecond
                                        myHandler.postDelayed(updateTimerMethod, 0)
                                    } else {
                                    }
                                }
                            })

                    }

                       // }
                }
            })
        stopLayout?.setOnClickListener(
            object : View.OnClickListener {
                override fun onClick(view: View) {
                    pauseLayout.setVisibility(View.GONE)
                    startLayout.setVisibility(View.VISIBLE)
                    stopLayout.setVisibility(View.VISIBLE)
                    //completeJobLayout.setVisibility(View.GONE);
                    is_paused = true
                    if (job.timer.running.equals("false") ) {
                        /**
                         * dont call pause again because the job already in pause mode other wise it will be duplicate break
                         */
                    } else {
                        loading.visible()

                        mViewModel.PauseJob(job.id.toString(),"")
                        /*NetworkManager.getInstance().pauseTimer(
                            ModelManager.getInstance().getLogin().getAuthorization(),
                            jobList.getId().toString(),
                            false, ""
                        )*/
                    }
                    myHandler.removeCallbacks(updateTimerMethod)
                    AlertDialog.showDialogWithHeaderTwoButton(
                        this@InProgressDetailsActivity,
                        "Please sign to complete the job",
                        "Press OK to submit",
                        object : OnItemClickListener {
                            override fun onItemClick(o: Any?, position: Int) {
                                if (position == 0) {
                                    loading.visible()

                                   sign_layout.visible()

                                    //mViewModel.completeJob(job.id.toString())

                                    /* AppConstant.TrackingEvent(
                                         AnalyticsConstant.job,
                                         AnalyticsConstant.complete,
                                         AnalyticsConstant.completeJobClicked + jobList.getId()
                                     )*/
                                } else {
                                    //cancel do nothing
                                    pauseLayout.setVisibility(View.GONE)
                                    startLayout.setVisibility(View.VISIBLE)
                                    stopLayout.setVisibility(View.VISIBLE)
                                    //completeJobLayout.setVisibility(View.GONE);
                                }
                            }
                        })
                }
            })
        pauseLayout.setOnClickListener(
            object : View.OnClickListener {
                override fun onClick(view: View) {
                    /*AppConstant.TrackingEvent(
                        AnalyticsConstant.job,
                        AnalyticsConstant.pause,
                        AnalyticsConstant.pauseJobClicked + jobList.getId()
                    )*/
                    AlertDialog.showDialogWithoutHeaderTwoButtonLaunchWakeBreak(
                        this@InProgressDetailsActivity,
                        "Are you taking break?",
                        object : OnItemClickListener {
                            override fun onItemClick(o: Any?, position: Int) {
                                if (position == 0) {
                                    is_paused = true
                                    //completeJobLayout.setVisibility(View.GONE);
                                    myHandler.removeCallbacks(updateTimerMethod)
                                        loading.visible()
                                        mViewModel.PauseJob(jobID.toString(),AlertDialog.etName.text.toString())
                                       /* NetworkManager.getInstance().startOrPauseJob(
                                            ModelManager.getInstance().getLogin().getAuthorization(),
                                            jobList.getId().toString() + "",
                                            false,
                                            ""
                                        )*/

                                } else {

                                }
                            }
                        })
                    if (is_paused) {
                        pauseLayout.setVisibility(View.GONE)
                        startLayout.setVisibility(View.VISIBLE)
                        stopLayout.setVisibility(View.VISIBLE)
                    }
                }
            })




       with(mapView) {
            this.onCreate(savedInstanceState)
            this.onResume()
            this.getMapAsync(OnMapReadyCallback { googleMap ->
                map = googleMap
                map!!.setMapType(GoogleMap.MAP_TYPE_NORMAL)
                MapsInitializer.initialize(context)
                if (ActivityCompat.checkSelfPermission(
                        this.context!!,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) !== PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this.context!!,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) !== PackageManager.PERMISSION_GRANTED
                ) {

                    return@OnMapReadyCallback
                }

                map!!.setMyLocationEnabled(true)
                map!!.setMapType(GoogleMap.MAP_TYPE_NORMAL)
                val builder = LatLngBounds.Builder()
                val pickupLoc = Location("pickup")
                with (pickupLoc){
                    latitude = job.pickupLocation!!.lat!!
                    longitude = job.pickupLocation!!.lon!!
                }
                val deliveryLoc= Location("delivery")
                with (deliveryLoc){
                    latitude = job.deliveryLocation!!.lat!!
                    longitude = job.deliveryLocation!!.lon!!
                }
                val deliveryLatLong =
                    LatLng(
                        deliveryLoc.latitude,
                        deliveryLoc.longitude
                    )
                val deliveryTitle =
                    "Delivery to : " + job.deliveryLocation!!.name
                if (deliveryTitle != null) {
                    val deliveryMarkerOptions = MarkerOptions()
                        .title("To")
                        .icon(bitmapDescriptorFromVector(context!!,R.drawable.ic_marker))
                        .position(deliveryLatLong)
                    val dd: Marker = map!!.addMarker(deliveryMarkerOptions)
                    builder.include(deliveryMarkerOptions.position)
                    //dd.showInfoWindow();
                }
                val pickupLatLong =
                    LatLng(
                        pickupLoc.latitude,
                        pickupLoc.longitude
                    )


                val pickupTitle = "Pickup from : " + job.pickupLocation!!.name
                if (pickupTitle != null) {
                    val pickupMarkerOptions = MarkerOptions()
                        .title("From")
                        .icon(bitmapDescriptorFromVector(context!!,R.drawable.ic_marker))
                        .position(pickupLatLong)
                    builder.include(pickupMarkerOptions.position)
                    val mm: Marker = map!!.addMarker(pickupMarkerOptions)
                    // mm.showInfoWindow();
                }

                val origin = pickupLoc.latitude.toString().plus(",").plus(pickupLoc.longitude.toString())
                val destination = deliveryLoc.latitude.toString().plus(",").plus(deliveryLoc.longitude.toString())
                mViewModel.fetchDirections(origin,destination,"")

                val builderBounds = LatLngBounds.Builder()
                builderBounds.include(pickupLatLong)
                builderBounds.include(deliveryLatLong)
                val bounds = builder.build()
                val width = context!!.resources.displayMetrics.widthPixels
                val height = context!!.resources.displayMetrics.heightPixels
                val padding =
                    (width * 0.15).toInt() // offset from edges of the map 10% of screen
                val cu =
                    CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding)

                map!!.animateCamera(cu);
                map!!.setMyLocationEnabled(true)
                //map!!.animateCamera(CameraUpdateFactory.newLatLngZoom(pickupLatLong, 10.5f))
                map!!.setOnMapClickListener(GoogleMap.OnMapClickListener {
                    val intent = Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(
                            "http://maps.google.com/maps?saddr=" + pickupLoc.latitude
                                .toString() + "," + pickupLoc.longitude
                                .toString() + "&daddr=" + deliveryLoc.longitude
                                .toString() + "," + deliveryLoc.longitude
                        )
                    )
                    context.startActivity(intent)
                })
            })
            if (ActivityCompat.checkSelfPermission(this.context!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this.context!!,Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return@with
            }
        }
        loading.gone()
    }
    fun setPolylinesAndMarkers(mDirections : HomeDirections){
        if((mDirections != null) && (this.map!! != null)) {
            var polyline: LatLng
            val mPolylineOptions = PolylineOptions()
            val builder = LatLngBounds.Builder()
            if(mDirections.routes.size == 0){
                return
            }
            val selectedRoute = mDirections.routes.get(0)
            with(selectedRoute) {

                for (indexLegs in 0..legs.size.minus(1)) {
                    //Add Moarkers for Star & End Location
                    with(legs[indexLegs]) {
                       for (indexSteps in 0..legs[indexLegs].steps.size.minus(1)) {
                            with(legs[indexLegs].steps[indexSteps]) {
                                //Add to Polylin Options
                                mPolylineOptions.add(LatLng(startLocation.lat, startLocation.lng))
                                mPolylineOptions.add(LatLng(endLocation.lat, endLocation.lng))

                                //Add to Bounds
                                builder.include(LatLng(startLocation.lat, startLocation.lng))
                                builder.include(LatLng(endLocation.lat, endLocation.lng))
                            }
                        }
                    }
                }
            }
            //Draw Polyline
            val polylines = this.map!!.addPolyline(mPolylineOptions)
            polylines.color = android.graphics.Color.RED
            polylines.isGeodesic = true
            polylines.width = 12F

            //Include in the dispaly screen
            with(builder.build()) {

                val width = resources.displayMetrics.widthPixels
                val height = resources.displayMetrics.heightPixels
                val padding =
                    (width * 0.25).toInt() // offset from edges of the map 10% of screen
                val cu =
                    CameraUpdateFactory.newLatLngBounds(this, width, height, padding)
               // map!!.isMyLocationEnabled = true
               // map!!.animateCamera(CameraUpdateFactory.newLatLngZoom(pickupLatLong, 10.5f))
                map!!.animateCamera(cu)
            }

        }

    }
    override fun onResume() {
   //     mViewModel.fetchjobsList()
        super.onResume()
    }

    private fun bitmapDescriptorFromVector(context: Context, @DrawableRes vectorDrawableResourceId: Int): BitmapDescriptor? {
        val background =
            ContextCompat.getDrawable(context, R.drawable.ic_marker)
        background!!.setBounds(0, 0, background.intrinsicWidth, background.intrinsicHeight)
        val vectorDrawable = ContextCompat.getDrawable(context, vectorDrawableResourceId)
        vectorDrawable!!.setBounds(
            23,
            23,
            vectorDrawable.intrinsicWidth + 23,
            vectorDrawable.intrinsicHeight + 23
        )
        val bitmap = Bitmap.createBitmap(
            background.intrinsicWidth,
            background.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        // background.draw(canvas)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }
   fun drawTextToBitmap(mContext: Context, bitmaps: Bitmap, mText: String): Bitmap? {
        return try {
            val resources = mContext.resources
            val scale = resources.displayMetrics.density
            var bitmap = bitmaps
            var bitmapConfig = bitmap.config
            // set default bitmap config if none
            if (bitmapConfig == null) {
                bitmapConfig = Bitmap.Config.ARGB_8888
            }
            // resource bitmaps are imutable,
            // so we need to convert it to mutable one
            bitmap = bitmap.copy(bitmapConfig, true)
            val canvas = Canvas(bitmap)
            // new antialised Paint
            val paint =
                Paint(Paint.ANTI_ALIAS_FLAG)
            // text color - #3D3D3D
            paint.color = Color.rgb(110, 110, 110)
            // text size in pixels
            paint.setTextSize((15 * scale) as Float)
            // text shadow
            paint.setShadowLayer(1f, 0f, 1f, Color.DKGRAY)
            // draw text to the Canvas center
            val bounds = Rect()
            paint.getTextBounds(mText, 0, mText.length, bounds)
            /**
             * bitmap.getWidth()-bounds.right-20 --> meaning get bitmap width then minus the bound right and add another space width from rightby -20
             * bitmap.getHeight()-bounds.top-40 --> meaning get bitmap height then minus the bound top and add another space height from bottom by -40
             */
            //            canvas.drawText("Signed : " +mText, bitmap.getWidth()-bounds.right-200, bitmap.getHeight()-bounds.top-50, paint);
            /**
             *
             * x = 15 --> meaning get the canvas start from x = 15
             * y = bitmap.getHeight()-bounds.top-40 --> meaning get bitmap height then minus the bound top and add another space height from bottom by -40
             */
            canvas.drawText(
                "Signed : $mText",
                15f,
                bitmap.height - bounds.top - 50.toFloat(),
                paint
            )
            bitmap
        } catch (e: Exception) {
            null
        }
    }
    fun onTelephoneClicked(number :String) {
        //AppConstant.TrackingEvent(AnalyticsConstant.call, AnalyticsConstant.userAction, AnalyticsConstant.callClickLabel)
        AlertDialog.showDialogWithHeaderTwoButtonForCall(this, "Call", number) { o, position ->
            if (position == 0) {
            } else {
                if (ContextCompat.checkSelfPermission(this@InProgressDetailsActivity, mPermission[0]) !== PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this@InProgressDetailsActivity, mPermission, 900)
                } else {
                    val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + number))
                    startActivity(intent)
                }
            }
        }
    }
    var mPermission =
        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun onsaveTextClicked(id : Int, signaturePad : SignaturePad) {

        if (ContextCompat.checkSelfPermission(this!!.applicationContext,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) !== PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, mPermission,5)
        } else {
            var bitmap: Bitmap? = signaturePad.getSignatureBitmap()
            signaturePad.clear()
            val date =
                SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.getDefault())
                    .format(Date())
            bitmap = drawTextToBitmap(this, bitmap!!, date)
            if (bitmap == null) {
                Toast.makeText(this, "Please do the signature", Toast.LENGTH_SHORT).show()
            } else {
                val i = Intent(this, SendGalleryPhotoActivity::class.java)
                i.putExtra("imageUri", getImageUri(this, bitmap))
                i.putExtra("album", "no")
                i.putExtra("signature", true)
                i.putExtra("fromcomplete", false)
                i.putExtra("jobId", id.toLong())
                ContextCompat.startActivity(this!!, i, null)
            }
        }
    }
    fun getImageUri(inContext: Context, inImage: Bitmap): Uri? {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(inContext.contentResolver, inImage, "Title", null)
        return Uri.parse(path)
    }
    fun SetUpView(){
        if(!job.clientObj?.phone?.contactNumber.toString().isNullOrEmpty()){
            findViewById<ImageView>(R.id.call_btn).gone()
        }else{
            findViewById<ImageView>(R.id.call_btn).setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    onTelephoneClicked(job.clientObj?.phone?.contactNumber.toString())
                }
            })

        }

        if (job.timer.running.equals("true")){
            jobStatusText.text ="InProgress"
            jobStatusText.setTextColor(getColor(R.color.inprogress))
            accept_decline_layout.gone()
            decline_bottom_button.visible()
            val times = job.timer.timer.split(":")
            /**CONVERT TIME TO SECOND*/
            /**CONVERT TIME TO SECOND */
            val second =
                times[0].toInt() * 60 * 60 + times[1].toInt() * 60 + times[2]
                    .toInt()
            /**CONVERT TIME TO millisecond*/
            /**CONVERT TIME TO millisecond */
            val millisecond = second * 1000
            startTime = SystemClock.uptimeMillis() - millisecond
            myHandler.postDelayed(updateTimerMethod, 0)
            startLayout.visibility = View.GONE
            pauseLayout.visibility = View.VISIBLE
            stopLayout.visibility = View.VISIBLE
        }else if(!jobStatus.equals("Waiting") && !jobStatus.equals("Expired") && !jobStatus.equals("Accepted")){
            jobStatusText.text ="Break"
            jobStatusText.setTextColor(getColor(R.color.upcoming))
            accept_decline_layout.gone()
            decline_bottom_button.visible()
            val times = job.timer.timer.split(":")
            val mseconds = times[2].toInt()
            val mminutes = times[1].toInt()
            val mhours = times[0].toInt()
            timer?.text = (String.format("%02d", mhours)
                .plus(":")
                .plus(String.format("%02d", mminutes))
                .plus(":" )
                .plus( String.format("%02d", mseconds))
                    )
            startLayout?.visibility = View.VISIBLE
            pauseLayout.visibility = View.GONE
            stopLayout?.visibility = View.VISIBLE
        }else if(jobStatus.equals("Accepted")){
            with(jobStatusText){
                text = jobStatus
                setTextColor(getColor(R.color.accepted))}
            accept_decline_layout.gone()
            decline_bottom_button.visible()
            start_stop_layout.visible()
            pauseLayout.gone()
        }else if (jobStatus.equals("Waiting")){
            with(jobStatusText){
                text = jobStatus
                setTextColor(getColor(R.color.upcoming))}
                timer?.gone()
                start_stop_layout.gone()
                accept_decline_layout.visible()
                pauseLayout.visibility = View.GONE
                accept_decline_layout.findViewById<MaterialButton>(R.id.accept_btn).visible()
                accept_decline_layout.findViewById<MaterialButton>(R.id.decline_btn).visible()
            decline_bottom_button.gone()
        }else if(jobStatus.equals("Expired")) {
            with(jobStatusText){
                text = jobStatus
                setTextColor(getColor(R.color.expired))
            }
            (constraintLayout4).gone()
        }else if(jobStatus.equals("Upcoming")) {
            with(jobStatusText){
                text = jobStatus
                setTextColor(getColor(R.color.upcoming))
                if(job.accepted){
                    accept_decline_layout.gone()
                    decline_bottom_button.visible()
                }
                accept_decline_layout.visible()
                decline_bottom_button.gone()
            }
        }

        var jobDate : Date? = null
        var jobTime : Date? = null
        val joinDatesdf = SimpleDateFormat(getString(R.string.start_only_date_sdf))
        val joinTimedf = SimpleDateFormat(getString(R.string.start_time_sdf))
        try {
            jobDate = joinDatesdf.parse(AppConstants.Epoch2DateStringTimeStartOnlyDate(job.startDate.toString())!!)
            jobTime = joinTimedf.parse(AppConstants.Epoch2DateStringTimeStartTime(job.startDate.toString())!!)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        job.timer.timer = jobTimer


        (saveText).setOnClickListener(object:View.OnClickListener{
            override fun onClick(v: View?) {
                sign_layout.gone()
                if(is_paused) {
                    mViewModel.completeJob(jobID.toString())
                }else{
                    mViewModel.startJob(jobID.toString(), "")
                    startLayout.setVisibility(View.GONE)
                    stopLayout.setVisibility(View.VISIBLE)
                    pauseLayout.setVisibility(View.VISIBLE)
                    //completeJobLayout.setVisibility(View.GONE);
                    val timeText: String = job.timer.timer
                    val times = timeText.split(":").toTypedArray()

                    /**CONVERT TIME TO SECOND */
                    val second = times[0].toInt() * 60 * 60 + times[1]
                        .toInt() * 60 + times[2].toInt()

                    /**CONVERT TIME TO millisecond */
                    val millisecond = second * 1000
                    startTime = SystemClock.uptimeMillis() - millisecond
                    myHandler.postDelayed(updateTimerMethod, 0)
                }

                onsaveTextClicked(job.id,(signature_pad))
            }

        })


        job_no.text = job.id.toString()
        (startdate_text).text = joinDatesdf.format(jobDate!!)
        (starttime_text).text = joinTimedf.format(jobTime!!)
        (cust_name_text).text = (job.clientObj?.contacts?.get(0)?.name?.returnDashIfEmpty())
        (cust_phone_text).text = (job.clientObj?.contacts?.get(0)?.contactNumber?.returnDashIfEmpty())
        (cust_pick_text).text = job.pickupLocation.name.returnDashIfEmpty()
        (cust_deliver_text).text = job.deliveryLocation.name.returnDashIfEmpty()
        (cust_notes_text).text = (job.note.returnDashIfEmpty())
        bedroom.text = job.additionalInformation.type
        helper.text = job.additionalInformation.helpers.plus( " ").plus("helpers")

    }
    private fun populateJobs(jobSelected : Jobs) {
        var strDate: Date? = null
        val sdf = SimpleDateFormat(this!!.getString(R.string.sdf_string))
        val joinDatesdf = SimpleDateFormat(this!!.getString(R.string.start_date_sdf))
        try {
            strDate =
                sdf.parse(AppConstants.Epoch2DateStringTimesss(jobSelected?.endDate.toString())!!)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        if (jobSelected.startDateActual.compareTo(0).equals(0)
            && !jobSelected.completed
            && strDate != null
            && Date().before(strDate) && job.accepted) {
            //Accepted
            jobStatus = "Accepted"
        }else if (jobSelected.startDateActual.compareTo(0).equals(0)
            && !jobSelected.completed
            && strDate != null
            && Date().before(strDate) && !job.accepted){
            //waiting
            jobStatus = "Waiting"
        }else if (jobSelected!!.completed) {
            //complete
            jobStatus = "Completed"
        } else if (!jobSelected!!.startDateActual.compareTo(0).equals(0) && job.accepted) {
            //in-progress
            jobStatus = "InProgress"
        } else {
            if (jobSelected.endDate != null && !jobSelected!!.endDate.compareTo(0)
                    .equals(0) && Date().after(strDate)
            ) {
                //expired
                jobStatus = "Expired"
            } else {
                //upcoming
                jobStatus = "Upcoming"
            }
        }
    }
    fun addTime(hour: Int, minute: Int, minutesToAdd: Int): String? {
        val calendar: Calendar = GregorianCalendar(1990, 1, 1, hour, minute)
        calendar.add(Calendar.MINUTE, minutesToAdd)
        val sdf = SimpleDateFormat("HH:mm")
        return sdf.format(calendar.time)
    }

    fun minusTime(hour: Int, minute: Int, minutesToAdd: Int): String? {

        val calendar: Calendar = GregorianCalendar(1990, 1, 1, hour, minute)
        calendar.add(Calendar.MINUTE, -minutesToAdd)
        val sdf = SimpleDateFormat("HH:mm")
        return sdf.format(calendar.time)
    }

}
