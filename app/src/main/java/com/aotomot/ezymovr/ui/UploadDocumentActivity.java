package com.aotomot.ezymovr.ui;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.aotomot.ezymovr.CustomAlert.AlertDialog;
import com.aotomot.ezymovr.CustomAlert.OnItemClickListener;
import com.aotomot.ezymovr.R;
import com.asksira.bsimagepicker.BSImagePicker;
import com.mindorks.paracamera.Camera;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;


public class UploadDocumentActivity extends AppCompatActivity implements BSImagePicker.OnSingleImageSelectedListener,
        BSImagePicker.OnMultiImageSelectedListener {

   long jobId;
    private Toolbar toolbar;


    public enum ScalingLogic {
        CROP, FIT
    }

    private Bitmap mBitmapIncidentImage = null;
    private Intent pictureActionIntent = null;
    protected static final int CAMERA_REQUEST = 1110;
    protected static final int GALLERY_PICTURE = 1120;
    private static final int REQUEST_ID_CAMERA_PERMISSIONS = 2011;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_document);
        setToolbar();
        jobId = getIntent().getIntExtra("jobId", 0);
        toolbar = findViewById(R.id.toolbar);
        findViewById(R.id.home).setOnClickListener(v -> finish());
        findViewById(R.id.upload).setOnClickListener(v -> uploadPhoto());

    }


    private void setToolbar() {
        setSupportActionBar(toolbar);
        /*getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);*/
    }


    public void uploadPhoto() {
        AlertDialog.showDialogWithAlertHeaderThreeButton(this, "Upload Image", "Upload Pictures Option", new OnItemClickListener() {
            @Override
            public void onItemClick(Object o, int position) {
                if (position == 0) {
                    //camera
                    if (checkAndRequestPermissions(UploadDocumentActivity.this)) {
                        int permissionCheck = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA);
                        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                            launchCameraOption();
                        } else {
                            if (Build.VERSION.SDK_INT >= 23) {
                                if (!shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                                    requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_ID_CAMERA_PERMISSIONS);
                                    return;
                                }
                                return;
                            } else {
                                launchCameraOption();
                            }
                        }
                    }
                } else {
                    //gallery
                    if (checkAndRequestPermissions(UploadDocumentActivity.this)) {
                        pictureActionIntent = new Intent(Intent.ACTION_GET_CONTENT, null);
                        pictureActionIntent.setType("image/*");
                        pictureActionIntent.putExtra("return-data", true);
                        startActivityForResult(pictureActionIntent, GALLERY_PICTURE);

                    }
                }
            }
        });
    }

    public static boolean checkAndRequestPermissions(final Activity context) {
        int ExtstorePermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_EXTERNAL_STORAGE);
        int cameraPermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.CAMERA);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (ExtstorePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(context, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_CAMERA_PERMISSIONS);
            return false;
        }
        return true;
    }

    Uri imageUri;

    /**
     * Launches camera
     */
    Camera camera;

    private void launchCameraOption() {
        camera = new Camera.Builder()
                .resetToCorrectOrientation(true)// it will rotate the camera bitmap to the correct orientation from meta data
                .setTakePhotoRequestCode(1)
                .setDirectory("pics")
                .setName("ali_" + System.currentTimeMillis())
                .setImageFormat(Camera.IMAGE_JPEG)
                .setCompression(75)
                .setImageHeight(1000)// it will try to achieve this height as close as possible maintaining the aspect ratio;
                .build(this);
        try {
            camera.takePicture();
        } catch (Exception e) {
            e.printStackTrace();
        }

//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        // start the image capture Intent
//        startActivityForResult(intent, CAMERA_REQUEST);
//        ContentValues values = new ContentValues();
//        values.put(MediaStore.Images.Media.TITLE, "New Picture");
//        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
//        imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
//        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(base));
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 0, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    public Bitmap getBitmapFromUri() {

        getContentResolver().notifyChange(imageUri, null);
        ContentResolver cr = getContentResolver();
        Bitmap bitmap;

        try {
            bitmap = MediaStore.Images.Media.getBitmap(cr, imageUri);
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == android.app.Activity.RESULT_OK) {

            Bitmap thumbnail = null;
            try {
                mBitmapIncidentImage = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (mBitmapIncidentImage != null) {
//                mBitmapIncidentImage = (Bitmap) data.getExtras().get("data");
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    Intent i = new Intent(this, SendGalleryPhotoActivity.class);
                    i.putExtra("imageUri", getImageUri(this, mBitmapIncidentImage));
                    i.putExtra("album", "no");
                    i.putExtra("jobId", jobId);
                    startActivity(i);
                    //finish();
                } else {
                    Intent i = new Intent(this, SendGalleryPhotoActivity.class);
                    i.putExtra("imageUri", getImageUri(this, mBitmapIncidentImage));
                    i.putExtra("jobId", jobId);
                    i.putExtra("album", "no");
                    startActivity(i);
                    //finish();
                }
            }
        } else if (requestCode == GALLERY_PICTURE && resultCode == android.app.Activity.RESULT_OK) {

            Uri uri = data.getData();
            Intent i = new Intent(this, SendGalleryPhotoActivity.class);
            i.putExtra("imageUri", uri);
            i.putExtra("jobId", jobId);
            i.putExtra("album", "yes");
            startActivity(i);
            finish();
        } else if (requestCode == REQUEST_ID_CAMERA_PERMISSIONS && resultCode == android.app.Activity.RESULT_OK) {
            if (ContextCompat.checkSelfPermission(UploadDocumentActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(), "FlagUp Requires Access to Camera.", Toast.LENGTH_SHORT).show();
                finish();
            } else if (ContextCompat.checkSelfPermission(UploadDocumentActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(), "FlagUp Requires Access to Your Storage.", Toast.LENGTH_SHORT).show();
                finish();
            }
        } else if (requestCode == Camera.REQUEST_TAKE_PHOTO) {
            try {
                Bitmap bitmap = camera.getCameraBitmap();
                Intent i = new Intent(this, SendGalleryPhotoActivity.class);
                i.putExtra("imageUri", getImageUri(this, bitmap));
                i.putExtra("jobId", jobId);
                i.putExtra("album", "no");
                startActivity(i);
                finish();
            } catch (Exception e) {

            }
        }
    }

    @Override
    public void onMultiImageSelected(List<Uri> uriList) {

    }

    @Override
    public void onSingleImageSelected(Uri uri) {

        finish();
        Intent i = new Intent(this, SendGalleryPhotoActivity.class);
        i.putExtra("imageUri", uri);
        i.putExtra("jobId", jobId);
        i.putExtra("album", "yes");
        startActivity(i);
    }

    private static final String IMAGE_DIRECTORY = "/DMS";

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }


    @Override
    protected void onPause() {
        super.onPause();
        //EventBus.getDefault().unregister(this);
    }

    @Override
    public void onStop() {
        super.onStop();
       // EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        //EventBus.getDefault().register(this);

        SharedPreferences finishedImageUpload = getSharedPreferences("IMAGE_UPLOAD_DONE", Context.MODE_PRIVATE);
        if (finishedImageUpload.getBoolean("imageUploadFinished", false)) {
            SharedPreferences.Editor finishedImageUploadEditor = getSharedPreferences("IMAGE_UPLOAD_DONE", Context.MODE_PRIVATE).edit();
            finishedImageUploadEditor.putBoolean("imageUploadFinished", false);
            finishedImageUploadEditor.commit();
            finish();
        }
    }

    private File getTempFile(Context context) {
        //it will return /sdcard/image.tmp
        final File path = new File(Environment.getExternalStorageDirectory(), context.getPackageName());
        if (!path.exists()) {
            path.mkdir();
        }
        return new File(path, "image.tmp");
    }


}
