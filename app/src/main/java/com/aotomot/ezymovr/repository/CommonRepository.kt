package com.aotomot.ezymovr.Repository

import android.content.Context
import android.util.Log
import com.aotomot.ezymovr.AppConstants
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.WebService.WebServiceAPI
import java.io.IOException
import com.aotomot.ezymovr.WebService.Result
import com.aotomot.ezymovr.WebService.Result.*
import com.aotomot.ezymovr.models.*
import com.aotomot.homeworld.models.DeviceRegistrationRequest
import com.aotomot.homeworld.models.DeviceRegistrationResponse
import com.aotomot.homeworld.models.HomeDirections
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Response

class CommonRepository(
    private val api: WebServiceAPI,
    private val context: Context?,
    private val domain : String
) : BaseRepository() {

    val baseUrl = "https://".plus(domain).plus(".ezymovr.com/api/")
    /*suspend fun getAlertsNotification() : Result<MutableList<AlertNotification>> {
        Log.d("","")
        val locationsResponse = safeApiCall(
            call = {getNotificationByDevice()},
            errorMessage = "Error Fetch Alerts"
        )

        return locationsResponse

    }*/

   suspend fun getNotificationByDevice(userID :String): Result<MutableList<AlertNotification>> {
        Log.d("","")
        var response: MutableList<AlertNotification> = mutableListOf()
       val url = baseUrl.plus("notification/byUserId")

        try {
            response = api.getNotification(url,AppConstants.Authorization,userID)
        }catch (e:Exception){
            Log.d("Exception","" + e)
        }

        Log.d("","")
        if(response.isNotEmpty()) {
            return Success(response)
        }
        return Error(IOException("Error Fetch Alerts"))

    }
    suspend fun getOnBoarding() : Result<MutableList<OnBoarding>> {
       Log.d("","")
        val locationsResponse = safeApiCall(
            call = {getOnBoardingFromWeb()},
            errorMessage = context!!.getString(R.string.error_network_onboarding)
        )

        return locationsResponse

    }

    suspend fun getOnBoardingFromWeb(): Result<MutableList<OnBoarding>> {
        Log.d("","")
        var response :MutableList<OnBoarding> = mutableListOf()
        try {
            response = api.getOnBoarding("asaifeqwmqfkvkfvdznzyg2323msamn723i2chbfekwf244r")
        }catch (e: Exception){
            Log.d("","")
        }

            if(response.isNotEmpty()) {
                return Success(response)
            }
            return Error(IOException(context!!.getString(R.string.error_network_onboarding)))

    }
    suspend fun addDevice(mData: DeviceRegistrationRequest) : Result<DeviceRegistrationResponse> {

        //safeApiCall is defined in BaseRepository.kt (https://gist.github.com/navi25/67176730f5595b3f1fb5095062a92f15)
        val directionsResponse = safeApiCall(
            call = {addDeviceToServer(mData)},
            errorMessage = "Error Adding Device"
        )

        return directionsResponse

    }
    suspend fun updateDevice(ID :String,mData: DeviceRegistrationRequest) : Result<DeviceRegistrationResponse> {

        val updateResponse = safeApiCall(
            call = {updateDeviceToServer(ID,mData)},
            errorMessage = "Error Updating Device"
        )
        return updateResponse

    }
    suspend fun addDeviceToServer(mDeviceDataRequest: DeviceRegistrationRequest): Result<DeviceRegistrationResponse> {
        val url = baseUrl.plus("device/add")
        val response = api.addDeviceForPushNotifications(url,mDeviceDataRequest)
        if (response.isSuccessful)
            return Result.Success(response.body()!!)
        return Result.Error(IOException("Error occurred during Adding Device"))
    }
    private suspend fun updateDeviceToServer(ID : String,mDeviceDataRequest: DeviceRegistrationRequest): Result<DeviceRegistrationResponse> {
        val url = baseUrl.plus("device/update/{deviceId}")
        val response = api.updateDeviceForPushNotifications(url,ID,mDeviceDataRequest)
        if (response.isSuccessful)
            return Result.Success(response.body()!!)
        return Result.Error(IOException("Error occurred during Updating Device"))
    }

    suspend fun login(username: String, password: String) : Result<LoginResponse> {
        Log.d("","")
        val locationsResponse = safeApiCall(
            call = {loginFromWeb(username,password)},
            errorMessage = "Error"
        )

        return locationsResponse

    }

    suspend fun loginFromWeb(username: String, password: String): Result<LoginResponse> {
        Log.d("","")

        val requestBody = PostLoginBody(username,password)
        val url = baseUrl.plus("login")
        val response = api.postLogin(url,requestBody)
        Log.d("","")
        if(response.isSuccessful) {
            return Success(response.body()!!)
        }
        return Error(IOException("Error Logging In"))

    }

    suspend fun getDriverDetails(userId: String) : Result<Driver> {
        Log.d("","")
        val response = safeApiCall(
            call = {getDriverByUserID(userId)},
            errorMessage = "Error"
        )
        return response
    }

    suspend fun getDriverByUserID(userId: String) : Result<Driver> {
        Log.d("","")
        val url = baseUrl.plus("driver/byUser/").plus(userId)
        val response = api.getMoverByUserId(url,AppConstants.Authorization)
        Log.d("","")
        if(response.isSuccessful) {
            return Success(response.body()!!)
        }
        return Error(IOException("Error Logging In"))

    }

    suspend fun getjobattchments(userId: String) : Result<MutableList<AttachmentItem>> {
        Log.d("","")

        val response = safeApiCall(
            call = {getjobattchmentsList(userId)},
            errorMessage = "Error"
        )
        return response
    }
    suspend fun resetPassword(email: String) : Result<PostMessage> {
        Log.d("","")
        val url = baseUrl.plus("user/resetPassword")
        // val response = api.getJobList(AppConstants.Authorization,userId)
        val response = api.postResetPassword(url,email)

        Log.d("","")
        if(response != null) {
            return Success(response)
        }
        return Error(IOException("Error Reset Password In"))

    }
    suspend fun getjobattchmentsList(userId: String) : Result<MutableList<AttachmentItem>> {
        Log.d("","")

        val url = baseUrl.plus("image/listByJobId")
        // val response = api.getJobList(AppConstants.Authorization,userId)
        val response = api.getListjobById(url,AppConstants.Authorization,userId)
        /*val response = api.getListjobById(
            "Bearer eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJodHRwczovL2xpY2Vuc2luZy5hb3RvbW90LmNvbSIsIk1vYmlBdXRoIjoiVVNFUiIsInN1YiI6ImRyaXZlckB0ZXN0LmNvbSIsImV4cCI6MTU4NjMwMjcxMn0.TVK7MZ8qEuVqwdCPCZVTqZhaPJ0fAswkF0caF6JtHM7OFhh3L2SBF4zae1BuSFufYssJoFnUl64o8_h4hoHxYA",
            userId)*/
        Log.d("","")
        if(!response.isNullOrEmpty()) {
            return Success(response)
        }
        return Error(IOException("Error Logging In"))

    }
    suspend fun getjobDetails(userId: String) : Result<MutableList<Jobs>> {
        Log.d("","")
        val response = safeApiCall(
            call = {getjobByUserID(userId)},
            errorMessage = "Error"
        )
        return response
    }

    suspend fun getjobByUserID(userId: String) : Result<MutableList<Jobs>> {
        Log.d("","")
        val url = baseUrl.plus("job/listByDriverId")
       // val response = api.getJobList(AppConstants.Authorization,userId)
        val response = api.getJobList(url,AppConstants.Authorization,userId)
        /*val response = api.getJobList(
            "Bearer eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJodHRwczovL2xpY2Vuc2luZy5hb3RvbW90LmNvbSIsIk1vYmlBdXRoIjoiVVNFUiIsInN1YiI6ImRyaXZlckB0ZXN0LmNvbSIsImV4cCI6MTU4NjMwMjcxMn0.TVK7MZ8qEuVqwdCPCZVTqZhaPJ0fAswkF0caF6JtHM7OFhh3L2SBF4zae1BuSFufYssJoFnUl64o8_h4hoHxYA",
            "42983")*/
        Log.d("","")
        if(!response.isNullOrEmpty()) {
            return Success(response)
        }
        return Error(IOException("Error Logging In"))

    }
    suspend fun signJobs(jobID: String,comment :String) : Result<ResponseBody> {
        Log.d("","")
        val url = baseUrl.plus("job/sign/").plus(jobID)
        // val response = api.getJobList(AppConstants.Authorization,userId)
        val response = api.signJob(url,AppConstants.Authorization,comment)
        //val response = api.signJob(url,AppConstants.Authorization,jobID,comment)
        Log.d("","")
        if(response != null) {
            return Success(response)
        }
        return Error(IOException("Error Logging In"))

    }
    suspend fun assignToJob(comment :AssignToJobModel) : Result<ResponseBody> {
        Log.d("","")
        val url = baseUrl.plus("document/assignToJob")
        // val response = api.getJobList(AppConstants.Authorization,userId)
        val response = api.assignToJob(url,AppConstants.Authorization,comment)
        Log.d("","")
        if(response != null) {
            return Success(response)
        }
        return Error(IOException("Error Logging In"))

    }
    suspend fun UpdatePassword(newPass: ChangePasswordModel,jobID :String) : Result<ChangePassword> {
        Log.d("","")
        var response : ChangePassword = ChangePassword()
        val url = baseUrl.plus("user/updatePassword")
        try {
            response = api.updatePassword(url,AppConstants.Authorization,newPass,jobID)
        }catch (e:java.lang.Exception){
            Log.d("","")
        }
        // val response = api.putImageDetails(AppConstants.Authorization,jobID,comment)
        Log.d("","")
        if(response != null) {
            return Success(response)
        }
        return Error(IOException("Error Logging In"))

    }
    suspend fun AddTime(jobID :String,time : Int, comment :String, isAdditional :Boolean) : Result<ResponseBody> {
        Log.d("","")
        var response : ResponseBody? = null
        val url = baseUrl.plus("job/addTime/").plus(jobID)
        try {
            response = api.addAdditionalTime(url,AppConstants.Authorization,"application/x-www-form-urlencoded",isAdditional,time,comment)
        }catch (e:java.lang.Exception){
            Log.d("","")
        }
        // val response = api.putImageDetails(AppConstants.Authorization,jobID,comment)
        Log.d("","")
        if(response != null) {
            return Success(response)
        }
        return Error(IOException("Error Logging In"))

    }
    suspend fun DeclineJob(jobID: String,comment :String) : Result<ResponseBody> {
        Log.d("","")
        var response : ResponseBody? = null
        val url = baseUrl.plus("job/decline/").plus(jobID)
        try {
            response = api.declineJob(url,AppConstants.Authorization,comment)
        }catch (e:java.lang.Exception){
            Log.d("","")
        }
        // val response = api.putImageDetails(AppConstants.Authorization,jobID,comment)
        Log.d("","")
        if(response != null) {
            return Success(response)
        }
        return Error(IOException("Error Logging In"))

    }

    suspend fun AcceptJob(jobID: String) : Result<ResponseBody> {
        Log.d("","")
        var response : ResponseBody? = null
        val url = baseUrl.plus("job/accept/").plus(jobID)
        try {
            response = api.acceptJob(url,AppConstants.Authorization)
        }catch (e:java.lang.Exception){
            Log.d("","")
        }
        // val response = api.putImageDetails(AppConstants.Authorization,jobID,comment)
        Log.d("","")
        if(response != null) {
            return Success(response)
        }
        return Error(IOException("Error Logging In"))

    }

    suspend fun UpdateDriverDetails(driverID: String, mDriver : Driver) : Result<String> {
        Log.d("","")
        var response : String? = null
        val url = baseUrl.plus("driver/").plus(driverID)
        try {
            response = api.updateDriverDetails(url,AppConstants.Authorization,mDriver)
        }catch (e:java.lang.Exception){
            Log.d("","")
        }
        // val response = api.putImageDetails(AppConstants.Authorization,jobID,comment)
        Log.d("","")
        if(response != null) {
            return Success(response)
        }
        return Error(IOException("Error Logging In"))

    }

    suspend fun UpdateDriverImage(driverID: String, mDriver : UpdateUserProfile) : Result<String> {
        Log.d("","")
        var response : String? = null
        val url = baseUrl.plus("image/").plus(driverID)
        try {
            response = api.updateImageToDriver(url,AppConstants.Authorization,mDriver)
        }catch (e:java.lang.Exception){
            Log.d("","")
        }
        // val response = api.putImageDetails(AppConstants.Authorization,jobID,comment)
        Log.d("","")
        if(response != null) {
            return Success(response)
        }
        return Error(IOException("Error Logging In"))

    }

    suspend fun markRead(driverID: String, notificationID : String) : Result<ResponseBody> {
        Log.d("","")
        var response : ResponseBody? = null
        val url = baseUrl.plus("notification/markAsRead/").plus(notificationID)
        try {
            response = api.markPushAlertToBeRead(url,AppConstants.Authorization)
        }catch (e:java.lang.Exception){
            Log.d("","")
        }
        // val response = api.putImageDetails(AppConstants.Authorization,jobID,comment)
        Log.d("","")
        if(response != null) {
            return Success(response)
        }
        return Error(IOException("Error Logging In"))

    }
    suspend fun getSettings( smartPhone : String) : Result<ResponseBody> {
        Log.d("","")
        var response : ResponseBody? = null
        val url = baseUrl.plus("settings")
        try {
            response = api.getSetting(url,AppConstants.Authorization,smartPhone)
        }catch (e:java.lang.Exception){
            Log.d("","")
        }
        // val response = api.putImageDetails(AppConstants.Authorization,jobID,comment)
        Log.d("","")
        if(response != null) {
            return Success(response)
        }
        return Error(IOException("Error Logging In"))

    }
    suspend fun AssignToDriver( assign : AssignToJobModel) : Result<ResponseBody> {
        Log.d("","")
        var response : ResponseBody? = null
        val url = baseUrl.plus("document/assignToDriver")
        try {
            response = api.assignToDriver(url,AppConstants.Authorization,assign)
        }catch (e:java.lang.Exception){
            Log.d("","")
        }
        // val response = api.putImageDetails(AppConstants.Authorization,jobID,comment)
        Log.d("","")
        if(response != null) {
            return Success(response)
        }
        return Error(IOException("Error Logging In"))

    }
    suspend fun PostLocationUpdate( Id :String , locUpdate : PostLocationUpdate) : Result<MutableList<UploadImage>> {
        Log.d("","")
        var response : MutableList<UploadImage>? = null
        val url = baseUrl.plus("user/").plus(Id).plus("/locationUpdate")
        //val url = baseUrl.plus("user/{id}/locationUpdate")
        try {
            response = api.doPostLocationUpdate(url,AppConstants.Authorization,locUpdate)
        }catch (e:java.lang.Exception){
            Log.d("","")
        }
        // val response = api.putImageDetails(AppConstants.Authorization,jobID,comment)
        Log.d("","")
        if(response != null) {
            return Success(response)
        }
        return Error(IOException("Error Logging In"))

    }
    suspend fun putImageDetails(jobID: String,comment :ImagePUTTitleComment) : Result<UploadImage> {
        Log.d("","")
        var response:UploadImage = UploadImage()
        val url = baseUrl.plus("image/").plus(jobID)
        try {
            response = api.putImageDetails(url,AppConstants.Authorization,comment)
        }catch (e:java.lang.Exception){
            Log.d("","")
        }
       // val response = api.putImageDetails(AppConstants.Authorization,jobID,comment)
        Log.d("","")
        if(response != null) {
            return Success(response)
        }
        return Error(IOException("Error Logging In"))

    }
    suspend fun postImage(part: MultipartBody.Part, comment :String) : Result<UploadImage> {
        Log.d("","")

        var response:UploadImage = UploadImage()
        val url = baseUrl.plus("media/uploadImage")
        try {
            response = api.postMediaUpload(url,AppConstants.Authorization,part)
        }catch (e:java.lang.Exception){
            Log.d("","")
        }
        //val response = api.postMediaUpload(AppConstants.Authorization,part)
        Log.d("","")
        if(response != null) {
            return Success(response)
        }
        return Error(IOException("Error Logging In"))

    }

    suspend fun sendstartORPauseJobs(jobId: String, start : Boolean,comment :String) : Result<ResponseBody> {
        Log.d("","")

        val url = baseUrl.plus("job/timer/").plus(jobId)
        val response = api.startOrPauseJob(url,AppConstants.Authorization,start,comment)
        //val response = api.startOrPauseJob(url,AppConstants.Authorization,jobId,start,comment)
        Log.d("","")
        if(response != null) {
            return Result.Success(response)
        }
        return Error(IOException("Error Logging In"))

    }
    suspend fun completeJob(jobId: String) : Result<ResponseBody> {
        Log.d("","")

        val url = baseUrl.plus("job/complete/").plus(jobId)
        val response = api.doCompleteJob(url,AppConstants.Authorization)
        //val response = api.doCompleteJob(url,AppConstants.Authorization,jobId)
        Log.d("","")
        if(response != null) {
            return Result.Success(response)
        }
        return Error(IOException("Error Logging In"))

    }

    suspend fun getDirections(origin:String,destination:String,apiKey:String) : Result<HomeDirections> {

        //safeApiCall is defined in BaseRepository.kt (https://gist.github.com/navi25/67176730f5595b3f1fb5095062a92f15)
        val directionsResponse = safeApiCall(
            call = {getDirectionsFromWeb(origin,destination,apiKey )},
            errorMessage = "Error Fetching Directions"
        )

        return directionsResponse

    }
    private suspend fun getDirectionsFromWeb(origin:String,destination:String,apiKey:String): Result<HomeDirections> {
        val response = api.getDirectionsFromGoogle(origin,destination,apiKey,"driving","now","false")

        if(response != null)
            return Result.Success(response)
        return Error(IOException("Error Logging In"))
    }
    /*suspend fun getAlertsNotification() : Result<MutableList<AlertsNotificaation>> {
        Log.d("","")
        val locationsResponse = safeApiCall(
            call = {getNotificationByDevice()},
            errorMessage = "Error Fetch Alerts"
        )

        return locationsResponse

    }

    private suspend fun getNotificationByDevice(): Result<MutableList<AlertsNotificaation>> {
        Log.d("","")
        var response: MutableList<AlertsNotificaation> = mutableListOf()

        try {
            response = api.getNotificationByDevice(AppConstants.registeredDeviceID.toString())
        }catch (e:Exception){
            Log.d("Exception","" + e)
        }

        Log.d("","")
        if(response.isNotEmpty()) {
            return Success(response)
        }
        return Error(IOException("Error Fetch Alerts"))

    }
*/
}