package com.aotomot.ezymovr.Repository

import android.util.Log
import com.aotomot.ezymovr.WebService.Result
import java.io.IOException

open class BaseRepository {

    suspend fun <T : Any> safeApiCall(
        call: suspend () -> Result<T>,
        errorMessage: String
    ): Result<T> = try {
        call.invoke()
    } catch (e: Exception) {
        Log.d("BaseRepository","Exception : " + e.localizedMessage)
        Result.Error(IOException(errorMessage, e))
    }
}