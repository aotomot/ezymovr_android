package com.aotomot.ezymovr.viewmodels

import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.Repository.CommonRepository
import com.aotomot.ezymovr.WebService.Result
import com.aotomot.ezymovr.WebService.WebServiceAPIFactory
import com.aotomot.ezymovr.extFunctions.domain
import com.aotomot.ezymovr.extFunctions.getAppSharedPrefs
import com.aotomot.ezymovr.extFunctions.userID
import com.aotomot.ezymovr.models.*
import com.aotomot.ezymovr.ui.OnBoardingActivity
import com.aotomot.homeworld.models.HomeDirections
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Response

class SendPhotoViewModel(private val mParentActivity : AppCompatActivity) : ViewModel() {

    private val repository : CommonRepository =
        CommonRepository(WebServiceAPIFactory.webServiceApi_joblist, mParentActivity,mParentActivity.getAppSharedPrefs().domain())

    private val repository_directions : CommonRepository =
        CommonRepository(WebServiceAPIFactory.webServiceApi_directions, mParentActivity,mParentActivity.getAppSharedPrefs().domain())
    private val _signResult = MutableLiveData<ResponseBody>()
    val signResult : LiveData<ResponseBody>
        get() = _signResult

    private val _postImageREsult = MutableLiveData<UploadImage>()
    val postImageREsult : LiveData<UploadImage>
        get() = _postImageREsult

    private val _putImageResult = MutableLiveData<UploadImage>()
    val putImageResult : LiveData<UploadImage>
        get() = _putImageResult

    private val _assignToJobResult = MutableLiveData<ResponseBody>()
    val assignToJobResult : LiveData<ResponseBody>
        get() = _assignToJobResult

    private val _updateDriverDetails = MutableLiveData<String>()
    val updateDriverDetails : LiveData<String>
        get() = _updateDriverDetails

    private val _updateDriverDetailsError = MutableLiveData<String>()
    val updateDriverDetailsError : LiveData<String>
        get() = _updateDriverDetailsError

    private val _directions = MutableLiveData<HomeDirections>()
    val directions: LiveData<HomeDirections> = _directions

    val _error = MutableLiveData<String>()
    val error : LiveData<String>
        get() = _error
    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
val text: LiveData<String> = _text

    fun postImageDetails(jobID : String,comment: ImagePUTTitleComment){

        viewModelScope.launch {
            val response = repository.putImageDetails(jobID,comment)
            when (response) {
                is Result.Success<*> -> _putImageResult.postValue(response.data as UploadImage)
                is Result.Error -> _error.postValue(response.exception.toString())
            }
            Log.d("","")
        }
    }
    fun UpdateDriver(driverID: String, mDriver : Driver){

        viewModelScope.launch {
            val response = repository.UpdateDriverDetails(driverID,mDriver)
            when (response) {
                is Result.Success<*> -> _updateDriverDetails.postValue(response.data as String)
                is Result.Error -> _updateDriverDetailsError.postValue(response.exception.toString())
            }
            Log.d("","")
        }
    }
    fun assignToJob(comment: AssignToJobModel){

        viewModelScope.launch {
            val response = repository.assignToJob(comment)
            when (response) {
                is Result.Success<*> -> _assignToJobResult.postValue(response.data as ResponseBody)
                is Result.Error -> _error.postValue(response.exception.toString())
            }
            Log.d("","")
        }
    }
    fun signJob(jobID : String,comment: String){

        viewModelScope.launch {
            val response = repository.signJobs(jobID,comment)
            when (response) {
                is Result.Success<*> -> _signResult.postValue(response.data as ResponseBody)
                is Result.Error -> _error.postValue(response.exception.toString())
            }
            Log.d("","")
        }
    }
    fun postImage(part : MultipartBody.Part, comment : String){

        viewModelScope.launch {
            val response = repository.postImage(part,comment)
            when (response) {
                is Result.Success<*> -> _postImageREsult.postValue(response.data as UploadImage)
                is Result.Error -> _error.postValue(response.exception.toString())
            }
            Log.d("","")
        }
    }


    fun fetchDirections(origin:String,destination:String,apiKey:String){
        viewModelScope.launch {
            //  val response : Result<String> = repository.getDirectionsString(origin,destination)
            val response  = repository_directions.getDirections(origin,destination,mParentActivity.getString(
                R.string.google_directionsapi_key))
            when(response) {
                is Result.Success -> _directions.postValue((response.data))
                // is Result.Success -> _directions.postValue(parseDirectionResult(response.data))
                is Result.Error -> _error.postValue(response.exception.toString())
            }
        }

    }
}