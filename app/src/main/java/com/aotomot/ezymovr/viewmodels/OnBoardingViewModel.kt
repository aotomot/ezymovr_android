package com.aotomot.ezymovr.viewModels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aotomot.ezymovr.AppConstants
import com.aotomot.ezymovr.models.OnBoarding
import com.aotomot.ezymovr.Repository.CommonRepository
import com.aotomot.ezymovr.WebService.Result.Error
import com.aotomot.ezymovr.WebService.Result.Success
import com.aotomot.ezymovr.WebService.WebServiceAPIFactory
import com.aotomot.ezymovr.extFunctions.deviceRegisteredID
import com.aotomot.ezymovr.extFunctions.getAppSharedPrefs
import com.aotomot.ezymovr.extFunctions.isDeviceRegistered
import com.aotomot.ezymovr.ui.OnBoardingActivity
import com.aotomot.homeworld.models.DeviceRegistrationResponse
import kotlinx.coroutines.launch

class OnBoardingViewModel(private val mParentActivity : OnBoardingActivity): ViewModel() {

    private val repository : CommonRepository =
        CommonRepository(
            WebServiceAPIFactory.webServiceApi_onBoarding,
            mParentActivity,""
        )

    val _onBoarding_LiveData = MutableLiveData<MutableList<OnBoarding>>()
    val onBoarding_LiveData : LiveData<MutableList<OnBoarding>>
        get() = _onBoarding_LiveData

    val _error = MutableLiveData<String>()
    val error : LiveData<String>
        get() = _error

    init {
        fetchOnBoardingDetails()
    }

    fun fetchOnBoardingDetails(){

        viewModelScope.launch {
            val response = repository.getOnBoardingFromWeb()
            when (response) {
                is Success<*> -> _onBoarding_LiveData.postValue(response.data as MutableList<OnBoarding>)
                is Error -> _error.postValue(response.exception.toString())
            }
            Log.d("","")
        }
    }

    fun RegisterDevice(){
        viewModelScope.launch {
            if ((!mParentActivity.getAppSharedPrefs().isDeviceRegistered()) || (mParentActivity.getAppSharedPrefs().deviceRegisteredID() == 0 ) ) {
                if (!AppConstants.mDataForDeviceRegistrationRequest.deviceToken.isNullOrBlank()) {
                    val response =
                        repository.addDeviceToServer(AppConstants.mDataForDeviceRegistrationRequest)
                    when (response) {
                        is Success<*> -> {
                            mParentActivity.getAppSharedPrefs().isDeviceRegistered(true)
                            mParentActivity.getAppSharedPrefs().deviceRegisteredID((response.data as DeviceRegistrationResponse).id)
                            AppConstants.registeredDeviceID = (response.data as DeviceRegistrationResponse).id
                            Log.d("Firebase 4","Registered Device Onboarding")
                        }
                        is Error -> _error.postValue(response.exception.toString())
                    }
                }
            }else{
                //update Device
                if (!AppConstants.mDataForDeviceRegistrationRequest.deviceToken.isNullOrBlank()) {
                    val response =
                        repository.updateDevice(mParentActivity.getAppSharedPrefs().deviceRegisteredID().toString(),
                            AppConstants.mDataForDeviceRegistrationRequest)
                    when (response) {
                        is Success<*> -> {Log.d("","")
                        }
                        is Error -> _error.postValue(response.exception.toString())
                    }
                }
            }
        }
    }
}