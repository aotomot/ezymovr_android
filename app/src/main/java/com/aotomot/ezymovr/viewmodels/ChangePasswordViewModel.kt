package com.aotomot.ezymovr.viewmodels

import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aotomot.ezymovr.AppConstants
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.Repository.CommonRepository
import com.aotomot.ezymovr.WebService.Result
import com.aotomot.ezymovr.WebService.WebServiceAPIFactory
import com.aotomot.ezymovr.extFunctions.domain
import com.aotomot.ezymovr.extFunctions.getAppSharedPrefs
import com.aotomot.ezymovr.extFunctions.userID
import com.aotomot.ezymovr.models.*
import com.aotomot.ezymovr.ui.OnBoardingActivity
import com.aotomot.homeworld.models.HomeDirections
import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import retrofit2.Response

class ChangePasswordViewModel(private val mParentActivity : AppCompatActivity) : ViewModel() {

    private val repository : CommonRepository =
        CommonRepository(WebServiceAPIFactory.webServicebApi, mParentActivity,mParentActivity.getAppSharedPrefs().domain())

    private val repository_joblist : CommonRepository =
        CommonRepository(WebServiceAPIFactory.webServiceApi_joblist, mParentActivity,mParentActivity.getAppSharedPrefs().domain())

    private val repository_directions : CommonRepository =
        CommonRepository(WebServiceAPIFactory.webServiceApi_directions, mParentActivity,mParentActivity.getAppSharedPrefs().domain())

    private val _jobAttachmentList = MutableLiveData<MutableList<AttachmentItem>>()
    val jobAttachmentList : LiveData<MutableList<AttachmentItem>>
        get() = _jobAttachmentList

    private val _startJob = MutableLiveData<ResponseBody>()
    val startJob : LiveData<ResponseBody>
        get() = _startJob

    private val _pauseJob = MutableLiveData<ResponseBody>()
    val pauseJob : LiveData<ResponseBody>
        get() = _pauseJob

    private val _completeJob = MutableLiveData<ResponseBody>()
    val completeJob : LiveData<ResponseBody>
        get() = _completeJob

    private val _completeJobResult =  MutableLiveData<String>()
    val completeJobResult : LiveData<String>
        get() = _completeJobResult

    private val _acceptJob = MutableLiveData<ResponseBody>()
    val acceptJob : LiveData<ResponseBody>
        get() = _acceptJob

    private val _acceptJobResult =  MutableLiveData<String>()
    val acceptJobResult : LiveData<String>
        get() = _acceptJobResult

    private val _declineJob = MutableLiveData<ResponseBody>()
    val declineJob : LiveData<ResponseBody>
        get() = _declineJob

    private val _declineJobResult =  MutableLiveData<String>()
    val declineJobResult : LiveData<String>
        get() = _declineJobResult

    private val _directions = MutableLiveData<HomeDirections>()
    val directions: LiveData<HomeDirections> = _directions

    private val _jobList = MutableLiveData<MutableList<Jobs>>()
    val jobList : LiveData<MutableList<Jobs>>
        get() = _jobList

    private val _updateResult = MutableLiveData<ChangePassword>()
    val updateResult : LiveData<ChangePassword>
        get() = _updateResult

    val _error = MutableLiveData<String>()
    val error : LiveData<String>
        get() = _error
    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
val text: LiveData<String> = _text

    fun UpdatePassword(newPass: ChangePasswordModel, userID :String){

        viewModelScope.launch {
            val response = repository.UpdatePassword(newPass,userID)
            when (response) {
                is Result.Success<*> -> _updateResult.postValue(response.data as ChangePassword)
                is Result.Error -> _error.postValue(response.exception.toString())
            }
            Log.d("","")
        }
    }

    fun fetchjobsList(){

        viewModelScope.launch {
            val response = repository_joblist.getjobDetails(AppConstants.driver.id.toString())
            when (response) {
                is Result.Success<*> -> _jobList.postValue(response.data as MutableList<Jobs>)
                is Result.Error -> _error.postValue(response.exception.toString())
            }
            Log.d("","")
        }
    }
    fun fetchjobAttachmentList(jobID : String){

        viewModelScope.launch {
            val response = repository.getjobattchmentsList(jobID)
            when (response) {
                is Result.Success<*> -> _jobAttachmentList.postValue(response.data as MutableList<AttachmentItem>)
                is Result.Error -> _error.postValue(response.exception.toString())
            }
            Log.d("","")
        }
    }
    fun startJob(jobID : String, comment : String){

        viewModelScope.launch {
            val response = repository.sendstartORPauseJobs(jobID,true,comment)
            when (response) {
                is Result.Success<*> -> _startJob.postValue(response.data as ResponseBody)
                is Result.Error -> _error.postValue(response.exception.toString())
            }
            Log.d("","")
        }
    }
    fun PauseJob(jobID : String, comment : String){

        viewModelScope.launch {
            val response = repository.sendstartORPauseJobs(jobID,false,comment)
            when (response) {
                is Result.Success<*> -> _pauseJob.postValue(response.data as ResponseBody)
                is Result.Error -> _error.postValue(response.exception.toString())
            }
            Log.d("","")
        }
    }
    fun completeJob(jobID : String){

        viewModelScope.launch {
            val response = repository.completeJob(jobID)
            when (response) {
                is Result.Success<*> -> _completeJob.postValue(response.data as ResponseBody)
                is Result.Error -> _completeJobResult.postValue(response.exception.toString())
            }
            Log.d("","")
        }
    }
    fun AcceptJob(jobID : String){

        viewModelScope.launch {
            val response = repository.AcceptJob(jobID)
            when (response) {
                is Result.Success<*> -> _acceptJob.postValue(response.data as ResponseBody)
                is Result.Error -> _acceptJobResult.postValue(response.exception.toString())
            }
            Log.d("","")
        }
    }

    fun DeclineJob(jobID : String){

        viewModelScope.launch {
            val response = repository.DeclineJob(jobID,"")
            when (response) {
                is Result.Success<*> -> _declineJob.postValue(response.data as ResponseBody)
                is Result.Error -> _declineJobResult.postValue(response.exception.toString())
            }
            Log.d("","")
        }
    }

    fun fetchDirections(origin:String,destination:String,apiKey:String){
        viewModelScope.launch {
            //  val response : Result<String> = repository.getDirectionsString(origin,destination)
            val response  = repository_directions.getDirections(origin,destination,mParentActivity.getString(
                R.string.google_directionsapi_key))
            when(response) {
                is Result.Success -> _directions.postValue((response.data))
                // is Result.Success -> _directions.postValue(parseDirectionResult(response.data))
                is Result.Error -> _error.postValue(response.exception.toString())
            }
        }

    }
}