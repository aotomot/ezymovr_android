package com.aotomot.ezymovr.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.util.Patterns
import androidx.lifecycle.viewModelScope
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.Repository.CommonRepository
import com.aotomot.ezymovr.WebService.WebServiceAPIFactory
import com.aotomot.ezymovr.models.Driver
import com.aotomot.ezymovr.models.PostMessage
import com.aotomot.ezymovr.ui.login.LoginFormState
import kotlinx.coroutines.launch


class ForgotPassViewModel(
    private val loginRepository: CommonRepository,
    private val driverRepository: CommonRepository
) : ViewModel() {

    /*private val driverRepository: CommonRepository =  CommonRepository(
        WebServiceAPIFactory.webServiceApi_driver,
        null
    )*/

    private lateinit var loginRepository1: CommonRepository
    private val _forgotPassForm = MutableLiveData<LoginFormState>()
    val forgotPassFormState: LiveData<LoginFormState> = _forgotPassForm

    private val _resetResult = MutableLiveData<PostMessage>()
    val resetResult: LiveData<PostMessage> = _resetResult

    private val _resetResultError = MutableLiveData<String>()
    val resetResultError: LiveData<String> = _resetResultError

    private val _driverResult = MutableLiveData<Driver>()
    val driverResult: LiveData<Driver> = _driverResult

    private val _driverResultError = MutableLiveData<String>()
    val driverResultError: LiveData<String> = _driverResultError



    fun resetPassword(email: String,companyName: String) {
        // can be launched in a separate asynchronous job
        loginRepository1 =  CommonRepository(WebServiceAPIFactory.webServicebApi, null, companyName)
        viewModelScope.launch {
            val response = loginRepository1.resetPassword(email)

            when (response) {
                is com.aotomot.ezymovr.WebService.Result.Success<*> -> _resetResult.postValue(response.data as PostMessage)
                is com.aotomot.ezymovr.WebService.Result.Error -> _resetResultError.postValue(response.exception.toString())
            }

        }
    }

    fun fetchDriverDatails(userId : String) {
        // can be launched in a separate asynchronous job
        viewModelScope.launch {

            val response = driverRepository.getDriverByUserID(userId)

            when (response) {
                is com.aotomot.ezymovr.WebService.Result.Success<*> -> _driverResult.postValue(response.data as Driver)
                is com.aotomot.ezymovr.WebService.Result.Error -> _driverResultError.postValue(response.exception.toString())
            }

        }
    }

    fun loginDataChanged(username: String, password: String) {
        if (!isUserNameValid(username)) {
            _forgotPassForm.value = LoginFormState(usernameError = R.string.invalid_username)
        } else {
            //_loginForm.value = LoginFormState(isDataValid = true)
            _forgotPassForm.value = LoginFormState(isDataValid = true)
        }
    }

    fun loginDataChangedPass(password: String) {
        if (!isPasswordValid(password)) {
            _forgotPassForm.value = LoginFormState(passwordError = R.string.invalid_password)
        } else {
            _forgotPassForm.value = LoginFormState(isDataValid = true)
        }
    }

    fun loginDataChangedComapnyName(companyName: String) {
        if (!isCompanyNameValid(companyName)) {
            _forgotPassForm.value = LoginFormState(companyNameError = R.string.invalid_company_name)
        } else {
            //_loginForm.value = LoginFormState(isDataValid = true)
            _forgotPassForm.value = LoginFormState(isDataValid = true)
        }
    }

    // A placeholder username validation check
    private fun isUserNameValid(username: String): Boolean {
        return if (username.contains('@')) {
            Patterns.EMAIL_ADDRESS.matcher(username).matches()
        } else {
            username.isNotBlank()
        }
    }

    // A placeholder password validation check
    private fun isPasswordValid(password: String): Boolean {
        return password.isNotBlank()
    }

    private fun isCompanyNameValid(companyName: String): Boolean {
        return companyName.isNotBlank()
    }
}
