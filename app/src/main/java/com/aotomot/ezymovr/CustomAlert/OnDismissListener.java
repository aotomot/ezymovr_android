package com.aotomot.ezymovr.CustomAlert;

/**
 * Created on 15/8/9.
 */
public interface OnDismissListener {
    void onDismiss(Object o);
}
