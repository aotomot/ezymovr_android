package com.aotomot.ezymovr.CustomAlert;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.aotomot.ezymovr.R;


/**
 * Created by andrewi on 13/01/2017.
 */

public class AlertDialog {

    private static AlertView mAlertViewExt;
    public static EditText etName;
    private static String emailText;
    public static InputMethodManager imm;


    public static void showDialogWithHeaderFortouchId(Context context, OnItemClickListener onItemClickListener) {
        new AlertView("REST SUPER", "Would you like to use touch ID to access your super account", "", null, new String[]{"Not now", "Yes please"}, context, AlertView.Style.Alert, true, false, onItemClickListener, true, true).show();
    }

    public static void showDialogWithHeaderForDeleteBeneficiary(Context context, String heading, String message, OnItemClickListener onItemClickListener) {
        new AlertView(heading, message, "", null, new String[]{"Cancel", "Go To Setting"}, context, AlertView.Style.Alert, true, false, onItemClickListener, true, false).show();
    }

    public static void showDialogWithoutAlertHeader(Context context, String title, String message, OnItemClickListener onItemClickListener) {
        new AlertView(title, message, "Cancel", new String[]{"OK"}, null, context, AlertView.Style.Alert, false, true, onItemClickListener).show();
    }

    public static void showDialogWithAlertHeaderThreeButton(Context context, String header, String message, OnItemClickListener onItemClickListener) {
        new AlertView(header, message, "", null, new String[]{"Camera", "Gallery"}, context, AlertView.Style.Alert, true, false, onItemClickListener, true, false).show();
    }


    public static void showDialogWithoutAlertHeaderSingleButton(Context context, String message, OnItemClickListener onItemClickListener) {
        new AlertView("", message, null, new String[]{"OK"}, null, context, AlertView.Style.Alert, true, true, onItemClickListener).show();
    }

    public static void showDialogWithAlertHeaderSingleButton(Context context, String header, String message, OnItemClickListener onItemClickListener) {
        new AlertView(header, message, null, new String[]{"OK"}, null, context, AlertView.Style.Alert, true, true, onItemClickListener).show();
    }

    public static void showDialogWithoutAlertHeadelaunchWakeAPI(Context context, String message, OnItemClickListener onItemClickListener) {
        AlertView alertView = new AlertView("", message, null, new String[]{"OK"}, null, context, AlertView.Style.Alert, false, true, onItemClickListener);
        alertView.show();
        alertView.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(Object o) {

            }
        });
    }

    public static void showDialogWithoutHeaderTwoButtonLaunchWakeBreak(Context context, String message, OnItemClickListener onItemClickListener) {
        final AlertView alertView = new AlertView("", message, "Cancel", null, new String[]{"Yes"}, context, AlertView.Style.Alert, false, false, onItemClickListener);

        alertView.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(Object o) {

            }
        });

        imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        ViewGroup extView = (ViewGroup) LayoutInflater.from(context).inflate(R.layout.alerttext_form, null);
        etName = (EditText) extView.findViewById(R.id.etName);
        etName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focus) {
                //输入框出来则往上移动
                boolean isOpen = imm.isActive();
                alertView.setMarginBottom(isOpen && focus ? 120 : 0);

//                System.out.println(isOpen);
            }
        });
        alertView.addExtView(extView);
        alertView.show();
    }

    public static void showDialogWithoutHeaderTwoButtonLaunchWake(Context context, String message, OnItemClickListener onItemClickListener) {
        AlertView alertView = new AlertView("", message, "Cancel", null, new String[]{"Yes"}, context, AlertView.Style.Alert, false, false, onItemClickListener);
        alertView.show();
        alertView.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(Object o) {

            }
        });

    }

    public static void showDialogWithouteaderTwoButtonRedText(Context context, String header, String message, String button, String color, boolean isTitleBold, OnItemClickListener onItemClickListener) {
        new AlertView(header, message, "Cancel", null, new String[]{button}, context, AlertView.Style.Alert, false, false, onItemClickListener, color, isTitleBold).show();
    }

    public static void showDialogWithHeaderTwoButtonRedText(Context context, String header, String message, String button, String color, boolean isTitleBold, OnItemClickListener onItemClickListener) {
        new AlertView(header, message, "Cancel", null, new String[]{button}, context, AlertView.Style.Alert, true, false, onItemClickListener, color, isTitleBold).show();
    }

    public static void showDialogWithHeaderTwoButtonUpload(Context context, String header, String message, String button, OnItemClickListener onItemClickListener) {
        new AlertView(header, message, "Cancel", null, new String[]{button}, context, AlertView.Style.Alert, true, false, onItemClickListener).show();
    }

    public static void showDialogWithHeaderTwoButton(Context context, String header, String message, OnItemClickListener onItemClickListener) {
        new AlertView(header, message, "Cancel", null, new String[]{"OK"}, context, AlertView.Style.Alert, true, false, onItemClickListener).show();
    }

    public static void showDialogWithoutHeaderTwoButton(Context context, String message, OnItemClickListener onItemClickListener) {
        new AlertView("", message, "Cancel", null, new String[]{"OK"}, context, AlertView.Style.Alert, false, false, onItemClickListener).show();
    }

    public static void showDialogWithHeaderTwoButtonForCall(Context context, String header, String message, OnItemClickListener onItemClickListener) {
        new AlertView(header, message, null, null, new String[]{"Cancel", "Call"}, context, AlertView.Style.Alert, true, false, onItemClickListener).show();
    }

    public static boolean isEmailValid(String email) {
        if (email.contains(" ")) {
            email = email.replace(" ", "");
        }
        return !(email == null || TextUtils.isEmpty(email)) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
