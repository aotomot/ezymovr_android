package com.aotomot.ezymovr.deserializer

import com.aotomot.ezymovr.models.*
import com.google.gson.Gson
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type

class DriverDeserializer : JsonDeserializer<Driver>{
    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): Driver {
        val obj = json!!.asJsonObject
        val newDriver = Driver()
        newDriver.id = obj.get("id").asInt

        newDriver.createdBy = obj.get("createdBy").asString
        newDriver.createdDate = obj.get("createdDate").asLong
        newDriver.lastModifiedBy = obj.get("lastModifiedBy").asString
        newDriver.lastModifiedDate = obj.get("lastModifiedDate").asLong
        newDriver.companyName = obj.get("companyName").asString
        newDriver.abn = obj.get("abn").asString
        newDriver.firstName = obj.get("firstName").asString
        newDriver.lastName = obj.get("lastName").asString
        newDriver.email = obj.get("email").asString
        newDriver.password = obj.get("password").asString
        newDriver.hourlyRate = obj.get("hourlyRate").asString
        newDriver.gvm = obj.get("gvm").asInt
        newDriver.tweight = obj.get("tweight").asString
        newDriver.chargeSize = obj.get("chargeSize").asString
        newDriver.multiDriver = obj.get("multiDriver").asBoolean
        newDriver.enabled = obj.get("enabled").asBoolean

        /*Long imageID = json.getAsJsonObject().get("imageId").getAsJsonArray().get(0).getAsLong();
        newDriver.setImageId(imageID);*/
        newDriver.contact  = Gson().fromJson(
            json.asJsonObject["contact"].toString(),
            DriverContact::class.java
        )
        newDriver.emergencyContact = Gson().fromJson(
            json.asJsonObject["emergencyContact"].toString(),
            DriverEmergencyContact::class.java
        )
        val imageList :MutableList<DriverImage> = mutableListOf()
        if (obj["image"] != null) {
            if (obj["image"].isJsonArray) {
                val arr = obj.getAsJsonArray("image")
                if (!arr.isJsonNull) {
                    if (arr.size()>0) {
                        if (arr[0].asJsonObject != null) {
                            imageList.add(
                                Gson().fromJson(
                                    arr[0].asJsonObject.toString(),
                                    DriverImage::class.java
                                )
                            )
                        }
                    }
                }

                if(imageList.isNotEmpty()){
                    val profileList = imageList.filter { it.profile == true }
                    newDriver.driverImage = (profileList.sortedBy { it.createdDate }).get(0)
                }
            }
        }

        val arrLoc = json.asJsonObject["location"].asJsonObject
        if (!arrLoc.isJsonNull) {
            newDriver.location = Gson().fromJson(arrLoc.toString(), DriverLocation::class.java)
        }

        return newDriver
    }
}