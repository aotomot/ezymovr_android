package com.aotomot.ezymovr.deserializer

import com.aotomot.ezymovr.models.*
import com.google.gson.Gson
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type

class JobListDeserializer : JsonDeserializer<Jobs>{
    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): Jobs {
        val obj = json!!.asJsonObject
        val newJob = Jobs()
        newJob.id = obj.get("id").asInt

        newJob.createdBy = obj.get("createdBy").asString
        newJob.createdDate = obj.get("createdDate").asLong
        newJob.lastModifiedBy = obj.get("lastModifiedBy").asString
        newJob.lastModifiedDate = obj.get("lastModifiedDate").asLong
        newJob.startDate = obj.get("startDate").asLong
        newJob.endDate = obj.get("endDate").asLong

        if( !obj.get("completedDate").isJsonNull)
        newJob.completedDate = obj.get("completedDate").asLong

        if( !obj.get("startDateActual").isJsonNull)
        newJob.startDateActual = obj.get("startDateActual").asLong

        if( !obj.get("clientId").isJsonNull)
        newJob.clientId = obj.get("clientId").asInt

        if( !obj.get("driverId").isJsonNull)
        newJob.driverId = obj.get("driverId").asInt

        if( !obj.get("client").isJsonNull)
        newJob.client = obj.get("client").asString

        if( !obj.get("user").isJsonNull)
        newJob.user = Gson().fromJson(json.asJsonObject["user"].toString(), JobUser::class.java)

        if( !obj.get("clientImageUrl").isJsonNull)
        newJob.clientImageUrl = obj.get("clientImageUrl").asString

        if( !obj.get("clientObj").isJsonNull)
        newJob.clientObj = Gson().fromJson(json.asJsonObject["clientObj"].toString(), ClientObj::class.java)

        if( !obj.get("driver").isJsonNull)
        newJob.driver = obj.get("driver").asString

        if( !obj.get("gvm").isJsonNull)
        newJob.gvm = obj.get("gvm").asInt

        if( !obj.get("rate").isJsonNull)
        newJob.rate = obj.get("rate").asInt

        if( !obj.get("tweight").isJsonNull)
        newJob.tweight = obj.get("tweight").asString

        if( !obj.get("chargeSize").isJsonNull)
        newJob.chargeSize = obj.get("chargeSize").asString

        if( !obj.get("driverImageUrl").isJsonNull)
        newJob.driverImageUrl = obj.get("driverImageUrl").asString

        if( !obj.get("note").isJsonNull)
        newJob.note = obj.get("note").asString

        if( !obj.get("adminNote").isJsonNull)
        newJob.adminNote = obj.get("adminNote").asString

        if( !obj.get("noteHistory").isJsonNull)
        newJob.noteHistory = obj.get("noteHistory").asString

        if( !obj.get("adminNoteHistory").isJsonNull)
        newJob.adminNoteHistory = obj.get("adminNoteHistory").asString

        if( !obj.get("timerEdited").isJsonNull)
        newJob.timerEdited = obj.get("timerEdited").asString

        if( !obj.get("local").isJsonNull)
        newJob.local = obj.get("local").asBoolean

        if( !obj.get("jobRate").isJsonNull)
        newJob.jobRate = obj.get("jobRate").asBoolean

        if( !obj.get("completed").isJsonNull)
        newJob.completed = obj.get("completed").asBoolean

        if( !obj.get("signed").isJsonNull)
        newJob.signed = obj.get("signed").asBoolean

        if( !obj.get("accepted").isJsonNull){
            newJob.accepted = obj.get("accepted").asBoolean
            newJob.declined = !newJob.accepted
        }
        else {
            newJob.declined = false
        }

        if( !obj.get("driverContact").isJsonNull)
        newJob.driverContact = Gson().fromJson(json.asJsonObject["driverContact"].toString(), DriverContact::class.java)

        if( !obj.get("driverEmergencyContact").isJsonNull)
        newJob.driverEmergencyContact = Gson().fromJson(json.asJsonObject["driverEmergencyContact"].toString(), DriverEmergencyContact::class.java)

        if( !obj.get("pickupLocation").isJsonNull)
        newJob.pickupLocation = Gson().fromJson(json.asJsonObject["pickupLocation"].toString(), PickupLocation::class.java)

        if( !obj.get("deliveryLocation").isJsonNull)
        newJob.deliveryLocation = Gson().fromJson(json.asJsonObject["deliveryLocation"].toString(), DeliveryLocation::class.java)

        if( !obj.get("timer").isJsonNull) {
            var contentString = obj.get("timer").asString
            contentString = contentString.replace("\\", "")
            newJob.timer = Gson().fromJson(contentString, JobTimer::class.java)
        }

        if( !obj.get("history").isJsonNull) {
            var contentString1 = obj.get("history").asString
            contentString1.replace("\\", "")
            newJob.history = Gson().fromJson(contentString1, JobHistory::class.java)
        }
        if( !obj.get("additionalInformation").isJsonNull){
            if(!(obj.get("additionalInformation").asString).isBlank()) {
                var contentString = obj.get("additionalInformation").asString
                contentString = contentString.replace("\\", "")
                newJob.additionalInformation = Gson().fromJson(contentString, AdditionalInformation::class.java)
            }
        }
        if( !obj.get("additionalTimeOnJob").isJsonNull){
            newJob.additionalTimeOnJob = obj.get("additionalTimeOnJob").asInt
        }
        return newJob
    }
}