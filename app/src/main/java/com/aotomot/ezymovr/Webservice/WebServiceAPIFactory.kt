package com.aotomot.ezymovr.WebService


import com.aotomot.ezymovr.AppConstants
import com.aotomot.ezymovr.BuildConfig
import com.aotomot.ezymovr.deserializer.DriverDeserializer
import com.aotomot.ezymovr.deserializer.JobListDeserializer
import com.aotomot.ezymovr.models.Driver
import com.aotomot.ezymovr.models.Jobs
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit


object WebServiceAPIFactory {
    val TIMEOUT :Long = 20
    val dateFormat = "yyyy-MM-dd"
    val contentType = "Content-Type"
    val contentTypeValue = "application/json"
    val apiKey = "x-api-key"
    val directionsURL = "https://maps.googleapis.com/maps/api/directions/"
    val onBoardingURL = "https://ezymovrinternal.aotomot.com/"

    //Creating Auth Interceptor to add api_key query in front of all the requests.
    private val authInterceptor = Interceptor {chain->
        val newUrl = chain.request().url()
            .newBuilder()
            .build()

        val newRequest = chain.request()
            .newBuilder()
            .header(contentType, contentTypeValue)
            .header(apiKey, AppConstants.ApiKey)
            .url(newUrl)
            .build()

        chain.proceed(newRequest)
    }
    private val loggingInterceptor =  HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }
    //OkhttpClient for building http request url
    //Not logging the authkey if not debug
    private val webServiceClient =
        if(BuildConfig.DEBUG){
            OkHttpClient().newBuilder()
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(authInterceptor)
                .addInterceptor(loggingInterceptor)
                .build()
        }else{
            OkHttpClient().newBuilder()
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(loggingInterceptor)
                .addInterceptor(authInterceptor)
                .build()
        }

    private val webServiceClient_Directions =

            OkHttpClient().newBuilder()
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(loggingInterceptor)
                .build()

    var gsonDriver = GsonBuilder()
        .registerTypeAdapter(
            Driver::class.java,
            DriverDeserializer()
        )
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .setDateFormat(dateFormat)
        .create()
  var gsonJobList = GsonBuilder()
        .registerTypeAdapter(
            Jobs::class.java,
            JobListDeserializer()
        )
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .setDateFormat(dateFormat)
        .create()
  /*    var gsonBuilder = GsonBuilder()
        .registerTypeAdapter(
            Builders::class.java,
            BuilderDeserializer()
        )
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .setDateFormat(dateFormat)
        .create()
    var gsonHomes = GsonBuilder()
        .registerTypeAdapter(
            Homes::class.java,
            HomesDeserializer()
        )
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .setDateFormat(dateFormat)
        .create()
    var gsonGallery = GsonBuilder()
        .registerTypeAdapter(
            Homes::class.java,
            GalleryDeserializer()
        )
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .setDateFormat(dateFormat)
        .create()*/

    fun retrofit(baseUrl : String,mGson : Gson) : Retrofit = Retrofit.Builder()
        .client(webServiceClient)
        .baseUrl(baseUrl)
        .addConverterFactory(ScalarsConverterFactory.create())
        .addConverterFactory(GsonConverterFactory.create(mGson))
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()
    fun retrofit(baseUrl : String) : Retrofit = Retrofit.Builder()
        .client(webServiceClient)
        .baseUrl(baseUrl)
        .addConverterFactory(ScalarsConverterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()
    var directionsGson = GsonBuilder()
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .setDateFormat(dateFormat)
        .create()
    fun retrofit_directions(baseUrl : String) : Retrofit = Retrofit.Builder()
        .client(webServiceClient_Directions)
        .baseUrl(baseUrl)
        .addConverterFactory(ScalarsConverterFactory.create())
        .addConverterFactory(GsonConverterFactory.create(directionsGson))
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()


    val webServicebApi : WebServiceAPI = retrofit(
        AppConstants.BASE_URL
    ).create(WebServiceAPI::class.java)
   /* val webServiceSegApi : WebServiceAPI = retrofit(
        AppConstants.BASE_URL,
        gsonSeg
    ).create(WebServiceAPI::class.java)
    val webServiceLocApi : WebServiceAPI = retrofit(
        AppConstants.BASE_URL,
        gsonLoc
    ).create(WebServiceAPI::class.java)
    val webServiceBuildersApi : WebServiceAPI = retrofit(
        AppConstants.BASE_URL,
        gsonBuilder
    ).create(WebServiceAPI::class.java)
    val webServiceHomesApi : WebServiceAPI = retrofit(
        AppConstants.BASE_URL,
        gsonHomes
    ).create(WebServiceAPI::class.java)
    val webServiceGalleryApi : WebServiceAPI = retrofit(
        AppConstants.BASE_URL,
        gsonGallery
    ).create(WebServiceAPI::class.java)*/
    val webServiceApi_directions : WebServiceAPI = retrofit_directions(directionsURL).create(WebServiceAPI::class.java)
    val webServiceApi_onBoarding : WebServiceAPI = retrofit_directions(onBoardingURL).create(WebServiceAPI::class.java)
    val webServiceApi_driver : WebServiceAPI = retrofit(AppConstants.BASE_URL,gsonDriver).create(WebServiceAPI::class.java)
    val webServiceApi_joblist : WebServiceAPI = retrofit(AppConstants.BASE_URL, gsonJobList).create(WebServiceAPI::class.java)
}