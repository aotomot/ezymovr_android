package com.aotomot.ezymovr.WebService

import com.aotomot.ezymovr.AppConstants
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.models.*
import com.aotomot.homeworld.models.DeviceRegistrationRequest
import com.aotomot.homeworld.models.DeviceRegistrationResponse
import com.aotomot.homeworld.models.HomeDirections
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface WebServiceAPI {

    @GET("/api/onboarding")
    suspend fun getOnBoarding(@Query("apiKey")  apiKey : String): MutableList<OnBoarding>

    //@POST("device/add")
    @POST
    suspend fun addDeviceForPushNotifications(@Url url : String,@Body postDeviceRegistrationRequest : DeviceRegistrationRequest): Response<DeviceRegistrationResponse>

    //@PUT("/api/device/update/{deviceId}")
    @PUT
    suspend fun updateDeviceForPushNotifications(@Url url : String,@Query("deviceId") deviceId: String?,
                                                 @Body deviceLocation : DeviceRegistrationRequest
    ): Response<DeviceRegistrationResponse>

    @POST()
    suspend fun postLogin(@Url url : String,
    @Body postLoginBody : PostLoginBody): Response<LoginResponse>

   // @GET("/api/driver/byUser/{userId}")
    @GET
    suspend fun getMoverByUserId(@Url url : String,
        @Header("Authorization") authorization: String?
       // @Path("userId") userId: String?
    ): Response<Driver>

    //@GET("/api/job/listByDriverId")
    @GET
    suspend fun getJobList(@Url url : String,
        @Header("Authorization") authorization: String?,
        @Query("id") id: String?
    ): MutableList<Jobs>


    @GET("json")
    suspend fun getDirectionsFromGoogle(@Query("origin") origin: String,
                                        @Query("destination") destination :String,
                                        @Query("key") apiKey:String,
                                        @Query("mode") mode:String,
                                        @Query("departure_time") time:String,
                                        @Query("sensor") sensor:String): HomeDirections

    //@GET("/api/notification/byUserId")
    @GET
    suspend fun getNotification(@Url url : String,
        @Header("Authorization") authorization: String?,
        @Query("id") id: String?
    ): MutableList<AlertNotification>

    //@GET("/api/image/listByJobId")
    @GET
    suspend fun getListjobById(@Url url : String,
        @Header("Authorization") authorization: String?,
        @Query("id") id: String?
    ): MutableList<AttachmentItem>

    //@POST("/api/job/timer/{id}")
    @POST
    suspend fun startOrPauseJob(@Url url : String,
        @Header("Authorization") authorization: String?,
        //@Path("id") id: String?,
        @Query("start") start: Boolean,
        @Query("comment") comment: String?
    ): ResponseBody

    @GET("/api/job/timer/{id}")
    suspend fun getTimer(
        @Header("Authorization") authorization: String?,
        @Path("id") id: String?
    ): JobTimer
   // @PATCH("/api/job/complete/{id}")
    @PATCH
    suspend fun doCompleteJob(@Url url: String,
        @Header("Authorization") authorization: String?
       // @Path("id") id: String?
    ): ResponseBody

    //@PATCH("/api/job/sign/{id}")
    @PATCH
    suspend fun signJob(@Url url: String,
        @Header("Authorization") authorization: String?,
        //@Path("id") id: String?,
        @Query("comment") comment: String?
    ): ResponseBody

    @Multipart
    //@POST("/api/media/uploadImage")
    @POST
    suspend fun postMediaUpload(@Url url: String,
        @Header("Authorization") authorization: String?,
        @Part file: MultipartBody.Part?
    ): UploadImage

    //@PUT("/api/image/{imageId}")
    @PUT
    suspend fun putImageDetails(@Url url : String,
        @Header("Authorization") authorization: String?,
       // @Path("imageId") imageId: String?,
        @Body imagePUTTitleComment: ImagePUTTitleComment?
    ): UploadImage

   // @PATCH("/api/document/assignToJob")
    @PATCH
    suspend fun assignToJob(@Url url: String,
        @Header("Authorization") authorization: String?,
        @Body assignToJobModel: AssignToJobModel?
    ): ResponseBody

    //@POST("/api/user/updatePassword")
    @POST
    suspend fun updatePassword(@Url url :String,
        @Header("Authorization") authorization: String?,
        @Body changePasswordModel: ChangePasswordModel,
        @Query("id") id: String?
    ): ChangePassword

    @POST
    suspend fun addAdditionalTime(@Url url :String,
                                  @Header("Authorization") authorization: String?,
                                  @Header("Content-Type") contentType: String?,
                                  @Query("isAdditional") isAdditional: Boolean?,
                                  @Query("additionalTime") time: Int?,
                                  @Query("comment") comment: String?
    ): ResponseBody
    //@PATCH("/api/job/decline/{id}")
    @PATCH
    suspend fun declineJob(@Url url :String,
        @Header("Authorization") authorization: String?,
       // @Path("id") id: String?,
        @Query("comment") comment: String?
    ):ResponseBody

   // @PATCH("/api/job/accept/{id}")
    @PATCH
    suspend fun acceptJob(@Url url :String,
        @Header("Authorization") authorization: String?
        //@Path("id") id: String?
    ):ResponseBody

    //@PUT("/api/driver/{id}")
    @PUT
    suspend fun updateDriverDetails(@Url url :String,
        @Header("Authorization") authorization: String?,
        //@Path("id") id: String?,
        @Body driverModel: Driver
    ): String

    //@PUT("/api/image/{id}")
    @PUT
    suspend fun updateImageToDriver(@Url url :String,
        @Header("Authorization") authorization: String?,
       // @Path("id") id: String?,
        @Body driverModel: UpdateUserProfile
    ): String

    //@PATCH("/api/notification/markAsRead/{id}")
    @PATCH
    suspend fun markPushAlertToBeRead(@Url url :String,
        @Header("Authorization") authorization: String?
        //@Path("id") id: String?
    ): ResponseBody

    //@GET("/api/settings")
    @GET
    suspend fun getSetting(@Url url :String,
        @Header("Authorization") authorization: String?,
        @Query("key") supportPhone: String?
    ): ResponseBody

    //@PATCH("/api/document/assignToDriver")
    @PATCH
    suspend fun assignToDriver(@Url url :String,
        @Header("Authorization") authorization: String?,
        @Body assignToJobModel: AssignToJobModel
    ): ResponseBody

    //@POST("/api/user/{id}/locationUpdate")
    @POST
    suspend fun doPostLocationUpdate(@Url url :String,
        @Header("Authorization") authorization: String?,
       // @Path("id") id: String?,
        @Body postLocationUpdate: PostLocationUpdate
    ): MutableList<UploadImage>

    //@POST("/api/user/resetPassword")
    @POST
    suspend fun postResetPassword(@Url url : String,
        @Query("email") email: String?
    ): PostMessage
}