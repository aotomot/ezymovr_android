package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class UploadImage(
    @SerializedName("createdBy")
    var createdBy: String = "", // mobiddiction+1@gmail.com
    @SerializedName("createdDate")
    var createdDate: Long = 0, // 1581563067000
    @SerializedName("lastModifiedBy")
    var lastModifiedBy: String = "", // driver@test.com
    @SerializedName("lastModifiedDate")
    var lastModifiedDate: Long = 0, // 1581563096000
    @SerializedName("id")
    var id: Int = 0, // 42988
    @SerializedName("name")
    var name: String = "", // New job assigned
    @SerializedName("state")
    var state: String = "", // Hi Test, a new job has been assigned to you. Job number: #42987
    @SerializedName("url")
    var url: String = "", // null
    @SerializedName("favourite")
    var favourite: Boolean = false, // true
    @SerializedName("deleted")
    var deleted: Boolean = false,
    @SerializedName("clientId")
    var clientId: Int = 0, // null
    @SerializedName("userId")
    var userId: Int = 0, // null
    @SerializedName("newsId")
    var newsId: Int = 0, // null
    @SerializedName("eventId")
    var eventId: Int = 0, // null
    @SerializedName("driverId")
    var driverId: Int = 0,
    @SerializedName("galleryId")
    var galleryId: Int = 0, // null
    @SerializedName("jobId")
    var jobId: Int = 0, // null
    @SerializedName("expiry")
    var expiry: String = "", // READ
    @SerializedName("profile")
    var profile: String = "" // null

)