package com.aotomot.homeworld.models


import com.google.gson.annotations.SerializedName

data class StartLocationX(
    @SerializedName("lat")
    var lat: Double = 0.0, // -33.6906871
    @SerializedName("lng")
    var lng: Double = 0.0 // 150.8217661
)