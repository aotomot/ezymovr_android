package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName("Authorization")
    var authorization: String = "", // Bearer eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJodHRwczovL2xpY2Vuc2luZy5hb3RvbW90LmNvbSIsIk1vYmlBdXRoIjoiVVNFUiIsInN1YiI6ImRyaXZlckB0ZXN0LmNvbSIsImV4cCI6MTU4NTM0OTMxNX0.9LUhJ1-N36JZKEpWn930TMv9aCy_As4HHZMyOBGnnmyd42rnLu-WYugmOItnPODy6IOEJHQyy5NOfE9i6K9kYw
    @SerializedName("firstName")
    var firstName: String = "", // Test
    @SerializedName("termsAccepted")
    var termsAccepted: Boolean = false, // false
    @SerializedName("roles")
    var roles: List<String> = emptyList(),
    @SerializedName("tokenExpiry")
    var tokenExpiry: Long = 0, // 1585349315917
    @SerializedName("userId")
    var userId: Int = 0, // 42979
    @SerializedName("changePassword")
    var changePassword: Boolean = false // false
)