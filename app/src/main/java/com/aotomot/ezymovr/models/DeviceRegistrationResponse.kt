package com.aotomot.homeworld.models


import com.google.gson.annotations.SerializedName

data class DeviceRegistrationResponse(
    @SerializedName("createdBy")
    var createdBy: String = "", // anonymousUser
    @SerializedName("createdDate")
    var createdDate: Long = 0, // 1580790681011
    @SerializedName("lastModifiedBy")
    var lastModifiedBy: String = "", // anonymousUser
    @SerializedName("lastModifiedDate")
    var lastModifiedDate: Long = 0, // 1580790681011
    @SerializedName("id")
    var id: Int = 0, // 20420
    @SerializedName("udid")
    var udid: String = "", // 5696218b-1dde-4dd1-b49d-0fd77b732b64
    @SerializedName("deviceToken")
    var deviceToken: String = "",
    @SerializedName("deviceName")
    var deviceName: String = "", // Samsung SM-G975F
    @SerializedName("deviceOS")
    var deviceOS: String = "Android",
    @SerializedName("deviceType")
    var deviceType: String = "", // ANDROID
    @SerializedName("appVersion")
    var appVersion: String = "", // 1.0
    @SerializedName("active")
    var active: Boolean = false, // true
    @SerializedName("pushNotifications")
    var pushNotifications: Boolean = false, // true
    @SerializedName("awsEndpoint")
    var awsEndpoint: Any? = null, // null
    @SerializedName("user")
    var user: Int = 0, // 0
    @SerializedName("version")
    var version: Any? = null // null
)