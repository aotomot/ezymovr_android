package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class RoleXX(
    @SerializedName("id")
    var id: Int = 0, // 6
    @SerializedName("privileges")
    var privileges: List<PrivilegeXX> = listOf(),
    @SerializedName("name")
    var name: String = "" // ROLE_USER
)