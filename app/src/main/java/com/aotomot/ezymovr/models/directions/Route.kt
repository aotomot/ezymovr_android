package com.aotomot.homeworld.models


import com.google.gson.annotations.SerializedName

data class Route(
    @SerializedName("bounds")
    var bounds: Bounds = Bounds(),
    @SerializedName("copyrights")
    var copyrights: String = "", // Map data ©2020
    @SerializedName("legs")
    var legs: List<Leg> = listOf(),
    @SerializedName("overview_polyline")
    var overviewPolyline: OverviewPolyline = OverviewPolyline(),
    @SerializedName("summary")
    var summary: String = "", // Broadfoot Ave
    @SerializedName("warnings")
    var warnings: List<String> = listOf(),
    @SerializedName("waypoint_order")
    var waypointOrder: List<Any> = listOf()
)