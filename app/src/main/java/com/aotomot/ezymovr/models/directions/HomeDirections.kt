package com.aotomot.homeworld.models


import com.aotomot.ezymovr.models.directions.GeocodedWaypoint
import com.google.gson.annotations.SerializedName

data class HomeDirections(
    @SerializedName("geocoded_waypoints")
    var geocodedWaypoints: List<GeocodedWaypoint> = listOf(),
    @SerializedName("routes")
    var routes: List<Route> = listOf(),
    @SerializedName("status")
    var status: String = "" // OK
)