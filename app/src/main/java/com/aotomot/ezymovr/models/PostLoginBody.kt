package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class PostLoginBody(
    @SerializedName("email")
    var email: String = "", // driver@test.com
    @SerializedName("password")
    var password: String = "" // Pass1234@
)