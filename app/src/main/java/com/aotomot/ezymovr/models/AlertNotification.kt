package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class AlertNotification(
    @SerializedName("createdBy")
    var createdBy: String = "", // mobiddiction+1@gmail.com
    @SerializedName("createdDate")
    var createdDate: Long = 0, // 1581563067000
    @SerializedName("lastModifiedBy")
    var lastModifiedBy: String = "", // driver@test.com
    @SerializedName("lastModifiedDate")
    var lastModifiedDate: Long = 0, // 1581563096000
    @SerializedName("id")
    var id: Int = 0, // 42988
    @SerializedName("notifTitle")
    var notifTitle: String = "", // New job assigned
    @SerializedName("notifText")
    var notifText: String = "", // Hi Test, a new job has been assigned to you. Job number: #42987
    @SerializedName("notifSound")
    var notifSound: Any = Any(), // null
    @SerializedName("push")
    var push: Boolean = false, // true
    @SerializedName("segmentList")
    var segmentList: List<Any> = listOf(),
    @SerializedName("beacon")
    var beacon: Any = Any(), // null
    @SerializedName("os")
    var os: Any = Any(), // null
    @SerializedName("callToAction")
    var callToAction: String = "", // null
    @SerializedName("device")
    var device: Any = Any(), // null
    @SerializedName("user")
    var user: UserX = UserX(),
    @SerializedName("image")
    var image: NotificationImage? = null, // null
    @SerializedName("scheduled")
    var scheduled: Any = Any(), // null
    @SerializedName("status")
    var status: String = "", // READ
    @SerializedName("responseMessage")
    var responseMessage: Any = Any(), // null
    @SerializedName("version")
    var version: Any = Any(), // null
    @SerializedName("systemGenerated")
    var systemGenerated: Boolean = false, // true
    @SerializedName("ref")
    var ref: Ref = Ref(),
    @SerializedName("masterId")
    var masterId: Long = 0, // 1618885658
    @SerializedName("licenceId")
    var licenceId: Int = 0 // 408
)