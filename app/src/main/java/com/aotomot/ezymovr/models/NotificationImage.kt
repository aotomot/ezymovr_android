package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class NotificationImage(
    @SerializedName("id")
    var id: String = "", // mobiddiction+1@gmail.com
    @SerializedName("notification_id")
    var notification_id: String = "",// driver@test.com
    @SerializedName("name")
    var name: String = "",// driver@test.com
    @SerializedName("url")
    var url: String = "",// driver@test.com
    @SerializedName("favourite")
    var favourite: String = "",// driver@test.com
    @SerializedName("deleted")
    var deleted: String = ""// driver@test.com

)