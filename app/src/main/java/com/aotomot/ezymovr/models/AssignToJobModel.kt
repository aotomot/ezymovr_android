package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class AssignToJobModel(
    @SerializedName("imageId")
    var imageId: String = "", // mobiddiction+1@gmail.com
    @SerializedName("assignmentId")
    var assignmentId: String = ""// driver@test.com


)