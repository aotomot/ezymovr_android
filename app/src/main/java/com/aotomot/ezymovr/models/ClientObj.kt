package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class ClientObj(
    @SerializedName("createdBy")
    var createdBy: String = "", // mobiddiction+1@gmail.com
    @SerializedName("createdDate")
    var createdDate: Long = 0, // 1581562427000
    @SerializedName("lastModifiedBy")
    var lastModifiedBy: String = "", // mobiddiction+1@gmail.com
    @SerializedName("lastModifiedDate")
    var lastModifiedDate: Long = 0, // 1581562477000
    @SerializedName("id")
    var id: Int = 0, // 42976
    @SerializedName("name")
    var name: String = "", // Test Client
    @SerializedName("note")
    var note: String = "",
    @SerializedName("adminNote")
    var adminNote: String = "",
    @SerializedName("requirements")
    var requirements: Any? = null, // null
    @SerializedName("abn")
    var abn: String = "", // 000000000
    @SerializedName("phone")
    var phone: Phone? = null,
    @SerializedName("businessUnit")
    var businessUnit: Any? = null, // null
    @SerializedName("enabled")
    var enabled: Boolean = false, // true
    @SerializedName("contacts")
    var contacts: List<ClientContact>? = null,
    @SerializedName("imageId")
    var imageId: Any? = null, // null
    @SerializedName("driverImage")
    var image: List<ClientImage>? = null,
    @SerializedName("deleted")
    var deleted: Boolean = false, // false
    @SerializedName("licenceId")
    var licenceId: Int = 0, // 408
    @SerializedName("location")
    var location: ClientLocation? = null
)