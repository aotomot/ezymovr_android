package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class Jobs(
    @SerializedName("createdBy")
    var createdBy: String = "", // mobiddiction+1@gmail.com
    @SerializedName("createdDate")
    var createdDate: Long = 0, // 1581563067000
    @SerializedName("lastModifiedBy")
    var lastModifiedBy: String = "", // driver@test.com
    @SerializedName("lastModifiedDate")
    var lastModifiedDate: Long = 0, // 1581640538000
    @SerializedName("id")
    var id: Int = 0, // 42987
    @SerializedName("clientId")
    var clientId: Int = 0, // 42976
    @SerializedName("driverId")
    var driverId: Int = 0, // 42983
    @SerializedName("user")
    var user: JobUser = JobUser(),
    @SerializedName("client")
    var client: String = "", // Test Client
    @SerializedName("clientImageUrl")
    var clientImageUrl: String = "", // null
    @SerializedName("clientObj")
    var clientObj: ClientObj? = null,
    @SerializedName("driver")
    var driver: String = "", // Test Driver
    @SerializedName("driverImageUrl")
    var driverImageUrl: String = "", // https://aotomot-uploads.s3-ap-southeast-2.amazonaws.com/1548024559235driverlogiconpngi40034png_i42978.png
    @SerializedName("driverContact")
    var driverContact: DriverContact? = null,
    @SerializedName("driverEmergencyContact")
    var driverEmergencyContact: DriverEmergencyContact? = null,
    @SerializedName("local")
    var local: Boolean = false, // false
    @SerializedName("jobRate")
    var jobRate: Boolean = false, // false
    @SerializedName("rate")
    var rate: Int = 0, // 0
    @SerializedName("chargeSize")
    var chargeSize: String = "",
    @SerializedName("gvm")
    var gvm: Int = 0, // 0
    @SerializedName("tweight")
    var tweight: String = "", // 100
    @SerializedName("pickupLocation")
    var pickupLocation: PickupLocation = PickupLocation(),
    @SerializedName("deliveryLocation")
    var deliveryLocation: DeliveryLocation = DeliveryLocation(),
    @SerializedName("note")
    var note: String = "", // This is test job for testing.
    @SerializedName("adminNote")
    var adminNote: String = "",
    @SerializedName("noteHistory")
    var noteHistory: String = "", // null
    @SerializedName("adminNoteHistory")
    var adminNoteHistory: Any? = null, // null
    @SerializedName("timer")
    var timer: JobTimer = JobTimer(), // {"running":"false","timer":"00:00:00","events":[]}
    @SerializedName("timerEdited")
    var timerEdited: String = "",
    @SerializedName("history")
    var history: JobHistory = JobHistory(), // [{"created":1581563066712,"text":"ASSIGNED TO DRIVER: Test Driver (#42983)","userId":42951},{"created":1581640538431,"text":"ACCEPTED","userId":42979}]
    @SerializedName("startDate")
    var startDate: Long = 0, // 1581535800000
    @SerializedName("startDateActual")
    var startDateActual: Long = 0, // null
    @SerializedName("endDate")
    var endDate: Long = 0, // 1582313400000
    @SerializedName("completedDate")
    var completedDate: Long? = 0, // null
    @SerializedName("completed")
    var completed: Boolean = false, // false
    @SerializedName("signed")
    var signed: Boolean = false, // null
    @SerializedName("accepted")
    var accepted: Boolean = false, // true
    @SerializedName("additionalInformation")
    var additionalInformation: AdditionalInformation = AdditionalInformation(),
    @SerializedName("additionalTimeOnJob")
    var additionalTimeOnJob: Int = 0 ,// 0,
    var declined : Boolean = false
)