package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class ClientImage(
    @SerializedName("id")
    var id: Int = 0, // 42977
    @SerializedName("name")
    var name: String = "", // Group.png
    @SerializedName("url")
    var url: String = "", // https://aotomot-uploads.s3-ap-southeast-2.amazonaws.com/Grouppng_i42977.png
    @SerializedName("favourite")
    var favourite: Boolean = false, // false
    @SerializedName("deleted")
    var deleted: Boolean = false // false
)