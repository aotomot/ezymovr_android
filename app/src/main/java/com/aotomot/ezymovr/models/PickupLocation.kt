package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class PickupLocation(
    @SerializedName("createdBy")
    var createdBy: String = "", // mobiddiction+1@gmail.com
    @SerializedName("createdDate")
    var createdDate: Long = 0, // 1581563067000
    @SerializedName("lastModifiedBy")
    var lastModifiedBy: String = "", // mobiddiction+1@gmail.com
    @SerializedName("lastModifiedDate")
    var lastModifiedDate: Long = 0, // 1581563067000
    @SerializedName("id")
    var id: Int = 0, // 42985
    @SerializedName("version")
    var version: Any? = null, // null
    @SerializedName("suiteNumber")
    var suiteNumber: Any? = null, // null
    @SerializedName("streetName")
    var streetName: String = "", // null
    @SerializedName("streetNumber")
    var streetNumber: Any? = null, // null
    @SerializedName("name")
    var name: String = "", // 22 Market St, Sydney NSW 2000, Australia
    @SerializedName("suburb")
    var suburb: String = "", // null
    @SerializedName("postcode")
    var postcode: Int = 0, // null
    @SerializedName("lat")
    var lat: Double? = null, // -33.8706423
    @SerializedName("lon")
    var lon: Double? = null, // 151.205003
    @SerializedName("state")
    var state: String = "", // null
    @SerializedName("country")
    var country: String = "", // null
    @SerializedName("tags")
    var tags: List<Any>? = null,
    @SerializedName("deleted")
    var deleted: Boolean = false, // false
    @SerializedName("licenceId")
    var licenceId: Int = 0 // 408
)