package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class ChangePassword(
    @SerializedName("message")
    var message: String = "", // mobiddiction+1@gmail.com
    @SerializedName("error")
    var error: String = ""// driver@test.com


)