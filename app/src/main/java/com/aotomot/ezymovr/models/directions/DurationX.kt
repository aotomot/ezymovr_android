package com.aotomot.homeworld.models


import com.google.gson.annotations.SerializedName

data class DurationX(
    @SerializedName("text")
    var text: String = "", // 1 min
    @SerializedName("value")
    var value: Int = 0 // 51
)