package com.aotomot.ezymovr.models.directions


import com.google.gson.annotations.SerializedName

data class Distance(
    @SerializedName("text")
    var text: String = "", // 0.5 km
    @SerializedName("value")
    var value: Int = 0 // 458
)