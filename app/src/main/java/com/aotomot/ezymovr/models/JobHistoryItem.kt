package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class JobHistoryItem(
    @SerializedName("created")
    var created: Long = 0, // 1584334806371
    @SerializedName("text")
    var text: String = "", // SIGNED
    @SerializedName("userId")
    var userId: Int = 0, // 42979
    @SerializedName("comment")
    var comment: String = "" // 2020-03-16 04:01 pm
)