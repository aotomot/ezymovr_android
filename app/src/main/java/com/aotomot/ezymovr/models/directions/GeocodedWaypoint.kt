package com.aotomot.ezymovr.models.directions


import com.google.gson.annotations.SerializedName

data class GeocodedWaypoint(
    @SerializedName("geocoder_status")
    var geocoderStatus: String = "", // OK
    @SerializedName("place_id")
    var placeId: String = "", // ChIJc9URY2acEmsRbgFCPAuQNgQ
    @SerializedName("types")
    var types: List<String> = listOf()
)