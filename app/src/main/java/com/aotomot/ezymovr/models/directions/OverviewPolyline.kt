package com.aotomot.homeworld.models


import com.google.gson.annotations.SerializedName

data class OverviewPolyline(
    @SerializedName("points")
    var points: String = "" // fqslEuxow[cAFcABIcBI_BM_A_@gAYm@yBaEO[bBwA
)