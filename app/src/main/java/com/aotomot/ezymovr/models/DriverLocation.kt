package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class DriverLocation(
    @SerializedName("createdBy")
    var createdBy: Any? = null, // null
    @SerializedName("createdDate")
    var createdDate: Any? = null, // null
    @SerializedName("lastModifiedBy")
    var lastModifiedBy: String = "", // mobiddiction@gmail.com
    @SerializedName("lastModifiedDate")
    var lastModifiedDate: Long = 0, // 1584585627000
    @SerializedName("id")
    var id: Int = 0, // 42982
    @SerializedName("version")
    var version: Any? = null, // null
    @SerializedName("suiteNumber")
    var suiteNumber: Any? = null, // null
    @SerializedName("streetName")
    var streetName: Any? = null, // null
    @SerializedName("streetNumber")
    var streetNumber: Any? = null, // null
    @SerializedName("name")
    var name: String = "", // 22 Market St, Sydney NSW 2000, Australia
    @SerializedName("suburb")
    var suburb: Any? = null, // null
    @SerializedName("postcode")
    var postcode: Any? = null, // null
    @SerializedName("lat")
    var lat: Double? = null, // 12.9774842000
    @SerializedName("lon")
    var lon: Double? = null, // 77.5733936000
    @SerializedName("state")
    var state: Any? = null, // null
    @SerializedName("country")
    var country: Any? = null, // null
    @SerializedName("tags")
    var tags: List<Any>? = null,
    @SerializedName("deleted")
    var deleted: Boolean = false, // false
    @SerializedName("licenceId")
    var licenceId: Int = 0 // 408
)