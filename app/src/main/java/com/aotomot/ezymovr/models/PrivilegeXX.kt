package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class PrivilegeXX(
    @SerializedName("id")
    var id: Int = 0, // 1
    @SerializedName("name")
    var name: String = "" // USER
)