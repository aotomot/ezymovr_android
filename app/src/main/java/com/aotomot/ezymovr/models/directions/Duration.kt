package com.aotomot.ezymovr.models.directions


import com.google.gson.annotations.SerializedName

data class Duration(
    @SerializedName("text")
    var text: String = "", // 5 mins
    @SerializedName("value")
    var value: Int = 0 // 326
)