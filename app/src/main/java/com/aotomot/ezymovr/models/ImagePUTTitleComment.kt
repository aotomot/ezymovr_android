package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class ImagePUTTitleComment(
    @SerializedName("note")
    var note: String = "", // mobiddiction+1@gmail.com
    @SerializedName("name")
    var name: String = "", // driver@test.com
   @SerializedName("jobId")
    var jobId: String = "" // New job assigned

)