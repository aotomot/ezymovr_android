package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class Ref(
    @SerializedName("name")
    var name: String = "", // job
    @SerializedName("id")
    var id: String = "" // 42987
)