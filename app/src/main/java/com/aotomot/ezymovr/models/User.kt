package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("createdBy")
    var createdBy: String = "", // mobiddiction+1@gmail.com
    @SerializedName("createdDate")
    var createdDate: Long = 0, // 1581562827000
    @SerializedName("lastModifiedBy")
    var lastModifiedBy: String = "", // driver@test.com
    @SerializedName("lastModifiedDate")
    var lastModifiedDate: Long = 0, // 1581562908000
    @SerializedName("id")
    var id: Int = 0, // 42979
    @SerializedName("firstName")
    var firstName: String = "", // Test
    @SerializedName("lastName")
    var lastName: String = "", // Driver
    @SerializedName("email")
    var email: String = "", // driver@test.com
    @SerializedName("mobile")
    var mobile: String = "", // 61-000000000
    @SerializedName("password")
    var password: String = "", // $2a$10$wSGQD7tKfQu3v3BnDMES/.AWNyZOMJqyLQnGLwYyZX3KfNH2bSwdy
    @SerializedName("enabled")
    var enabled: Boolean = false, // true
    @SerializedName("description")
    var description: Any? = null, // null
    @SerializedName("invitationAccepted")
    var invitationAccepted: Boolean = false, // false
    @SerializedName("userHistory")
    var userHistory: List<Any>? = null,
    @SerializedName("tags")
    var tags: List<Any>? = null,
    @SerializedName("roles")
    var roles: List<Role>? = null,
    @SerializedName("clientId")
    var clientId: Any? = null, // null
    @SerializedName("changePassword")
    var changePassword: Boolean = false, // false
    @SerializedName("termsAcceptedNew")
    var termsAcceptedNew: Any? = null, // null
    @SerializedName("imageId")
    var imageId: Any? = null, // null
    @SerializedName("driverImage")
    var image: Any? = null, // null
    @SerializedName("deleted")
    var deleted: Boolean = false, // false
    @SerializedName("welcomeMailSentDate")
    var welcomeMailSentDate: Any? = null, // null
    @SerializedName("additionalNote")
    var additionalNote: Any? = null, // null
    @SerializedName("contents")
    var contents: List<Any>? = null,
    @SerializedName("news")
    var news: List<Any>? = null,
    @SerializedName("licenceId")
    var licenceId: Int = 0, // 408
    @SerializedName("using2FA")
    var using2FA: Boolean = false // false
)