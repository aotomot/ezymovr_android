package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class Contact(
    @SerializedName("createdBy")
    var createdBy: Any? = null, // null
    @SerializedName("createdDate")
    var createdDate: Any? = null, // null
    @SerializedName("lastModifiedBy")
    var lastModifiedBy: String = "", // mobiddiction@gmail.com
    @SerializedName("lastModifiedDate")
    var lastModifiedDate: Long = 0, // 1584585627000
    @SerializedName("id")
    var id: Int = 0, // 42980
    @SerializedName("name")
    var name: String = "", // Test Driver
    @SerializedName("contactNumber")
    var contactNumber: String = "", // 61-000000000
    @SerializedName("email")
    var email: Any? = null, // null
    @SerializedName("role")
    var role: String = "", // driver
    @SerializedName("images")
    var images: List<Any>? = null,
    @SerializedName("tags")
    var tags: List<Any>? = null,
    @SerializedName("deleted")
    var deleted: Boolean = false, // false
    @SerializedName("licenceId")
    var licenceId: Int = 0 // 408
)