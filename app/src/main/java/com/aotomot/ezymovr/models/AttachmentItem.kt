package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class AttachmentItem(
    @SerializedName("createdBy")
    var createdBy: String = "", // driver@test.com
    @SerializedName("createdDate")
    var createdDate: Long = 0, // 1584313546000
    @SerializedName("lastModifiedBy")
    var lastModifiedBy: String = "", // driver@test.com
    @SerializedName("lastModifiedDate")
    var lastModifiedDate: Long = 0, // 1584313548000
    @SerializedName("id")
    var id: Int = 0, // 119259
    @SerializedName("name")
    var name: String = "", // Signature
    @SerializedName("state")
    var state: Any = Any(), // null
    @SerializedName("url")
    var url: String = "", // https://aotomot-uploads.s3-ap-southeast-2.amazonaws.com/JPEG202003161006534760010138849748117jpg_i119259.jpg
    @SerializedName("favourite")
    var favourite: Boolean = false, // false
    @SerializedName("deleted")
    var deleted: Boolean = false, // false
    @SerializedName("note")
    var note: String? = null, // 2020-03-16 10:06 am
    @SerializedName("expiry")
    var expiry: Any = Any(), // null
    @SerializedName("tags")
    var tags: List<Any> = listOf(),
    @SerializedName("listOrder")
    var listOrder: Any = Any(), // null
    @SerializedName("profile")
    var profile: Boolean = false // false
)