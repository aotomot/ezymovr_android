package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class PostDeviceRegistration(
    @SerializedName("createdBy")
    var createdBy: String = "", // driver@test.com
    @SerializedName("createdDate")
    var createdDate: Long = 0, // 1585264228309
    @SerializedName("lastModifiedBy")
    var lastModifiedBy: String = "", // driver@test.com
    @SerializedName("lastModifiedDate")
    var lastModifiedDate: Long = 0, // 1585264228309
    @SerializedName("id")
    var id: Int = 0, // 124086
    @SerializedName("udid")
    var udid: String = "", // 7c93a49f-222d-4ad8-856e-1a808ebc82a8
    @SerializedName("deviceToken")
    var deviceToken: String = "", // fXybNMF4HPQ:APA91bG7GMwJM19WjaPXprxlTvm5Hw2JoYphntJjtdTpEFNs1nDD1xkIh6qoUV8LMT_zTlDSVDZuGi7qW2lp2xNTa6smjjxZPBZhZg316sbsNcIVI1z2GIIxTqqwWPagY9HcV9Tcb1OG
    @SerializedName("deviceName")
    var deviceName: String = "", // Samsung SM-G975F
    @SerializedName("deviceOS")
    var deviceOS: String = "",
    @SerializedName("deviceType")
    var deviceType: String = "", // ANDROID
    @SerializedName("appVersion")
    var appVersion: String = "", // 1.0
    @SerializedName("active")
    var active: Boolean = true, // true
    @SerializedName("pushNotifications")
    var pushNotifications: Boolean = true, // true
    @SerializedName("awsEndpoint")
    var awsEndpoint: Any = "", // null
    @SerializedName("user")
    var user: Int = 0, // 42979
    @SerializedName("version")
    var version: Int = 0 // 1
)