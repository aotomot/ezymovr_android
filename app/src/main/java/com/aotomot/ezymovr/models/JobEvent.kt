package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class JobEvent(
    @SerializedName("jobId")
    var jobId: Int = 0, // 73358
    @SerializedName("paused")
    var paused: Boolean = false, // true
    @SerializedName("comment")
    var comment: String = "",
    @SerializedName("eventDate")
    var eventDate: Long = 0 // 1582595583000
)