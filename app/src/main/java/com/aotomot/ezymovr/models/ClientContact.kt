package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class ClientContact(
    @SerializedName("createdBy")
    var createdBy: String = "", // mobiddiction+1@gmail.com
    @SerializedName("createdDate")
    var createdDate: Long = 0, // 1581562427000
    @SerializedName("lastModifiedBy")
    var lastModifiedBy: String = "", // mobiddiction+1@gmail.com
    @SerializedName("lastModifiedDate")
    var lastModifiedDate: Long = 0, // 1581562427000
    @SerializedName("id")
    var id: Int = 0, // 42973
    @SerializedName("name")
    var name: String = "", // Mike Vasavada
    @SerializedName("contactNumber")
    var contactNumber: String  = "", // null
    @SerializedName("email")
    var email: String = "", // contactus@mobiddiction.com.au
    @SerializedName("role")
    var role: Any? = null, // null
    @SerializedName("images")
    var images: List<Any>? = null,
    @SerializedName("tags")
    var tags: List<Any>? = null,
    @SerializedName("deleted")
    var deleted: Boolean = false, // false
    @SerializedName("licenceId")
    var licenceId: Int = 0 // 408
)