package com.aotomot.homeworld.models


import com.google.gson.annotations.SerializedName

data class Southwest(
    @SerializedName("lat")
    var lat: Double = 0.0, // -33.6925161
    @SerializedName("lng")
    var lng: Double = 0.0 // 150.8187657
)