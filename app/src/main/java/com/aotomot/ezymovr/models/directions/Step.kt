package com.aotomot.homeworld.models


import com.google.gson.annotations.SerializedName

data class Step(
    @SerializedName("distance")
    var distance: DistanceX = DistanceX(),
    @SerializedName("duration")
    var duration: DurationX = DurationX(),
    @SerializedName("end_location")
    var endLocation: EndLocationX = EndLocationX(),
    @SerializedName("html_instructions")
    var htmlInstructions: String = "", // Turn <b>right</b> onto <b>Swifthome St</b>
    @SerializedName("polyline")
    var polyline: Polyline = Polyline(),
    @SerializedName("start_location")
    var startLocation: StartLocationX = StartLocationX(),
    @SerializedName("travel_mode")
    var travelMode: String = "", // WALKING
    @SerializedName("maneuver")
    var maneuver: String = "" // turn-right
)