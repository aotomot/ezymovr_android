package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class PostLocationUpdate(
    @SerializedName("lat")
    var lat: Double = 0.0, // mobiddiction+1@gmail.com
    @SerializedName("lon")
    var lon: Double = 0.0// driver@test.com


)