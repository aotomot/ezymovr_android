package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class RoleX(
    @SerializedName("id")
    var id: Int = 0, // 6
    @SerializedName("privileges")
    var privileges: List<PrivilegeX> = listOf(),
    @SerializedName("name")
    var name: String = "" // ROLE_USER
)