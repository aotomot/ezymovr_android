package com.aotomot.homeworld.models


import com.google.gson.annotations.SerializedName

data class EndLocationX(
    @SerializedName("lat")
    var lat: Double = 0.0, // -33.6911912
    @SerializedName("lng")
    var lng: Double = 0.0 // 150.8222084
)