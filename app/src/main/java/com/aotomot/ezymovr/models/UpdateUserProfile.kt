package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class UpdateUserProfile(

    @SerializedName("favourite")
    var favourite: Boolean = false,
    @SerializedName("profile")
    var profile: Boolean = false,
    @SerializedName("note")
    var note: String? = null,
    @SerializedName("state")
    var state: String? = null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("expiry")
    var expiry: Int = 0,
    @SerializedName("driverId")
    var driverId: String? = null,
    @SerializedName("userId")
    var userId: String? = null
)