package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class ChangePasswordModel(
    @SerializedName("oldPassword")
    var oldPassword: String = "", // mobiddiction+1@gmail.com
    @SerializedName("newPassword")
    var newPassword: String = ""// driver@test.com


)