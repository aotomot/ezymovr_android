package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class DriverImage(
    @SerializedName("createdBy")
    var createdBy: String = "", // driver@test.com
    @SerializedName("createdDate")
    var createdDate: Long = 0, // 1584318558000
    @SerializedName("lastModifiedBy")
    var lastModifiedBy: String = "", // mobiddiction@gmail.com
    @SerializedName("lastModifiedDate")
    var lastModifiedDate: Long = 0, // 1584585626000
    @SerializedName("id")
    var id: Int = 0, // 119317
    @SerializedName("name")
    var name: String = "", // Profile photo
    @SerializedName("state")
    var state: String = "",
    @SerializedName("url")
    var url: String = "", // https://aotomot-uploads.s3-ap-southeast-2.amazonaws.com/JPEG202003161130278740465557047438345jpg_i119317.jpg
    @SerializedName("favourite")
    var favourite: Boolean = false, // false
    @SerializedName("deleted")
    var deleted: Boolean = false, // false
    @SerializedName("note")
    var note: String = "",
    @SerializedName("expiry")
    var expiry: Int = 0, // 0
    @SerializedName("tags")
    var tags: List<Any>? = null,
    @SerializedName("listOrder")
    var listOrder: Int = 0, // 0
    @SerializedName("profile")
    var profile: Boolean = false // true
)