package com.aotomot.homeworld.models


import com.google.gson.annotations.SerializedName

data class DistanceX(
    @SerializedName("text")
    var text: String = "", // 69 m
    @SerializedName("value")
    var value: Int = 0 // 69
)