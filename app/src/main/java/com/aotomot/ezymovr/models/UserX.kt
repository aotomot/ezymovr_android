package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class UserX(
    @SerializedName("createdBy")
    var createdBy: String = "", // mobiddiction+1@gmail.com
    @SerializedName("createdDate")
    var createdDate: Long = 0, // 1581562827000
    @SerializedName("lastModifiedBy")
    var lastModifiedBy: String = "", // driver@test.com
    @SerializedName("lastModifiedDate")
    var lastModifiedDate: Long = 0, // 1581562908000
    @SerializedName("id")
    var id: Int = 0, // 42979
    @SerializedName("firstName")
    var firstName: String = "", // Test
    @SerializedName("lastName")
    var lastName: String = "", // Driver
    @SerializedName("email")
    var email: String = "", // driver@test.com
    @SerializedName("mobile")
    var mobile: String = "", // 61-000000000
    @SerializedName("password")
    var password: String = "", // $2a$10$wSGQD7tKfQu3v3BnDMES/.AWNyZOMJqyLQnGLwYyZX3KfNH2bSwdy
    @SerializedName("enabled")
    var enabled: Boolean = false, // true
    @SerializedName("description")
    var description: Any = Any(), // null
    @SerializedName("invitationAccepted")
    var invitationAccepted: Boolean = false, // false
    @SerializedName("userHistory")
    var userHistory: List<Any> = listOf(),
    @SerializedName("tags")
    var tags: List<Any> = listOf(),
    @SerializedName("roles")
    var roles: List<RoleXX> = listOf(),
    @SerializedName("clientId")
    var clientId: Any = Any(), // null
    @SerializedName("changePassword")
    var changePassword: Boolean = false, // false
    @SerializedName("termsAcceptedNew")
    var termsAcceptedNew: Any = Any(), // null
    @SerializedName("imageId")
    var imageId: Any = Any(), // null
    @SerializedName("image")
    var image: Any = Any(), // null
    @SerializedName("deleted")
    var deleted: Boolean = false, // false
    @SerializedName("welcomeMailSentDate")
    var welcomeMailSentDate: Any = Any(), // null
    @SerializedName("additionalNote")
    var additionalNote: Any = Any(), // null
    @SerializedName("contents")
    var contents: List<Any> = listOf(),
    @SerializedName("news")
    var news: List<Any> = listOf(),
    @SerializedName("licenceId")
    var licenceId: Int = 0, // 408
    @SerializedName("using2FA")
    var using2FA: Boolean = false // false
)