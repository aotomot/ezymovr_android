package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class Driver(
    @SerializedName("createdBy")
    var createdBy: String = "", // mobiddiction+1@gmail.com
    @SerializedName("createdDate")
    var createdDate: Long = 0, // 1581562827000
    @SerializedName("lastModifiedBy")
    var lastModifiedBy: String = "", // mobiddiction+1@gmail.com
    @SerializedName("lastModifiedDate")
    var lastModifiedDate: Long = 0, // 1581562827000
    @SerializedName("id")
    var id: Int = 0, // 42983
    @SerializedName("companyName")
    var companyName: String = "", // Test Driver Compay
    @SerializedName("abn")
    var abn: String = "", // 000000000
    @SerializedName("firstName")
    var firstName: String = "", // Test
    @SerializedName("lastName")
    var lastName: String = "", // Driver
    @SerializedName("email")
    var email: String = "", // driver@test.com
    @SerializedName("password")
    var password: String = "", // rKpjgr2$
    @SerializedName("contact")
    var contact: DriverContact = DriverContact(),
    @SerializedName("emergencyContact")
    var emergencyContact: DriverEmergencyContact = DriverEmergencyContact(),
    @SerializedName("multiDriver")
    var multiDriver: Boolean = false, // false
    @SerializedName("hourlyRate")
    var hourlyRate: String = "",
    @SerializedName("gvm")
    var gvm: Int = 0, // 0
    @SerializedName("tweight")
    var tweight: String = "",
    @SerializedName("chargeSize")
    var chargeSize: String = "", // -
    @SerializedName("imageId")
    var imageId: Int = 0,
    @SerializedName("driverImage")
    var driverImage: DriverImage = DriverImage(),
    @SerializedName("enabled")
    var enabled: Boolean = false, // true
    @SerializedName("location")
    var location: DriverLocation = DriverLocation()
)