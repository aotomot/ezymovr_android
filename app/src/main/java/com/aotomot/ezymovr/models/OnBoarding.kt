package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class OnBoarding(
    @SerializedName("createdBy")
    var createdBy: String = "", // mobiddiction@gmail.com
    @SerializedName("createdDate")
    var createdDate: Long = 0, // 1569415597000
    @SerializedName("lastModifiedBy")
    var lastModifiedBy: String = "", // mobiddiction@gmail.com
    @SerializedName("lastModifiedDate")
    var lastModifiedDate: Long = 0, // 1585010186000
    @SerializedName("id")
    var id: Long = 0, // 1371307895046556
    @SerializedName("headline")
    var headline: String = "", // DriverLocation on map
    @SerializedName("shortDescription")
    var shortDescription: String = "", // Pick up to and from location.
    @SerializedName("imageId")
    var imageId: Int = 0, // 123096
    @SerializedName("imageURL")
    var imageURL: String = "", // https://aotomot-uploads.s3-ap-southeast-2.amazonaws.com/2png_i123096.png
    @SerializedName("videoURL")
    var videoURL: String = "",
    @SerializedName("externalURL")
    var externalURL: String = "",
    @SerializedName("enabled")
    var enabled: Boolean = false, // true
    @SerializedName("featured")
    var featured: Boolean = false, // false
    @SerializedName("backgroundColor")
    var backgroundColor: String = "", // #4dc591
    @SerializedName("textColor")
    var textColor: String = "", // #000000
    @SerializedName("buttonColor")
    var buttonColor: String = "", // #FFFFFF
    @SerializedName("buttonTextColor")
    var buttonTextColor: String = "", // #000000
    @SerializedName("backgroundImageId")
    var backgroundImageId: Any? = null, // null
    @SerializedName("backgroundImageURL")
    var backgroundImageURL: Any? = null, // null
    @SerializedName("callToActionText")
    var callToActionText: String = "", // Skip
    @SerializedName("callToActionURL")
    var callToActionURL: String = "", // https://aotomot.com
    @SerializedName("listOrder")
    var listOrder: Long = 0 // 1
)