package com.aotomot.homeworld.models


import com.aotomot.ezymovr.models.directions.Distance
import com.aotomot.ezymovr.models.directions.Duration
import com.google.gson.annotations.SerializedName

data class Leg(
    @SerializedName("distance")
    var distance: Distance = Distance(),
    @SerializedName("duration")
    var duration: Duration = Duration(),
    @SerializedName("end_address")
    var endAddress: String = "", // Swifthome St, Marsden Park NSW 2765, Australia
    @SerializedName("end_location")
    var endLocation: EndLocation = EndLocation(),
    @SerializedName("start_address")
    var startAddress: String = "", // 105 Northbourne Dr, Marsden Park NSW 2765, Australia
    @SerializedName("start_location")
    var startLocation: StartLocation = StartLocation(),
    @SerializedName("steps")
    var steps: List<Step> = listOf(),
    @SerializedName("traffic_speed_entry")
    var trafficSpeedEntry: List<Any> = listOf(),
    @SerializedName("via_waypoint")
    var viaWaypoint: List<Any> = listOf()
)