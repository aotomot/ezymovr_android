package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class JobTimer(
    @SerializedName("running")
    var running: String = "false", // false
    @SerializedName("timer")
    var timer: String = "00:00", // 00:49:10
    @SerializedName("events")
    var events: List<JobEvent> = emptyList()
)