package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class PostMessage(
    @SerializedName("id")
    var id: String = "", // mobiddiction+1@gmail.com
    @SerializedName("message")
    var message: String = ""// driver@test.com


)