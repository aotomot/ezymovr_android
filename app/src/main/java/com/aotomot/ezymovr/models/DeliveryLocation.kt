package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class DeliveryLocation(
    @SerializedName("createdBy")
    var createdBy: String = "", // mobiddiction+1@gmail.com
    @SerializedName("createdDate")
    var createdDate: Long = 0, // 1581563067000
    @SerializedName("lastModifiedBy")
    var lastModifiedBy: String = "", // mobiddiction+1@gmail.com
    @SerializedName("lastModifiedDate")
    var lastModifiedDate: Long = 0, // 1581563067000
    @SerializedName("id")
    var id: Int = 0, // 42986
    @SerializedName("version")
    var version: Any? = null, // null
    @SerializedName("suiteNumber")
    var suiteNumber: Any? = null, // null
    @SerializedName("streetName")
    var streetName:String = "", // null
    @SerializedName("streetNumber")
    var streetNumber: Any? = null, // null
    @SerializedName("name")
    var name: String = "", // Liverpool NSW 2170, Australia
    @SerializedName("suburb")
    var suburb: String = "", // null
    @SerializedName("postcode")
    var postcode: Any? = null, // null
    @SerializedName("lat")
    var lat: Double? = null, // -33.9285567
    @SerializedName("lon")
    var lon: Double? = null, // 150.9179595
    @SerializedName("state")
    var state: String = "", // null
    @SerializedName("country")
    var country: String = "", // null
    @SerializedName("tags")
    var tags: List<Any>? = null,
    @SerializedName("deleted")
    var deleted: Boolean = false, // false
    @SerializedName("licenceId")
    var licenceId: Int = 0 // 408
)