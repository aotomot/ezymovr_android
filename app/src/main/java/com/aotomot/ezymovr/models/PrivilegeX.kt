package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class PrivilegeX(
    @SerializedName("id")
    var id: Int = 0, // 1
    @SerializedName("name")
    var name: String = "" // USER
)