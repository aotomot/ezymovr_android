package com.aotomot.homeworld.models


import com.google.gson.annotations.SerializedName

data class DeviceRegistrationRequest(
    @SerializedName("udid")
    var uuid: String = "default",

    @SerializedName("deviceType")
    var deviceType: String = "default",

    @SerializedName("deviceName")
    var deviceName: String = "default",

    @SerializedName("deviceOS")
    var deviceOS: String = "default",

    @SerializedName("awsEndpoint")
    var awsEndpoint: String = "default",

    @SerializedName("appVersion")
    var appVersion: String = "default",

    @SerializedName("deviceToken")
    var deviceToken: String = "",

    @SerializedName("pushNotifications")
    var pushNotifications: Boolean = true,

    @SerializedName("user")
    var user: Int = 0
)