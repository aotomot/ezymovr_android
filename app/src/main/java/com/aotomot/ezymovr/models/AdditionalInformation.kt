package com.aotomot.ezymovr.models


import com.google.gson.annotations.SerializedName

data class AdditionalInformation(
    @SerializedName("type")
    var type: String = "0 Bedrooms", // mobiddiction+1@gmail.com
    @SerializedName("helpers")
    var helpers: String = "0"// driver@test.com
)