package com.aotomot.ezymovr.extFunctions

import android.content.SharedPreferences
import java.util.*

fun SharedPreferences.AuthToken():String {
    return this.getString("auth_token", "")!!
}

fun SharedPreferences.AuthToken(token :String) {
    with(this.edit()) {
        putString("auth_token", token)
        commit()
    }
}

fun SharedPreferences.username():String {
    return this.getString("username", "")!!
}

fun SharedPreferences.username(token :String) {
    with(this.edit()) {
        putString("username", token)
        commit()
    }
}
fun SharedPreferences.domain():String {
    return this.getString("domain", "")!!
}

fun SharedPreferences.domain(token :String) {
    with(this.edit()) {
        putString("domain", token)
        commit()
    }
}
fun SharedPreferences.password():String {
    return this.getString("password", "")!!
}

fun SharedPreferences.password(token :String) {
    with(this.edit()) {
        putString("password", token)
        commit()
    }
}
fun SharedPreferences.userID():Int {
    return this.getInt("userID", 0)!!
}

fun SharedPreferences.userID(id :Int) {
    with(this.edit()) {
        putInt("userID", id)
        commit()
    }
}
fun SharedPreferences.uuid():String {
    if(getString("uuid" , "").isNullOrBlank()){
        with(this.edit()) {
            putString("uuid", UUID.randomUUID().toString())
            commit()
        }
    }

    return this.getString("uuid", "")!!
}
fun SharedPreferences.FirebaseToken():String {
    return this.getString("firebase_token", "")!!
}

fun SharedPreferences.FirebaseToken(token :String) {
    with(this.edit()) {
        putString("firebase_token", token)
        commit()
    }
}

fun SharedPreferences.isDeviceRegistered():Boolean {
    return this.getBoolean("deviceRegistered", false)!!
}

fun SharedPreferences.isDeviceRegistered(isRegistered :Boolean) {
    with(this.edit()) {
        putBoolean("deviceRegistered", isRegistered)
        commit()
    }
}
fun SharedPreferences.deviceRegisteredID():Int {
    return this.getInt("deviceRegisteredID", 0)!!
}

fun SharedPreferences.deviceRegisteredID(ID :Int) {
    with(this.edit()) {
        putInt("deviceRegisteredID", ID)
        commit()
    }
}
fun SharedPreferences.isonBoardingShown():Boolean {
    return this.getBoolean("onBoarding", false)!!
}


fun SharedPreferences.isonBoardingShown(isRegistered :Boolean) {
    with(this.edit()) {
        putBoolean("onBoarding", isRegistered)
        commit()
    }
}
