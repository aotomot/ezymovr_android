package com.aotomot.ezymovr.extFunctions

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import com.aotomot.ezymovr.R

fun FragmentActivity.getAppSharedPrefs(): SharedPreferences
    { return this.getSharedPreferences(this.resources.getString(R.string.preference_file_key), Context.MODE_PRIVATE)}
