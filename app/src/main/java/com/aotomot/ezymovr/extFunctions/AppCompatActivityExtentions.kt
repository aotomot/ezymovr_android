package com.aotomot.ezymovr.extFunctions

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import com.aotomot.ezymovr.R

fun AppCompatActivity.getAppSharedPrefs(): SharedPreferences
    { return this.getSharedPreferences(this.resources.getString(R.string.preference_file_key), Context.MODE_PRIVATE)}
