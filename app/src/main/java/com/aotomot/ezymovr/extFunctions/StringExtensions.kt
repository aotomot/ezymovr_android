package com.aotomot.ezymovr.extFunctions



fun String.returnDashIfEmpty(): CharSequence? {
    if(this.isNullOrEmpty())
        return "  -- "

    return this
}