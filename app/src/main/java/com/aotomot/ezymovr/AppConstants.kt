package com.aotomot.ezymovr

import com.aotomot.ezymovr.models.Driver
import com.aotomot.ezymovr.models.Jobs
import com.aotomot.homeworld.models.DeviceRegistrationRequest
import java.text.SimpleDateFormat
import java.util.*

/*import com.aotomot.ezymovr.models.DeviceRegistrationRequest
import com.aotomot.ezymovr.models.Segments*/

object AppConstants{
    var DOMAIN_NAME = "dms"
    var BASE_URL = "https://".plus(
        DOMAIN_NAME).plus(".ezymovr.com/api/")
    var ApiKey = "asaifeqwmqfkvkfvdznzyg2323msamn723i2chbfekwf244r"
    var Authorization = ""
    var FirebaseToken = ""
    var mDataForDeviceRegistrationRequest =
        DeviceRegistrationRequest()
    var registeredDeviceID : Int = 0
    var JobsLists : MutableList<Jobs> = mutableListOf<Jobs>()
    var driver : Driver = Driver()


  //  var SegmentsForApp : MutableList<Segments> = mutableListOf()*/
  open fun Epoch2DateStringTimesss(formatString: String?): String? {
      return if (formatString != null && !formatString.equals("null", ignoreCase = true)) {
          val itemLong = formatString.toLong() / 1000
          val d = Date(itemLong * 1000L)
          SimpleDateFormat("dd/MM/yyyy HH:mm").format(d)
      } else {
          "--"
      }
  }


    open fun Epoch2DateStringTimeStartOnlyDate(formatString: String?): String? {
        return if (formatString != null && !formatString.equals("null", ignoreCase = true)) {
            val itemLong = formatString.toLong() / 1000
            val d = Date(itemLong * 1000L)
            SimpleDateFormat("dd MMMM yyyy").format(d)
        } else {
            "--"
        }
    }
    open fun Epoch2DateStringTimeStartDate12Hr(formatString: String?): String? {
        return if (formatString != null && !formatString.equals("null", ignoreCase = true)) {
            val itemLong = formatString.toLong() / 1000
            val d = Date(itemLong * 1000L)
            SimpleDateFormat("dd/MM/yyyy hh:mm a").format(d)
        } else {
            "--"
        }
    }
    open fun Epoch2DateStringTimeStartDate(formatString: String?): String? {
        return if (formatString != null && !formatString.equals("null", ignoreCase = true)) {
            val itemLong = formatString.toLong() / 1000
            val d = Date(itemLong * 1000L)
            SimpleDateFormat("dd/MM/yyyy HH:mm a").format(d)
        } else {
            "--"
        }
    }
    open fun Epoch2DateStringTimeStartTime(formatString: String?): String? {
        return if (formatString != null && !formatString.equals("null", ignoreCase = true)) {
            val itemLong = formatString.toLong() / 1000
            val d = Date(itemLong * 1000L)
            SimpleDateFormat("hh:mm a").format(d)
        } else {
            "--"
        }
    }

    open fun Epoch2DateStringTime(formatString: String?): String? {
        return if (formatString != null && !formatString.equals("null", ignoreCase = true)) {
            val itemLong = formatString.toLong() / 1000
            val d = Date(itemLong * 1000L)
            SimpleDateFormat("d MMMMM yyyy h:mm a").format(d)
        } else {
            "--"
        }
    }
}