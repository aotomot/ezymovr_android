package com.aotomot.ezymovr.FirebaseUtility

import android.app.*
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.aotomot.ezymovr.AppConstants
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.extFunctions.FirebaseToken
import com.aotomot.ezymovr.ui.SplashActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class EzyMovrFirebaseService : FirebaseMessagingService() {

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    override fun onMessageReceived(p0: RemoteMessage) {
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: ${p0?.from}")

        // Check if message contains a data payload.
        p0?.data?.let {
            Log.d(TAG, "Message data payload: " + p0.data)
        }

        // Check if message contains a notification payload.
        p0?.notification?.let {
            Log.d(TAG, "Message Notification Body: ${it.body}")
            sendNotification(it.body!!)


        }

        var intent = Intent()
        intent.action = MESSAGE_RECEIVED
        intent.putExtra("message", p0.notification?.body)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }
    // [END receive_message]

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    override fun onNewToken(p0: String) {
        Log.d(TAG, "Refreshed token: $p0")
        this.getAppSharedPrefs().FirebaseToken(p0!!)
        AppConstants.FirebaseToken = p0!!
        Log.d("Firebase   5"," = " + AppConstants.FirebaseToken)
        var intent = Intent()
        intent.action = TOKEN_RECEIVED
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(p0)
    }
    // [END on_new_token]


    /**
     * Persist token to third-party servers.
     *
     * @param token The new token.
     */
    private fun sendRegistrationToServer(token: String?) {
        AppConstants.FirebaseToken = token!!
        Log.d("Firebase 6"," = " + AppConstants.FirebaseToken)
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private fun sendNotification(messageBody: String) {
        val notificationManager = ContextCompat.getSystemService(applicationContext, NotificationManager::class.java) as NotificationManager
        //sendNotification(messageBody, applicationContext)
    }

    companion object {
        private const val TAG = "MyFirebaseMsgService"
        const val MESSAGE_RECEIVED = "message_received"
        const val TOKEN_RECEIVED = "message_received"
    }
}
private fun EzyMovrFirebaseService.getAppSharedPrefs() : SharedPreferences {
    return this.getSharedPreferences(this.resources.getString(R.string.preference_file_key), Context.MODE_PRIVATE)
}
fun sendNotification(messageBody: String, applicationContext: Context) {

    lateinit var notificationManager: NotificationManager
    lateinit var notificationChannel: NotificationChannel
    lateinit var builder: Notification.Builder
    val channelId = "i.apps.notifications"
    val description = "Test notification"

    val contentIntent = Intent(applicationContext, SplashActivity::class.java)
    contentIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP)
    contentIntent.setAction(Intent.ACTION_MAIN)
    contentIntent.addCategory(Intent.CATEGORY_LAUNCHER)
    contentIntent.putExtra("PUSH_NOTIFICATION", true)
    val contentPendingIntent = PendingIntent.getActivity(
        applicationContext,
        0,
        contentIntent,
        PendingIntent.FLAG_UPDATE_CURRENT
    )

    val eggImage = BitmapFactory.decodeResource(
        applicationContext.resources,
        R.drawable.ic_login_logo
    )
    val bigPicStyle = NotificationCompat.BigPictureStyle()
        .bigPicture(eggImage)
        .bigLargeIcon(null)


    // Build the notification

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val chan1 = NotificationChannel("ezymovr.channel.id", "ezymovr", NotificationManager.IMPORTANCE_DEFAULT)
        chan1.lightColor = Color.GREEN
        chan1.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
        notificationManager.createNotificationChannel(chan1)
       // notificationManager.createNotificationChannel(NotificationChannel("ezymovr.channel.id", "ezymovr", NotificationManager.IMPORTANCE_HIGH))

       val builder = Notification.Builder(applicationContext, "ezymovr.channel.id")
            .setSmallIcon(R.drawable.ic_login_logo)
            .setContentTitle(applicationContext.getString(R.string.app_name))
            .setContentText(messageBody)
            .setLargeIcon(
                BitmapFactory.decodeResource(
                    applicationContext.resources,
                    R.drawable.ic_login_logo
                )
            )
            //.setContentIntent(contentPendingIntent)
           .build()
        notificationManager.notify(1234, builder)
    } else {

        val builder = Notification.Builder(applicationContext)
            .setSmallIcon(R.drawable.ic_login_logo)
            .setLargeIcon(
                BitmapFactory.decodeResource(
                    applicationContext.resources,
                    R.drawable.ic_login_logo
                )
            )
            .setContentIntent(contentPendingIntent)
            .build()
        notificationManager.notify(1234, builder)
    }
}