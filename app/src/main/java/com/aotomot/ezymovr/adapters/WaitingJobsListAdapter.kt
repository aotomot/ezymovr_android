package com.aotomot.ezymovr.adapters

import android.Manifest
import android.app.Activity
import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.provider.MediaStore
import android.util.Log
import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.aotomot.ezymovr.CustomAlert.AlertDialog
import com.aotomot.ezymovr.CustomAlert.OnItemClickListener
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.Repository.CommonRepository
import com.aotomot.ezymovr.WebService.Result
import com.aotomot.ezymovr.WebService.WebServiceAPIFactory
import com.aotomot.ezymovr.extFunctions.domain
import com.aotomot.ezymovr.extFunctions.getAppSharedPrefs
import com.aotomot.ezymovr.extFunctions.gone
import com.aotomot.ezymovr.extFunctions.visible
import com.aotomot.ezymovr.models.Jobs
import com.aotomot.ezymovr.ui.InProgressDetailsActivity
import com.aotomot.ezymovr.ui.SendGalleryPhotoActivity
import com.aotomot.ezymovr.ui.ui.home.HomeViewModel
import com.aotomot.ezymovr.utils.AutoUpdatableAdapter
import com.aotomot.homeworld.models.HomeDirections
import com.github.gcacace.signaturepad.views.SignaturePad
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.*
import com.google.android.material.button.MaterialButton
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*


class WaitingJobsListAdapter(
    private var jobLists: MutableList<Jobs>,
    private val context: Context?,
    private val mHomeScreenActivity: AppCompatActivity,
    private val mViewModel: HomeViewModel,
    private val savedInstanceState: Bundle?
) : RecyclerView.Adapter<WaitingJobsListAdapter.ListViewHolder>(),
    AutoUpdatableAdapter {

    private var startTime = 0L
    private val myHandler = Handler()
    var timeInMillies = 0L
    var timeSwap = 0L
    var finalTime = 0L
    private var is_paused = false
    var sdf = SimpleDateFormat("dd MMM yyyy h:mm a")
    var map: GoogleMap? = null

    private val repository : CommonRepository =
        CommonRepository(WebServiceAPIFactory.webServiceApi_directions, context,mHomeScreenActivity.getAppSharedPrefs().domain())
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ListViewHolder {
        return ListViewHolder(
            LayoutInflater.from(context).inflate(R.layout.waitinglist_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return jobLists.size
    }


    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
    fun updateData(
        jobListsUpdated: MutableList<Jobs>
    ) {

        //this.titles.clear()
        this.jobLists = jobListsUpdated
        //this.infoList.clear()
        notifyDataSetChanged()
    }
    class ListViewHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {

        var mContext :Context? = null
        var mActivity : AppCompatActivity? = null
        var startstopLayout : ConstraintLayout = v.findViewById(R.id.start_stop_layout)
        var startLayout : ConstraintLayout = v.findViewById(R.id.startLayout)
        var stopLayout : ConstraintLayout = v.findViewById(R.id.stopLayout)
        var pauseLayout : ConstraintLayout = v.findViewById(R.id.pauseLayout)
        var jobNumber = v.findViewById<TextView>(R.id.job_number)
        var jobStatus = v.findViewById<TextView>(R.id.job_status_text)
        var timerText = v.findViewById<TextView>(R.id.in_progresss_timer)
        val save = v.findViewById<TextView>(R.id.saveText)
        val signaturePad = v.findViewById<SignaturePad>(R.id.signature_pad)
        val signLayout = v.findViewById<ConstraintLayout>(R.id.signLayout)
        val decline = v.findViewById<MaterialButton>(R.id.decline_btn)
        val accept = v.findViewById<MaterialButton>(R.id.accept_btn)
        val map = v.findViewById<MapView>(R.id.mapView3)
        lateinit var mViewModel: HomeViewModel
        //3
        init {
            v.setOnClickListener(this)
        }

        public fun insert(key:View, value : String): Pair<View, String> {
            val pair = Pair(key, value);
            return pair
        }
        override fun onClick(v: View) {
            Log.d("RecyclerView", "CLICK!")

            var options: ActivityOptions? = null
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                val p1 = insert(jobNumber as View,mContext?.getString(R.string.picture_transition_name)!!)
                val p2= insert(jobStatus as View,mContext?.getString(R.string.jobstatus_transition_name)!!)
                val options = ActivityOptions.makeSceneTransitionAnimation((mContext as Activity)!!,p1,p2)
                val intent = Intent(mContext, InProgressDetailsActivity::class.java)
                intent.putExtra("jobID",jobNumber.text.toString().toInt() )
                intent.putExtra("status",jobStatus.text )
                intent.putExtra("timer",timerText.text )
                ( mContext as Activity)?.startActivityForResult(intent, 3333, options!!.toBundle()!!)
            }
        }

        companion object {
            //5
            private val PHOTO_KEY = "PHOTO"
        }

    }

    fun drawTextToBitmap(mContext: Context, bitmaps: Bitmap, mText: String): Bitmap? {
        return try {
            val resources = mContext.resources
            val scale = resources.displayMetrics.density
            var bitmap = bitmaps
            var bitmapConfig = bitmap.config
            // set default bitmap config if none
            if (bitmapConfig == null) {
                bitmapConfig = Bitmap.Config.ARGB_8888
            }
            // resource bitmaps are imutable,
            // so we need to convert it to mutable one
            bitmap = bitmap.copy(bitmapConfig, true)
            val canvas = Canvas(bitmap)
            // new antialised Paint
            val paint =
                Paint(Paint.ANTI_ALIAS_FLAG)
            // text color - #3D3D3D
            paint.color = Color.rgb(110, 110, 110)
            // text size in pixels
            paint.setTextSize((15 * scale) as Float)
            // text shadow
            paint.setShadowLayer(1f, 0f, 1f, Color.DKGRAY)
            // draw text to the Canvas center
            val bounds = Rect()
            paint.getTextBounds(mText, 0, mText.length, bounds)
            /**
             * bitmap.getWidth()-bounds.right-20 --> meaning get bitmap width then minus the bound right and add another space width from rightby -20
             * bitmap.getHeight()-bounds.top-40 --> meaning get bitmap height then minus the bound top and add another space height from bottom by -40
             */
            //            canvas.drawText("Signed : " +mText, bitmap.getWidth()-bounds.right-200, bitmap.getHeight()-bounds.top-50, paint);
            /**
             *
             * x = 15 --> meaning get the canvas start from x = 15
             * y = bitmap.getHeight()-bounds.top-40 --> meaning get bitmap height then minus the bound top and add another space height from bottom by -40
             */
            canvas.drawText(
                "Signed : $mText",
                15f,
                bitmap.height - bounds.top - 50.toFloat(),
                paint
            )
            bitmap
        } catch (e: Exception) {
            null
        }
    }
    var mPermission =
        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun onsaveTextClicked(id : Int, signaturePad :SignaturePad) {
        if (ContextCompat.checkSelfPermission(context!!.applicationContext,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) !== PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mHomeScreenActivity, mPermission,5)
        } else {
            var bitmap: Bitmap? = signaturePad.getSignatureBitmap()
            signaturePad.clear()
            val date =
                SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.getDefault())
                    .format(Date())
            bitmap = drawTextToBitmap(mHomeScreenActivity, bitmap!!, date)
            if (bitmap == null) {
                Toast.makeText(mHomeScreenActivity, "Please do the signature", Toast.LENGTH_SHORT).show()
            } else {
                val i = Intent(mHomeScreenActivity, SendGalleryPhotoActivity::class.java)
                i.putExtra("imageUri", getImageUri(mHomeScreenActivity, bitmap))
                i.putExtra("album", "no")
                i.putExtra("signature", true)
                i.putExtra("fromcomplete", false)
                i.putExtra("jobId", id.toLong())
                startActivity(context!!,i,null)
            }
        }
    }
    fun getImageUri(inContext: Context, inImage: Bitmap): Uri? {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(inContext.contentResolver, inImage, "Title", null)
        return Uri.parse(path)
    }
    fun updateTimerMethod(timerText: TextView): Runnable = object : Runnable {
        override fun run() {
            timeInMillies = SystemClock.uptimeMillis() - startTime
            finalTime = timeSwap + timeInMillies
            /**
             * display from backend to the timer
             */
            val seconds = (finalTime / 1000).toInt() % 60
            val minutes = (finalTime / (1000 * 60) % 60).toInt()
            val hours = (finalTime / (1000 * 60 * 60)).toInt()
            timerText?.text = (String.format("%02d", hours)
                .plus(":")
                .plus(String.format("%02d", minutes))
                .plus(":" )
                .plus( String.format("%02d", seconds))
                    )
            myHandler.postDelayed(this, 0)
        }
    }
    override fun onBindViewHolder(holder: WaitingJobsListAdapter.ListViewHolder, position: Int) {
        val itemLong = jobLists.get(position).endDate / 1000
        val d = Date(itemLong * 1000L)

        holder.mViewModel = mViewModel
        holder.mContext = context
        holder.mActivity = mHomeScreenActivity
        holder.jobNumber.text = jobLists.get(position).id.toString()
        if(jobLists.get(position).accepted) {
            holder.jobStatus.text = "Accepted"
            holder.accept.isEnabled = false
            holder.accept.isClickable = false
            holder.startstopLayout.visible()
            holder.pauseLayout.gone()
            holder.timerText.gone()
        }
        else {
            holder.jobStatus.text = "Waiting"
            holder.startstopLayout.gone()
            holder.timerText.gone()
        }
        holder.jobStatus.setTextColor(context!!.getColor(R.color.accepted))


        val times = jobLists.get(position).timer.timer.split(":")

        /**CONVERT TIME TO SECOND */
        val second =
            times[0].toInt() * 60 * 60 + times[1].toInt() * 60 + times[2]
                .toInt()

        /**CONVERT TIME TO millisecond */
        /* var timeSwap = 0L
         val millisecond = second * 1000
         val startTime = SystemClock.uptimeMillis() - millisecond
         val timeInMillies = SystemClock.uptimeMillis() - startTime
         val finalTime = timeSwap + timeInMillies
         *//**
         * display from backend to the timer
         *//*
        val seconds = (finalTime / 1000).toInt() % 60
        val minutes = (finalTime / (1000 * 60) % 60).toInt()
        val hours = (finalTime / (1000 * 60 * 60)).toInt()
        holder.timerText?.text = (String.format("%02d", hours)
            .plus(":")
            .plus(String.format("%02d", minutes))
            .plus(":" )
            .plus( String.format("%02d", seconds))
                )*/

        holder.decline.setOnClickListener(object:View.OnClickListener{
            override fun onClick(v: View?) {
                AlertDialog.showDialogWithoutHeaderTwoButtonLaunchWake(
                    context,
                    "Please confirm you wish to decline the job",
                    object : OnItemClickListener {
                        override fun onItemClick(o: Any?, position: Int) {
                            if (position == 0) {
                                /*AppConstant.TrackingEvent(
                                    AnalyticsConstant.job,
                                    AnalyticsConstant.continueJob,
                                    AnalyticsConstant.continueJobClicked + jobList.getId()
                                )*/
                                DeclineJobs(holder,position,holder.mContext!!)
                            } else {
                            }
                        }
                    })

            }

        })

        holder.accept.setOnClickListener(object:View.OnClickListener{
            override fun onClick(v: View?) {

                AcceptJobs(holder,position,holder.mContext!!)
            }

        })
        holder.save.setOnClickListener(object:View.OnClickListener{
            override fun onClick(v: View?) {
                holder.signLayout.gone()
                /*holder.startstopLayout.visible()
                holder.startLayout.gone()
                holder.pauseLayout.visible()
                holder.timerText.visible()*/
                AcceptJobs(holder,position,holder.mContext!!)
                onsaveTextClicked(jobLists.get(position).id,holder.signaturePad)

            }

        })
        holder.startLayout?.setOnClickListener(
            object : View.OnClickListener {
                override fun onClick(view: View) {
                    // AppConstants.TrackingEvent(AnalyticsConstant.job,AnalyticsConstant.pause, AnalyticsConstant.pauseJobClicked + jobList.getId())
                    AlertDialog.showDialogWithoutHeaderTwoButtonLaunchWake(
                        context,
                        "Please confirm you wish to start the timer.",
                        object : OnItemClickListener {
                            override fun onItemClick(o: Any?, position: Int) {
                                if (position == 0) {
                                    /*AppConstant.TrackingEvent(
                                        AnalyticsConstant.job,
                                        AnalyticsConstant.continueJob,
                                        AnalyticsConstant.continueJobClicked + jobList.getId()
                                    )*/
                                    is_paused = false
                                    /*NetworkManager.getInstance().startOrPauseJob(
                                        ModelManager.getInstance().getLogin().getAuthorization(),
                                        jobList.getId().toString() + "",
                                        true,
                                        ""
                                    )*/
                                    holder.startLayout.setVisibility(View.GONE)
                                    holder.stopLayout.setVisibility(View.VISIBLE)
                                    holder.pauseLayout.setVisibility(View.VISIBLE)
                                    holder.signLayout.visible()
                                    //completeJobLayout.setVisibility(View.GONE);
                                    val timeText: String = jobLists[position].timer.timer
                                    val times =timeText.split(":").toTypedArray()

                                    /**CONVERT TIME TO SECOND */
                                    val second = times[0].toInt() * 60 * 60 + times[1]
                                        .toInt() * 60 + times[2].toInt()

                                    /**CONVERT TIME TO millisecond */
                                    val millisecond = second * 1000
                                    startTime = SystemClock.uptimeMillis() - millisecond
                                    myHandler.postDelayed(updateTimerMethod(holder.timerText), 0)
                                } else {
                                }
                            }
                        })
                }
            })
        holder.stopLayout?.setOnClickListener(
            object : View.OnClickListener {
                override fun onClick(view: View) {
                    holder.pauseLayout.setVisibility(View.GONE)
                    holder.startLayout.setVisibility(View.VISIBLE)
                    holder.stopLayout.setVisibility(View.VISIBLE)
                    //completeJobLayout.setVisibility(View.GONE);
                    is_paused = true
                    if (jobLists[position].timer.running.equals("false") ) {
                        /**
                         * dont call pause again because the job already in pause mode other wise it will be duplicate break
                         */
                    } else {
                        /*NetworkManager.getInstance().pauseTimer(
                            ModelManager.getInstance().getLogin().getAuthorization(),
                            jobList.getId().toString(),
                            false, ""
                        )*/
                    }
                    myHandler.removeCallbacks(updateTimerMethod(holder.timerText))
                    AlertDialog.showDialogWithHeaderTwoButton(
                        context,
                        "Is the job completed?",
                        "Press OK to submit",
                        object : OnItemClickListener {
                            override fun onItemClick(o: Any?, position: Int) {
                                if (position == 0) {
                                    /*NetworkManager.getInstance().doCompleteJob(
                                        ModelManager.getInstance().getLogin().getAuthorization(),
                                        jobList.getId().toString()
                                    )*/
                                    /* AppConstant.TrackingEvent(
                                         AnalyticsConstant.job,
                                         AnalyticsConstant.complete,
                                         AnalyticsConstant.completeJobClicked + jobList.getId()
                                     )*/
                                } else {
                                    //cancel do nothing
                                    holder.pauseLayout.setVisibility(View.GONE)
                                    holder.startLayout.setVisibility(View.VISIBLE)
                                    holder.stopLayout.setVisibility(View.VISIBLE)
                                    //completeJobLayout.setVisibility(View.GONE);
                                }
                            }
                        })
                }
            })
        holder.pauseLayout.setOnClickListener(
            object : View.OnClickListener {
                override fun onClick(view: View) {
                    /*AppConstant.TrackingEvent(
                        AnalyticsConstant.job,
                        AnalyticsConstant.pause,
                        AnalyticsConstant.pauseJobClicked + jobList.getId()
                    )*/
                    AlertDialog.showDialogWithoutHeaderTwoButtonLaunchWakeBreak(
                        context,
                        "Are you taking break?",
                        object : OnItemClickListener {
                            override fun onItemClick(o: Any?, position: Int) {
                                if (position == 0) {
                                    is_paused = true


                                    //completeJobLayout.setVisibility(View.GONE);
                                    myHandler.removeCallbacks(updateTimerMethod(holder.timerText))
                                    /*if (AlertDialog.etName.getText().length() > 0) {
                                        NetworkManager.getInstance().startOrPauseJob(
                                            ModelManager.getInstance().getLogin().getAuthorization(),
                                            jobList.getId().toString() + "",
                                            false,
                                            AlertDialog.etName.getText().toString()
                                        )
                                    } else {
                                        NetworkManager.getInstance().startOrPauseJob(
                                            ModelManager.getInstance().getLogin().getAuthorization(),
                                            jobList.getId().toString() + "",
                                            false,
                                            ""
                                        )
                                    }*/
                                    if (AlertDialog.imm.isAcceptingText()) { // verify if the soft keyboard is open
                                        /*AlertDialog.imm.hideSoftInputFromWindow(
                                            quantumMainActivity.getCurrentFocus().getWindowToken(), 0
                                        )*/
                                    }
                                } else {
                                    if (AlertDialog.imm.isAcceptingText()) { // verify if the soft keyboard is open
                                        /*AlertDialog.imm.hideSoftInputFromWindow(
                                            quantumMainActivity.getCurrentFocus().getWindowToken(), 0
                                        )*/
                                    }
                                }
                            }
                        })
                    if (is_paused) {
                        holder.pauseLayout.setVisibility(View.GONE)
                        holder.startLayout.setVisibility(View.VISIBLE)
                        holder.stopLayout.setVisibility(View.VISIBLE)
                    }
                }
            })
        setMap(holder,position)

    }

    private fun setMap(holder: WaitingJobsListAdapter.ListViewHolder, position: Int) {

        holder.map.onCreate(savedInstanceState)
        holder.map.onResume()
        holder.map.getMapAsync(OnMapReadyCallback { googleMap ->
            this.map = googleMap
            this.map!!.setMapType(GoogleMap.MAP_TYPE_NORMAL)
            MapsInitializer.initialize(context)
            if (ActivityCompat.checkSelfPermission(
                    this.context!!,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) !== PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this.context!!,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) !== PackageManager.PERMISSION_GRANTED
            ) {

                return@OnMapReadyCallback
            }

            this.map!!.setMyLocationEnabled(true)
            this.map!!.setMapType(GoogleMap.MAP_TYPE_NORMAL)
            val builder = LatLngBounds.Builder()
            val pickupLoc = Location("pickup")
            with (pickupLoc){
                latitude = jobLists.get(position).pickupLocation!!.lat!!
                longitude = jobLists.get(position).pickupLocation!!.lon!!
            }
            val deliveryLoc= Location("delivery")
            with (deliveryLoc){
                latitude = jobLists.get(position).deliveryLocation!!.lat!!
                longitude = jobLists.get(position).deliveryLocation!!.lon!!
            }
            val deliveryLatLong =
                LatLng(
                    deliveryLoc.latitude,
                    deliveryLoc.longitude
                )
            val deliveryTitle =
                "Delivery to : " + jobLists.get(position).deliveryLocation!!.name
            if (deliveryTitle != null) {
                val deliveryMarkerOptions = MarkerOptions()
                    .title("To")
                    .icon(bitmapDescriptorFromVector(context!!,R.drawable.ic_marker))
                    .position(deliveryLatLong)
                val dd: Marker = this.map!!.addMarker(deliveryMarkerOptions)
                builder.include(deliveryMarkerOptions.position)
                //dd.showInfoWindow();
            }
            val pickupLatLong =
                LatLng(
                    pickupLoc.latitude,
                    pickupLoc.longitude
                )


            val pickupTitle = "Pickup from : " + jobLists.get(position).pickupLocation!!.name
            if (pickupTitle != null) {
                val pickupMarkerOptions = MarkerOptions()
                    .title("From")
                    .icon(bitmapDescriptorFromVector(context!!,R.drawable.ic_marker))
                    .position(pickupLatLong)
                builder.include(pickupMarkerOptions.position)
                val mm: Marker = this.map!!.addMarker(pickupMarkerOptions)
                // mm.showInfoWindow();
            }

            val origin = pickupLoc.latitude.toString().plus(",").plus(pickupLoc.longitude.toString())
            val destination = deliveryLoc.latitude.toString().plus(",").plus(deliveryLoc.longitude.toString())
            fetchDirections(origin,destination,"")

            val builderBounds = LatLngBounds.Builder()
            builderBounds.include(pickupLatLong)
            builderBounds.include(deliveryLatLong)
            val bounds = builder.build()
            val width = context!!.resources.displayMetrics.widthPixels
            val height = context!!.resources.displayMetrics.heightPixels
            val padding =
                (width * 0.15).toInt() // offset from edges of the map 10% of screen
            val cu =
                CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding)

            this.map!!.animateCamera(cu);
            this.map!!.setMyLocationEnabled(true)
            //this.map!!.animateCamera(CameraUpdateFactory.newLatLngZoom(pickupLatLong, 10.5f))
            this.map!!.setOnMapClickListener(GoogleMap.OnMapClickListener {
                val intent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(
                        "http://maps.google.com/maps?saddr=" + pickupLoc.latitude
                            .toString() + "," + pickupLoc.longitude
                            .toString() + "&daddr=" + deliveryLoc.longitude
                            .toString() + "," + deliveryLoc.longitude
                    )
                )
                context.startActivity(intent)
            })
        })
    }

    fun fetchDirections(origin:String,destination:String,apiKey:String){
        CoroutineScope(Dispatchers.IO).launch {
            //  val response : Result<String> = repository.getDirectionsString(origin,destination)
            val response  = repository.getDirections(origin,destination,mHomeScreenActivity.getString(R.string.google_directionsapi_key))
            when(response) {
                is Result.Success -> withContext(Dispatchers.Main){setPolylinesAndMarkers((response.data))}
                // is Result.Success -> _directions.postValue(parseDirectionResult(response.data))
                is Result.Error -> (response.exception.toString())
            }
        }

    }

    fun setPolylinesAndMarkers(mDirections : HomeDirections){
        if((mDirections != null) && (this.map!! != null)) {
            var polyline: LatLng
            val mPolylineOptions = PolylineOptions()
            val builder = LatLngBounds.Builder()
            if(mDirections.routes.size == 0){
                return
            }
            val selectedRoute = mDirections.routes.get(0)
            with(selectedRoute) {

                for (indexLegs in 0..legs.size.minus(1)) {
                    //Add Moarkers for Star & End Location
                    with(legs[indexLegs]) {
                        /* map!!.addMarker(MarkerOptions().position(LatLng(d.lat, startLocation.lng)))*//*.setIcon(
                            BitmapDescriptorFactory.fromResource(com.google.android.gms.maps.R.drawable.icon_current_marker))*//*
                        map!!.addMarker(MarkerOptions().position(LatLng(endLocation.lat, endLocation.lng)))*//*.setIcon(
                            BitmapDescriptorFactory.fromResource(com.google.android.gms.maps.R.drawable.icon_dest_marker))*//*

                        builder.include(LatLng(startLocation.lat, startLocation.lng))
                        builder.include(LatLng(endLocation.lat, endLocation.lng))*/
                        for (indexSteps in 0..legs[indexLegs].steps.size.minus(1)) {
                            with(legs[indexLegs].steps[indexSteps]) {
                                //Add to Polylin Options
                                mPolylineOptions.add(LatLng(startLocation.lat, startLocation.lng))
                                mPolylineOptions.add(LatLng(endLocation.lat, endLocation.lng))

                                //Add to Bounds
                                builder.include(LatLng(startLocation.lat, startLocation.lng))
                                builder.include(LatLng(endLocation.lat, endLocation.lng))
                            }
                        }
                    }
                }
            }
            //Draw Polyline
            val polylines = this.map!!.addPolyline(mPolylineOptions)
            polylines.color = android.graphics.Color.RED
            polylines.isGeodesic = true
            polylines.width = 12F

            //Include in the dispaly screen
            /*with(builder.build()) {

                val width = context!!.resources.displayMetrics.widthPixels
                val height = context.resources.displayMetrics.heightPixels
                val padding =
                    (width * 0.01).toInt() // offset from edges of the map 10% of screen
                val cu =
                    CameraUpdateFactory.newLatLngBounds(this, width, height, padding)
               // map!!.isMyLocationEnabled = true
               // map!!.animateCamera(CameraUpdateFactory.newLatLngZoom(pickupLatLong, 10.5f))
            }*/

        }

    }

    private fun bitmapDescriptorFromVector(context: Context, @DrawableRes vectorDrawableResourceId: Int): BitmapDescriptor? {
        val background =
            ContextCompat.getDrawable(context, R.drawable.ic_marker)
        background!!.setBounds(0, 0, background.intrinsicWidth, background.intrinsicHeight)
        val vectorDrawable = ContextCompat.getDrawable(context, vectorDrawableResourceId)
        vectorDrawable!!.setBounds(
            23,
            23,
            vectorDrawable.intrinsicWidth + 23,
            vectorDrawable.intrinsicHeight + 23
        )
        val bitmap = Bitmap.createBitmap(
            background.intrinsicWidth,
            background.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        // background.draw(canvas)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    private fun DeclineJobs(holder: ListViewHolder,position : Int, context : Context){
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.DeclineJob(jobLists.get(position).id.toString(),"")

            when (response) {
                is com.aotomot.ezymovr.WebService.Result.Success<*> -> {
                    withContext(Dispatchers.Main) {
                        AlertDialog.showDialogWithAlertHeaderSingleButton(
                            context,
                            "Job Declined",
                            "Job Declined Successfull",
                            object : OnItemClickListener {
                                override fun onItemClick(o: Any?, position: Int) {
                                    /*if (position == 0) {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            jobLists.removeIf { (it.id == jobLists.get(position).id) }
                                        } else {
                                            for (index in 0..jobLists.size.minus(1)) {
                                                if (jobLists.get(index).id == jobLists.get(position).id) {
                                                    jobLists.removeAt(index)
                                                }
                                            }
                                        }
                                        this@WaitingJobsListAdapter.notifyDataSetChanged()
                                    }*/

                                    holder.mViewModel.fetchjobsList()
                                }

                            })
                    }
                }
                is com.aotomot.ezymovr.WebService.Result.Error -> {
                    AlertDialog.showDialogWithAlertHeaderSingleButton(context, "Error", "Something went Wrong \n Try again", object: OnItemClickListener{
                        override fun onItemClick(o: Any?, position: Int) {
                            if(position == 0){

                            }
                        }

                    })
                }
            }

        }
    }

    private fun AcceptJobs(holder: ListViewHolder, position: Int, context: Context){
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.AcceptJob(jobLists.get(position).id.toString())

            when (response) {
                is com.aotomot.ezymovr.WebService.Result.Success<*> -> {
                    withContext(Dispatchers.Main) {
                        holder.accept.isEnabled = false
                        holder.accept.isClickable = false
                        /*holder.startstopLayout.visible()
                        holder.pauseLayout.gone()*/
                        holder.jobStatus.text = "Accepted"
                        holder.mViewModel.fetchjobsList()
                    }
                }
                is com.aotomot.ezymovr.WebService.Result.Error -> {
                    AlertDialog.showDialogWithAlertHeaderSingleButton(context, "Error", "Something went Wrong \n Try again", object: OnItemClickListener{
                        override fun onItemClick(o: Any?, position: Int) {
                            if(position == 0){

                            }
                        }

                    })
                }
            }

        }
    }
}