package com.aotomot.ezymovr.adapters

import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aotomot.ezymovr.AppConstants
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.Repository.CommonRepository
import com.aotomot.ezymovr.WebService.Result
import com.aotomot.ezymovr.WebService.WebServiceAPIFactory
import com.aotomot.ezymovr.extFunctions.domain
import com.aotomot.ezymovr.extFunctions.getAppSharedPrefs
import com.aotomot.ezymovr.extFunctions.userID
import com.aotomot.ezymovr.models.Jobs
import com.aotomot.ezymovr.models.OnBoarding
import com.aotomot.ezymovr.ui.OnBoardingActivity
import com.aotomot.homeworld.models.HomeDirections
import kotlinx.coroutines.launch
import retrofit2.Response

class UserProfileViewModel(private val mParentActivity : AppCompatActivity) : ViewModel() {

    private val repository : CommonRepository =
        CommonRepository(WebServiceAPIFactory.webServiceApi_joblist, mParentActivity,mParentActivity.getAppSharedPrefs().domain())

    private val repository_directions : CommonRepository =
        CommonRepository(WebServiceAPIFactory.webServiceApi_directions, mParentActivity,mParentActivity.getAppSharedPrefs().domain())

    private val _jobList = MutableLiveData<MutableList<Jobs>>()
    val jobList : LiveData<MutableList<Jobs>>
        get() = _jobList

    private val _directions = MutableLiveData<HomeDirections>()
    val directions: LiveData<HomeDirections> = _directions

    val _error = MutableLiveData<String>()
    val error : LiveData<String>
        get() = _error

    init {
        fetchjobsList()
    }
    fun fetchjobsList(){

        viewModelScope.launch {
            val response = repository.getjobDetails(AppConstants.driver.id.toString())
            when (response) {
                is Result.Success<*> -> _jobList.postValue(response.data as MutableList<Jobs>)
                is Result.Error -> _error.postValue(response.exception.toString())
            }
            Log.d("","")
        }
    }

    fun fetchDirections(origin:String,destination:String,apiKey:String){
        viewModelScope.launch {
            //  val response : Result<String> = repository.getDirectionsString(origin,destination)
            val response  = repository_directions.getDirections(origin,destination,mParentActivity.getString(
                R.string.google_directionsapi_key))
            when(response) {
                is Result.Success -> _directions.postValue((response.data))
                // is Result.Success -> _directions.postValue(parseDirectionResult(response.data))
                is Result.Error -> _error.postValue(response.exception.toString())
            }
        }

    }
}