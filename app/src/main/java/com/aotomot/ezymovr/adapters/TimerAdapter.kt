package com.aotomot.ezymovr.adapters

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.aotomot.ezymovr.AppConstants
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.models.AttachmentItem
import com.aotomot.ezymovr.models.JobHistory
import com.aotomot.ezymovr.models.JobTimer
import com.aotomot.ezymovr.utils.AutoUpdatableAdapter
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.attachment_item.view.*
import kotlinx.android.synthetic.main.timer_listview_item.view.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class TimerAdapter(private val mActivity: AppCompatActivity,
                   private var mGalleryAllImages: JobHistory?
) :RecyclerView.Adapter<TimerAdapter.ListViewHolder>(),
    AutoUpdatableAdapter {
    var circularProgressDrawable = CircularProgressDrawable(mActivity)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        return ListViewHolder(
            LayoutInflater.from(mActivity).inflate(R.layout.timer_listview_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return mGalleryAllImages!!.size
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val modeTwo = position % 2
        val item = mGalleryAllImages?.get(position)
        val dM = Resources.getSystem().displayMetrics
        val sdf = SimpleDateFormat("dd/MM/yyyy HH:mm a")
        var joinDate : Date
        try {
            joinDate = sdf.parse(AppConstants.Epoch2DateStringTimeStartDate(item?.created.toString()!!))
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        with(holder) {
            timerText.text = AppConstants.Epoch2DateStringTimeStartDate(item?.created.toString())
            context = mActivity
            statusText.text = item!!.text

        }
    }
    class ListViewHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {

        val statusText = v.status
        val timerText = v.timerText
        lateinit var context : Context
        lateinit var imageUrl : kotlin.String
        lateinit var imageName : kotlin.String
        lateinit var imageNote : kotlin.String

        init {
            v.setOnClickListener(this)
        }
        override fun onClick(v: View) {
                        /*val intent = Intent(context,
                            ViewHomeImagesActivity::class.java)
                        intent.putExtra("isGalleryList" , false)
                        intent.putExtra("galleryImageUrl" , imageUrl)
                        intent.putExtra("galleryImageName" , imageName)
                        intent.putExtra("galleryImageNote" , imageNote)
                        startActivity(context,intent,null)*/
                }
    }

    fun update (mUpdatedGalleryAllImages: JobHistory){
        this.mGalleryAllImages = mUpdatedGalleryAllImages
        notifyDataSetChanged()
    }

}