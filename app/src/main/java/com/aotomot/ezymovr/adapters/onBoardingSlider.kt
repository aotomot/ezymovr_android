package com.aotomot.ezymovr.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import androidx.viewpager.widget.PagerAdapter
import com.aotomot.ezymovr.models.OnBoarding
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.extFunctions.gone
import com.aotomot.ezymovr.extFunctions.visible
import com.bumptech.glide.Glide


class onBoardingSlider(private val mContext: Context, private var mList: MutableList<OnBoarding>): PagerAdapter() {

    val circularProgressDrawable = CircularProgressDrawable(mContext!!)

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {
        return mList.size
    }

    override fun instantiateItem(collection: ViewGroup, position: Int): Any {

        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.setColorSchemeColors(mContext.getColor(R.color.colorAccent),mContext.getColor(R.color.colorPrimary), mContext.getColor(R.color.colorPrimaryDark))
        circularProgressDrawable.start()

        val inflater = LayoutInflater.from(mContext)
        val layout = inflater.inflate(R.layout.onboarding_layout, collection, false) as ViewGroup
        val imageView = layout.findViewById<View>(R.id.onboarding_image) as ImageView
        val tiltle = layout.findViewById<View>(R.id.text_title) as TextView
        val desc = layout.findViewById<View>(R.id.description) as TextView

        if(mList != null) {
            if(mList[position] != null) {
                with(mList[position]) {
                    //   val imageViewTarget =GlideDrawableImageViewTarget(imageView)
                    if(!imageURL.isNullOrEmpty()) {
                        if (imageURL!!.endsWith("gif", true))
                            Glide.with(mContext).asGif().placeholder(circularProgressDrawable).load(
                                this.imageURL
                            ).into(
                                imageView
                            )
                        else
                            Glide.with(mContext).asBitmap().placeholder(circularProgressDrawable).load(
                                this.imageURL
                            ).into(
                                imageView
                            )

                        tiltle.setText(this.headline)
                        desc.setText(this.shortDescription)
                    }
                }
            }
        }
        collection.addView(layout)
        return layout
    }

    override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
        collection.removeView(view as View)
    }

    fun updateData(mListUpdated: MutableList<OnBoarding>) {
        Log.d("onBoardingSlider","updateData")
        this.mList = mListUpdated
        notifyDataSetChanged()
    }
}