package com.aotomot.ezymovr.adapters

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.models.AttachmentItem
import com.aotomot.ezymovr.utils.AutoUpdatableAdapter
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.attachment_item.view.*

class AttachListAdapter(private val mActivity: AppCompatActivity,
                        private var mGalleryAllImages: MutableList<AttachmentItem>?
) :RecyclerView.Adapter<AttachListAdapter.ListViewHolder>(),
    AutoUpdatableAdapter {
    var circularProgressDrawable = CircularProgressDrawable(mActivity)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        circularProgressDrawable = CircularProgressDrawable(mActivity)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.setColorSchemeColors((mActivity.resources.getColor(R.color.colorAccent)))
        circularProgressDrawable.start()

        return ListViewHolder(
            LayoutInflater.from(mActivity).inflate(R.layout.attachment_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return mGalleryAllImages!!.size
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val modeTwo = position % 2
        val item = mGalleryAllImages?.get(position)
        val dM = Resources.getSystem().displayMetrics

        with(holder) {
            /*when (modeTwo) {
                0 -> imageView.layoutParams.height = dM.heightPixels / 5
                1 -> imageView.layoutParams.height = dM.heightPixels / 4
            }*/

            imageView.layoutParams.width = dM.widthPixels / 2
            imageTitle.text = item!!.note
            context = mActivity
            imageUrl = item?.url!!
            imageName = item.name
            if(item.note != null)
                imageNote = item.note!!
            else
                imageNote = "--"
            Glide.with(mActivity)
                .asBitmap()
                .placeholder(circularProgressDrawable)
                .load(imageUrl)
                .into(imageView)
        }
    }
    class ListViewHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {

        val imageView = v.attach_img
        val imageTitle = v.attach_text
        lateinit var context : Context
        lateinit var imageUrl : kotlin.String
        lateinit var imageName : kotlin.String
        lateinit var imageNote : kotlin.String

        init {
            v.setOnClickListener(this)
        }
        override fun onClick(v: View) {
                        /*val intent = Intent(context,
                            ViewHomeImagesActivity::class.java)
                        intent.putExtra("isGalleryList" , false)
                        intent.putExtra("galleryImageUrl" , imageUrl)
                        intent.putExtra("galleryImageName" , imageName)
                        intent.putExtra("galleryImageNote" , imageNote)
                        startActivity(context,intent,null)*/
                }
    }

    fun update (mUpdatedGalleryAllImages: MutableList<AttachmentItem>){
        this.mGalleryAllImages = mUpdatedGalleryAllImages
        notifyDataSetChanged()
    }

}