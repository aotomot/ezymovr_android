package com.aotomot.ezymovr.adapters

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.aotomot.ezymovr.AppConstants
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.models.AlertNotification
import com.aotomot.ezymovr.utils.AutoUpdatableAdapter
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.notification_listview_item.view.*
import java.text.SimpleDateFormat
import java.util.*

class AlertNotificationListAdapter(private val mActivity: AppCompatActivity,
                                   private var mAlerts: MutableList<AlertNotification>?
) :RecyclerView.Adapter<AlertNotificationListAdapter.ListViewHolder>(),
    AutoUpdatableAdapter {
    private val mRandom = Random()
    private val imageWidth = Resources.getSystem().displayMetrics.widthPixels / 2

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        return ListViewHolder(
            LayoutInflater.from(mActivity).inflate(R.layout.notification_listview_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return mAlerts!!.size
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {

        val dateCreated : Date = Date(mAlerts?.get(position)?.createdDate!!)

        val formatTime = SimpleDateFormat("hh:mm aa")
        val formatDate = SimpleDateFormat("dd MMMM")

        val dt = formatTime.format(dateCreated)
        val dtDate = formatDate.format(dateCreated)

        with(holder){
            context = mActivity
            titile.setText(mAlerts?.get(position)?.notifTitle)
            caption.setText(mAlerts?.get(position)?.notifText)
            notifTime.setText(dt.plus(" on ").plus(dtDate))
            if(!(mAlerts?.get(position)?.image?.url.isNullOrBlank())){
                    Glide.with(mActivity).asBitmap().load(mAlerts?.get(position)?.image?.url).into(holder.imageView)
                }
           if(mAlerts?.get(position)?.callToAction.isNullOrEmpty()){
               callToAction.visibility = View.GONE
           }else if(mAlerts?.get(position)?.callToAction!!.contains("https://")){
               callToAction.setOnClickListener(object : View.OnClickListener{

                   override fun onClick(v: View?) {
                       val intent = Intent(Intent.ACTION_VIEW)
                       intent.data =
                           Uri.parse(mAlerts?.get(position)?.callToAction)

                       val title = "Please Select a browser to open"
                       val chooser = Intent.createChooser(intent, title)
                       if (intent.resolveActivity(mActivity.getPackageManager()) != null) {
                           startActivity(mActivity,chooser,null)
                       }

                   }

               })
           }
            /*if (mAlerts?.get(position)?.image.isNullOrEmpty())
                Glide.with(mActivity).asBitmap().load(mActivity.getDrawable(R.drawable.alert_default)).into(imageView)
            else
                Glide.with(mActivity).asBitmap().load(mAlerts?.get(position)?.image).into(imageView)*/
        }

    }
    class ListViewHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {

        val imageView = v.imageListItem
        val titile = v.title
        val caption = v.caption
        val notifTime = v.date
        val callToAction = v.moreInfo
        lateinit var context : Context
        lateinit var callUrl : kotlin.String
        lateinit var imageName : kotlin.String
        lateinit var imageNote : kotlin.String


        init {
            v.setOnClickListener(this)
        }
        override fun onClick(v: View) {

                }
    }

    fun update (mUpdatedAlerts: MutableList<AlertNotification>){
        this.mAlerts = mUpdatedAlerts
        notifyDataSetChanged()
    }

}