package com.aotomot.ezymovr.adapters

import android.Manifest
import android.app.Activity
import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.aotomot.ezymovr.Repository.CommonRepository
import com.aotomot.ezymovr.WebService.Result
import com.aotomot.ezymovr.WebService.WebServiceAPIFactory
import com.aotomot.ezymovr.models.Jobs
import com.aotomot.ezymovr.utils.AutoUpdatableAdapter
import com.aotomot.homeworld.models.HomeDirections
import com.google.android.gms.maps.*
import com.google.android.gms.maps.GoogleMap.OnMapClickListener
import com.google.android.gms.maps.model.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.extFunctions.domain
import com.aotomot.ezymovr.extFunctions.getAppSharedPrefs
import com.aotomot.ezymovr.ui.CompleteActivity
import kotlinx.coroutines.withContext


class CompletedJobsListAdapter(
    private var jobLists: MutableList<Jobs>,
    private val context: Context?,
    private val mHomeScreenActivity: AppCompatActivity,
    private val savedInstanceState: Bundle?
) : RecyclerView.Adapter<CompletedJobsListAdapter.ListViewHolder>(),
    AutoUpdatableAdapter {

    var sdf = SimpleDateFormat("dd MMM yyyy h:mm a")
    var map: GoogleMap? = null

   lateinit var repository : CommonRepository
   lateinit var repository_directions : CommonRepository
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ListViewHolder {
        repository =  CommonRepository(WebServiceAPIFactory.webServiceApi_directions, context,mHomeScreenActivity.getAppSharedPrefs().domain())
        return ListViewHolder(
            LayoutInflater.from(context).inflate(R.layout.joblist_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
       return jobLists.size
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {

        setMap(holder,position)
        val itemLong = jobLists.get(position).endDate / 1000
        val d = Date(itemLong * 1000L)

        holder.mContext = context
        holder.mActivity = mHomeScreenActivity
        if(jobLists.get(position).completed)
            holder.titleLayout.setBackgroundColor(context!!.getColor(R.color.completed))
        else
            holder.titleLayout.setBackgroundColor(context!!.getColor(R.color.expired))

        holder.jobNumber.text = jobLists.get(position).id.toString()
        holder.jobStatus.text = "Completed"
        holder.jobStatusDate.text = sdf.format(d)

        val times = jobLists.get(position).timer.timer.split(":")

        /**CONVERT TIME TO SECOND */
        val second =
            times[0].toInt() * 60 * 60 + times[1].toInt() * 60 + times[2]
                .toInt()

        /**CONVERT TIME TO millisecond */
        var timeSwap = 0L
        val millisecond = second * 1000
        val startTime = SystemClock.uptimeMillis() - millisecond
        val timeInMillies = SystemClock.uptimeMillis() - startTime
        val finalTime = timeSwap + timeInMillies
        /**
         * display from backend to the timer
         */
        val seconds = (finalTime / 1000).toInt() % 60
        val minutes = (finalTime / (1000 * 60) % 60).toInt()
        val hours = (finalTime / (1000 * 60 * 60)).toInt()
        holder.jobStatusTime?.text = (String.format("%02d", hours)
            .plus(":")
            .plus(String.format("%02d", minutes))
            .plus(":" )
            .plus( String.format("%02d", seconds))
                )



    }
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
    fun updateData(
        jobListsUpdated: MutableList<Jobs>
    ) {

        //this.titles.clear()
        this.jobLists = jobListsUpdated
        //this.infoList.clear()
        notifyDataSetChanged()
    }
    class ListViewHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {

        var mContext :Context? = null
        var mActivity : AppCompatActivity? = null
        var titleLayout : ConstraintLayout = v.findViewById(R.id.title_layout)
        var jobNumber = v.findViewById<TextView>(R.id.jobtitle_no)
        var jobStatus = v.findViewById<TextView>(R.id.job_status)
        var textOn = v.findViewById<TextView>(R.id.textview_on)
        var jobStatusDate = v.findViewById<TextView>(R.id.job_status_date)
        var jobStatusTime = v.findViewById<TextView>(R.id.job_status_time)
        var map = v.findViewById<MapView>(R.id.mapView)
        //3
        init {
            v.setOnClickListener(this)
        }

        public fun insert(key:View, value : String): Pair<View, String> {
            val pair = Pair(key, value);
            return pair
        }
        override fun onClick(v: View) {
            Log.d("RecyclerView", "CLICK!")

            var options: ActivityOptions? = null
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                val p1 = insert(jobNumber as View,mContext?.getString(R.string.picture_transition_name)!!)
                val p2= insert(jobStatus as View,mContext?.getString(R.string.jobstatus_transition_name)!!)
                val options = ActivityOptions.makeSceneTransitionAnimation((mContext as Activity)!!,p1,p2)
                val intent = Intent(mContext, CompleteActivity::class.java)
                intent.putExtra("jobID",jobNumber.text.toString().toInt() )
                intent.putExtra("status",jobStatus.text )
                ( mContext as Activity)?.startActivityForResult(intent, 3333, options!!.toBundle()!!)
            }
        }

        companion object {
            //5
            private val PHOTO_KEY = "PHOTO"
        }

    }

    private fun setMap(holder: ListViewHolder, position: Int) {

            holder.map.onCreate(savedInstanceState)
            holder.map.onResume()
            holder.map.getMapAsync(OnMapReadyCallback { googleMap ->
                this.map = googleMap
                this.map!!.setMapType(GoogleMap.MAP_TYPE_NORMAL)
                MapsInitializer.initialize(context)
                if (ActivityCompat.checkSelfPermission(
                        this.context!!,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) !== PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this.context!!,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) !== PackageManager.PERMISSION_GRANTED
                ) {

                    return@OnMapReadyCallback
                }

                //this.map!!.setMyLocationEnabled(true)
                this.map!!.setMapType(GoogleMap.MAP_TYPE_NORMAL)
                val builder = LatLngBounds.Builder()
                val pickupLoc =Location("pickup")
                with (pickupLoc){
                    latitude = jobLists.get(position).pickupLocation!!.lat!!
                    longitude = jobLists.get(position).pickupLocation!!.lon!!
                }
                val deliveryLoc=Location("delivery")
                with (deliveryLoc){
                    latitude = jobLists.get(position).deliveryLocation!!.lat!!
                    longitude = jobLists.get(position).deliveryLocation!!.lon!!
                }
                val origin = pickupLoc.latitude.toString().plus(",").plus(pickupLoc.longitude.toString())
                val destination = deliveryLoc.latitude.toString().plus(",").plus(deliveryLoc.longitude.toString())
               // fetchDirections(origin,destination,"")
                val deliveryLatLong =
                    LatLng(
                        deliveryLoc.latitude,
                        deliveryLoc.longitude
                    )
                val deliveryTitle =
                    "Delivery to : " + jobLists.get(position).deliveryLocation!!.name
                if (deliveryTitle != null) {
                    val deliveryMarkerOptions = MarkerOptions()
                        .title("To")
                        .icon(bitmapDescriptorFromVector(context!!,R.drawable.ic_marker))
                        .position(deliveryLatLong)
                    val dd: Marker = this.map!!.addMarker(deliveryMarkerOptions)
                    builder.include(deliveryMarkerOptions.position)
                    //dd.showInfoWindow();
                }
                val pickupLatLong =
                    LatLng(
                        pickupLoc.latitude,
                        pickupLoc.longitude
                    )


                val pickupTitle = "Pickup from : " + jobLists.get(position).pickupLocation!!.name
                if (pickupTitle != null) {
                    val pickupMarkerOptions = MarkerOptions()
                        .title("From")
                        .icon(bitmapDescriptorFromVector(context!!,R.drawable.ic_marker))
                        .position(pickupLatLong)
                    builder.include(pickupMarkerOptions.position)
                    val mm: Marker = this.map!!.addMarker(pickupMarkerOptions)
                    // mm.showInfoWindow();
                }



                 val builderBounds = LatLngBounds.Builder()
                builderBounds.include(pickupLatLong)
                builderBounds.include(deliveryLatLong)
                val bounds = builder.build()
                val width = context!!.resources.displayMetrics.widthPixels
                val height = context!!.resources.displayMetrics.heightPixels
                val padding =
                    (width * 0.25).toInt() // offset from edges of the map 10% of screen
                val cu =
                    CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding)

                this.map!!.animateCamera(cu);
               //this.map!!.setMyLocationEnabled(true)
                //this.map!!.animateCamera(CameraUpdateFactory.newLatLngZoom(pickupLatLong, 10.5f))
                this.map!!.setOnMapClickListener(OnMapClickListener {
                    val intent = Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(
                            "http://maps.google.com/maps?saddr=" + pickupLoc.latitude
                                .toString() + "," + pickupLoc.longitude
                                .toString() + "&daddr=" + deliveryLoc.longitude
                                .toString() + "," + deliveryLoc.longitude
                        )
                    )
                    context.startActivity(intent)
                })
            })
    }

    fun fetchDirections(origin:String,destination:String,apiKey:String){
        CoroutineScope(Dispatchers.IO).launch {
            //  val response : Result<String> = repository.getDirectionsString(origin,destination)
            val response  = repository.getDirections(origin,destination,mHomeScreenActivity.getString(R.string.google_directionsapi_key))
            when(response) {
                is Result.Success ->withContext(Dispatchers.Main){setPolylinesAndMarkers((response.data))}
                // is Result.Success -> _directions.postValue(parseDirectionResult(response.data))
                is Result.Error -> (response.exception.toString())
            }
        }

    }

    fun setPolylinesAndMarkers(mDirections : HomeDirections){
        if((mDirections != null) && (this.map!! != null)) {
            var polyline: LatLng
            val mPolylineOptions = PolylineOptions()
            val builder = LatLngBounds.Builder()
            if(mDirections.routes.size == 0){
                return
            }
            val selectedRoute = mDirections.routes.get(0)
            with(selectedRoute) {

                for (indexLegs in 0..legs.size.minus(1)) {
                    //Add Moarkers for Star & End Location
                    with(legs[indexLegs]) {

                        for (indexSteps in 0..legs[indexLegs].steps.size.minus(1)) {
                            with(legs[indexLegs].steps[indexSteps]) {
                                //Add to Polylin Options
                                mPolylineOptions.add(LatLng(startLocation.lat, startLocation.lng))
                                mPolylineOptions.add(LatLng(endLocation.lat, endLocation.lng))

                                /*//Add to Bounds
                                builder.include(LatLng(startLocation.lat, startLocation.lng))
                                builder.include(LatLng(endLocation.lat, endLocation.lng))*/
                            }
                        }
                    }
                }
            }
            //Draw Polyline
            val polylines = this.map!!.addPolyline(mPolylineOptions)
            polylines.color = android.graphics.Color.RED
            polylines.isGeodesic = true
            polylines.width = 12F

            //Include in the dispaly screen
            /*with(builder.build()) {

                val width = context!!.resources.displayMetrics.widthPixels
                val height = context.resources.displayMetrics.heightPixels
                val padding =
                    (width * 0.01).toInt() // offset from edges of the map 10% of screen
                val cu =
                    CameraUpdateFactory.newLatLngBounds(this, width, height, padding)
               // map!!.isMyLocationEnabled = true
               // map!!.animateCamera(CameraUpdateFactory.newLatLngZoom(pickupLatLong, 10.5f))
            }*/

        }

    }

    private fun bitmapDescriptorFromVector(context: Context, @DrawableRes vectorDrawableResourceId: Int): BitmapDescriptor? {
        val background =
            ContextCompat.getDrawable(context, R.drawable.ic_marker)
        background!!.setBounds(0, 0, background.intrinsicWidth, background.intrinsicHeight)
        val vectorDrawable = ContextCompat.getDrawable(context, vectorDrawableResourceId)
        vectorDrawable!!.setBounds(
            23,
            23,
            vectorDrawable.intrinsicWidth + 23,
            vectorDrawable.intrinsicHeight + 23
        )
        val bitmap = Bitmap.createBitmap(
            background.intrinsicWidth,
            background.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
       // background.draw(canvas)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

}