package com.aotomot.ezymovr.adapters

import android.app.Activity
import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.aotomot.ezymovr.AppConstants
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.models.Jobs
import com.aotomot.ezymovr.ui.CompleteActivity
import com.aotomot.ezymovr.utils.AutoUpdatableAdapter
import kotlinx.android.synthetic.main.calender_list_item.view.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import android.util.Pair
import com.aotomot.ezymovr.ui.InProgressDetailsActivity


class CalenderListAdapter(private val mActivity: AppCompatActivity,
                          private var mAlerts: MutableList<Jobs>?
) :RecyclerView.Adapter<CalenderListAdapter.ListViewHolder>(),
    AutoUpdatableAdapter {
    private val mRandom = Random()
    private val imageWidth = Resources.getSystem().displayMetrics.widthPixels / 2

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        return ListViewHolder(
            LayoutInflater.from(mActivity).inflate(R.layout.calender_list_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return mAlerts!!.size
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {

        val dateCreated : Date = Date(mAlerts?.get(position)?.createdDate!!)

        val formatTime = SimpleDateFormat("hh:mmaa")
        val formatDate = SimpleDateFormat("d MMM")

        val dt = formatTime.format(dateCreated)
        val dtDate = formatDate.format(dateCreated)

        with(holder){
            context = mActivity
            var strDate : Date? = null
            var jobDate : Date? = null
            val sdf = SimpleDateFormat(context.getString(R.string.sdf_string))
            val joinDatesdf = SimpleDateFormat(context.getString(R.string.start_date_12hr_sdf))
            try {
                strDate = sdf.parse(AppConstants.Epoch2DateStringTimesss(mAlerts?.get(position)?.endDate.toString())!!)
                jobDate = joinDatesdf.parse(AppConstants.Epoch2DateStringTimeStartDate12Hr(mAlerts?.get(position)?.startDate.toString())!!)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            val date =  Calendar.getInstance()
            date.time = strDate


             dateDay.text = date.get(Calendar.DAY_OF_MONTH).toString()
            dateWeekDay.text = date.getDisplayName(Calendar.DAY_OF_WEEK,Calendar.SHORT,Locale.ENGLISH)
            jobNumber.text = "Job #".plus(mAlerts?.get(position)!!.id.toString())
            jobTime.text = (joinDatesdf.format(jobDate))
            jobID =  mAlerts?.get(position)!!.id

            jobTimer = mAlerts?.get(position)!!.timer.timer.toString()
            if (mAlerts?.get(position)!!.startDateActual.compareTo(0).equals(0)
                && !mAlerts?.get(position)!!.completed
                && strDate != null
                && Date().before(strDate) && mAlerts?.get(position)!!.accepted) {
                //Accepted
                datelayout.setBackgroundColor(context.getColor(R.color.accepted))
                jobStatus.text = "Accepted"
                jobStatus.setTextColor(context.getColor(R.color.accepted))
            }else if (mAlerts?.get(position)!!.completed) {
                //complete
                datelayout.setBackgroundColor(context.getColor(R.color.completed))
                jobStatus.text = "Completed"
                jobStatus.setTextColor(context.getColor(R.color.completed))
            } else if (!mAlerts?.get(position)!!.startDateActual.compareTo(0).equals(0)) {
                //in-progress
                datelayout.setBackgroundColor(context.getColor(R.color.inprogress))
                jobStatus.text = "In-progress"
                jobStatus.setTextColor(context.getColor(R.color.inprogress))
            } else {
                if (mAlerts?.get(position)?.endDate != null && !mAlerts?.get(position)!!.endDate.compareTo(0).equals(0) && Date().after(strDate)) {
                    //expired
                    datelayout.setBackgroundColor(context.getColor(R.color.expired))
                    jobStatus.text = "Expired"
                    jobStatus.setTextColor(context.getColor(R.color.expired))
                } else {
                    //upcoming
                    datelayout.setBackgroundColor(context.getColor(R.color.upcoming))
                    jobStatus.text = "Upcoming"
                    jobStatus.setTextColor(context.getColor(R.color.upcoming))
                }
            }


        }

    }
    class ListViewHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {


       val datelayout = v.date_layout
        val dateDay = v.date_day
        val dateWeekDay = v.date_week_day
        val jobNumber = v.job_no
        val jobStatus = v.jobStatus
        val jobTime = v.jobtime_text
        var jobID : Int = 0
        var jobTimer = ""

        lateinit var context : Context

        init {
            v.setOnClickListener(this)
        }
        public fun insert(key:View, value : String): Pair<View, String> {
            val pair = Pair(key, value);
           return pair
        }
        override fun onClick(v: View) {
            var options: ActivityOptions? = null
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                if(jobStatus.text.equals("In-progress") || jobStatus.text.equals("Upcoming") || jobStatus.text.equals("Accepted")) {
                    val p1 = insert(jobNumber as View,context?.getString(R.string.picture_transition_name)!!)
                    val p2= insert(jobStatus as View,context?.getString(R.string.jobstatus_transition_name)!!)
                    val options = ActivityOptions.makeSceneTransitionAnimation((context as Activity)!!,p1,p2)
                    val intent = Intent(context, InProgressDetailsActivity::class.java)
                    intent.putExtra("jobID",jobID )
                    intent.putExtra("status",jobStatus.text.toString() )
                    intent.putExtra("timer",jobTimer )
                    ( context as Activity)?.startActivityForResult(intent, 3333, options!!.toBundle()!!)
                }else {
                    val p1 =
                        insert(jobNumber as View, context.getString(R.string.picture_transition_name))
                    val p2 =
                        insert(jobStatus as View, context.getString(R.string.jobstatus_transition_name))
                    val p3 =
                        insert(jobTime as View, context.getString(R.string.jobstime_transition_name))
                    val options =
                        ActivityOptions.makeSceneTransitionAnimation((context as Activity)!!, p1, p2)
                    val intent = Intent(context, CompleteActivity::class.java)
                    intent.putExtra("jobID", jobID)
                    intent.putExtra("status", jobStatus.text.toString())
                    (context as Activity)?.startActivityForResult(intent, 3333, options!!.toBundle()!!)
                }

            }
        }
    }

    fun update (mUpdatedAlerts: MutableList<Jobs>){
        this.mAlerts = mUpdatedAlerts
        notifyDataSetChanged()
    }

}