package com.aotomot.ezymovr.adapters

import android.Manifest
import android.app.Activity
import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.provider.MediaStore
import android.util.Log
import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.aotomot.ezymovr.CustomAlert.AlertDialog
import com.aotomot.ezymovr.CustomAlert.OnItemClickListener
import com.aotomot.ezymovr.R
import com.aotomot.ezymovr.Repository.CommonRepository
import com.aotomot.ezymovr.WebService.WebServiceAPIFactory
import com.aotomot.ezymovr.extFunctions.domain
import com.aotomot.ezymovr.extFunctions.getAppSharedPrefs
import com.aotomot.ezymovr.extFunctions.gone
import com.aotomot.ezymovr.extFunctions.visible
import com.aotomot.ezymovr.models.Jobs
import com.aotomot.ezymovr.ui.InProgressDetailsActivity
import com.aotomot.ezymovr.ui.SendGalleryPhotoActivity
import com.aotomot.ezymovr.ui.ui.home.HomeViewModel
import com.aotomot.ezymovr.utils.AutoUpdatableAdapter
import com.github.gcacace.signaturepad.views.SignaturePad
import com.google.android.gms.maps.GoogleMap
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*


class InProgressJobsListAdapter(
    private var jobLists: MutableList<Jobs>,
    private val context: Context?,
    private val mHomeScreenActivity: AppCompatActivity,
    private val savedInstanceState: Bundle?,
    private val mViewModel: HomeViewModel?
) : RecyclerView.Adapter<InProgressJobsListAdapter.ListViewHolder>(),
    AutoUpdatableAdapter {

    private var jobPosition: Int = 0
    private lateinit var holderView: InProgressJobsListAdapter.ListViewHolder
    private var startTime = 0L
    private val myHandler = Handler()
    var timeInMillies = 0L
    var timeSwap = 0L
    var finalTime = 0L
    private var is_paused = false
    var sdf = SimpleDateFormat("dd MMM yyyy h:mm a")
    var map: GoogleMap? = null

    private val repository : CommonRepository =
        CommonRepository(WebServiceAPIFactory.webServiceApi_directions, context,mHomeScreenActivity.getAppSharedPrefs().domain())
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ListViewHolder {
        return ListViewHolder(
            LayoutInflater.from(context).inflate(R.layout.inprogresslist_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return jobLists.size
    }


    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
    fun updateData(
        jobListsUpdated: MutableList<Jobs>
    ) {

        //this.titles.clear()
        this.jobLists = jobListsUpdated
        //this.infoList.clear()
        notifyDataSetChanged()
    }
    class ListViewHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {

        var mContext :Context? = null
        var mActivity : AppCompatActivity? = null
        var startLayout : ConstraintLayout = v.findViewById(R.id.startLayout)
        var stopLayout : ConstraintLayout = v.findViewById(R.id.stopLayout)
        var pauseLayout : ConstraintLayout = v.findViewById(R.id.pauseLayout)
        var sign_layout : ConstraintLayout = v.findViewById(R.id.signLayout)
        var saveSign = v.findViewById<TextView>(R.id.saveText)
        var jobNumber = v.findViewById<TextView>(R.id.job_number)
        var jobStatus = v.findViewById<TextView>(R.id.job_status_text)
        var timerText = v.findViewById<TextView>(R.id.in_progresss_timer)
        lateinit var mViewModel: HomeViewModel

         //3
        init {
            v.setOnClickListener(this)
        }

        public fun insert(key:View, value : String): Pair<View, String> {
            val pair = Pair(key, value);
            return pair
        }
        override fun onClick(v: View) {
            Log.d("RecyclerView", "CLICK!")

            var options: ActivityOptions? = null
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                val p1 = insert(jobNumber as View,mContext?.getString(R.string.picture_transition_name)!!)
                val p2= insert(jobStatus as View,mContext?.getString(R.string.jobstatus_transition_name)!!)
                val options = ActivityOptions.makeSceneTransitionAnimation((mContext as Activity)!!,p1,p2)
                val intent = Intent(mContext, InProgressDetailsActivity::class.java)
                intent.putExtra("jobID",jobNumber.text.toString().toInt() )
                intent.putExtra("status",jobStatus.text )
                intent.putExtra("timer",timerText.text )
                ( mContext as Activity)?.startActivityForResult(intent, 3333, options!!.toBundle()!!)
            }
        }

        companion object {
            //5
            private val PHOTO_KEY = "PHOTO"
        }

    }

    private val listofTimers = mutableListOf<Runnable>()
    private val updateTimerMethod: Runnable = object : Runnable {
        override fun run() {
            timeInMillies = SystemClock.uptimeMillis() - startTime
            finalTime = timeSwap + timeInMillies
            /**
             * display from backend to the timer
             */
            val seconds = (finalTime / 1000).toInt() % 60
            val minutes = (finalTime / (1000 * 60) % 60).toInt()
            val hours = (finalTime / (1000 * 60 * 60)).toInt()
            holderView.timerText.text = (
                String.format(
                    "%02d",
                    hours
                ) + ":" + String.format("%02d", minutes) + ":" + String.format(
                    "%02d",
                    seconds
                )
            )
            myHandler.postDelayed(this, 0)
        }
    }
    fun getImageUri(inContext: Context, inImage: Bitmap): Uri? {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(inContext.contentResolver, inImage, "Title", null)
        return Uri.parse(path)
    }
    fun drawTextToBitmap(mContext: Context, bitmaps: Bitmap, mText: String): Bitmap? {
        return try {
            val resources = mContext.resources
            val scale = resources.displayMetrics.density
            var bitmap = bitmaps
            var bitmapConfig = bitmap.config
            // set default bitmap config if none
            if (bitmapConfig == null) {
                bitmapConfig = Bitmap.Config.ARGB_8888
            }
            // resource bitmaps are imutable,
            // so we need to convert it to mutable one
            bitmap = bitmap.copy(bitmapConfig, true)
            val canvas = Canvas(bitmap)
            // new antialised Paint
            val paint =
                Paint(Paint.ANTI_ALIAS_FLAG)
            // text color - #3D3D3D
            paint.color = Color.rgb(110, 110, 110)
            // text size in pixels
            paint.setTextSize((15 * scale) as Float)
            // text shadow
            paint.setShadowLayer(1f, 0f, 1f, Color.DKGRAY)
            // draw text to the Canvas center
            val bounds = Rect()
            paint.getTextBounds(mText, 0, mText.length, bounds)
            /**
             * bitmap.getWidth()-bounds.right-20 --> meaning get bitmap width then minus the bound right and add another space width from rightby -20
             * bitmap.getHeight()-bounds.top-40 --> meaning get bitmap height then minus the bound top and add another space height from bottom by -40
             */
            //            canvas.drawText("Signed : " +mText, bitmap.getWidth()-bounds.right-200, bitmap.getHeight()-bounds.top-50, paint);
            /**
             *
             * x = 15 --> meaning get the canvas start from x = 15
             * y = bitmap.getHeight()-bounds.top-40 --> meaning get bitmap height then minus the bound top and add another space height from bottom by -40
             */
            canvas.drawText(
                "Signed : $mText",
                15f,
                bitmap.height - bounds.top - 50.toFloat(),
                paint
            )
            bitmap
        } catch (e: Exception) {
            null
        }
    }
    var mPermission =
        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun onsaveTextClicked(id : Int, signaturePad : SignaturePad) {

        if (ContextCompat.checkSelfPermission(mHomeScreenActivity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) !== PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mHomeScreenActivity, mPermission,5)
        } else {
            var bitmap: Bitmap? = signaturePad.getSignatureBitmap()
            signaturePad.clear()
            val date =
                SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.getDefault())
                    .format(Date())
            bitmap = drawTextToBitmap(mHomeScreenActivity, bitmap!!, date)
            if (bitmap == null) {
                Toast.makeText(mHomeScreenActivity, "Please do the signature", Toast.LENGTH_SHORT).show()
            } else {
                val i = Intent(mHomeScreenActivity, SendGalleryPhotoActivity::class.java)
                i.putExtra("imageUri", getImageUri(mHomeScreenActivity, bitmap))
                i.putExtra("album", "no")
                i.putExtra("signature", true)
                i.putExtra("fromcomplete", false)
                i.putExtra("jobId", id.toLong())
                ContextCompat.startActivity(mHomeScreenActivity!!, i, null)
            }
        }
    }
    override fun onBindViewHolder(holder: InProgressJobsListAdapter.ListViewHolder, position: Int) {
        val itemLong = jobLists.get(position).endDate / 1000
        val d = Date(itemLong * 1000L)
        holderView = holder

        holder.mViewModel = mViewModel!!
        jobPosition = position
        holder.mContext = context
        holder.mActivity = mHomeScreenActivity
        holder.jobNumber.text = jobLists.get(position).id.toString()
        holder.jobStatus.text = "In-Progress"
        holder.saveSign.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                holder.sign_layout.gone()
                    mViewModel.completeJob(jobLists.get(jobPosition).id.toString())

                onsaveTextClicked(jobLists.get(jobPosition).id,(holder.sign_layout.findViewById(R.id.signature_pad)))
            }

        })
        val updateTimerMethod: Runnable = object : Runnable {
            override fun run() {
                timeInMillies = SystemClock.uptimeMillis() - startTime
                finalTime = timeSwap + timeInMillies
                /**
                 * display from backend to the timer
                 */
                val seconds = (finalTime / 1000).toInt() % 60
                val minutes = (finalTime / (1000 * 60) % 60).toInt()
                val hours = (finalTime / (1000 * 60 * 60)).toInt()
                holderView.timerText.text = (
                        String.format(
                            "%02d",
                            hours
                        ) + ":" + String.format("%02d", minutes) + ":" + String.format(
                            "%02d",
                            seconds
                        )
                        )
                myHandler.postDelayed(this, 0)
            }
        }

        if (jobLists.get(position).timer.running.equals("true")){
            val times = jobLists.get(position).timer.timer.split(":")
            /**CONVERT TIME TO SECOND*/
            /**CONVERT TIME TO SECOND */
            val second : Int =
                times[0].toInt() * 60 * 60 + times[1].toInt() * 60 + times[2]
                    .toInt()
            /**CONVERT TIME TO millisecond*/
            /**CONVERT TIME TO millisecond */
            val millisecond = second * 1000
            startTime = SystemClock.uptimeMillis().minus(millisecond)
            listofTimers.add(updateTimerMethod)
            listofTimers.add(jobPosition,updateTimerMethod)
            myHandler.postDelayed(listofTimers.get(jobPosition), 0)
            holder.startLayout.visibility = View.GONE
            holder.pauseLayout.visibility = View.VISIBLE
            holder.stopLayout.visibility = View.VISIBLE
        }else{
            holder.jobStatus.text ="Break"
            val times = jobLists.get(position).timer.timer.split(":")
            val mseconds = times[2].toInt()
            val mminutes = times[1].toInt()
            val mhours = times[0].toInt()
            holder.timerText?.text = (String.format("%02d", mhours)
                .plus(":")
                .plus(String.format("%02d", mminutes))
                .plus(":" )
                .plus( String.format("%02d", mseconds))
                    )
            //listofTimers.add(updateTimerMethod)
            if(!listofTimers.isEmpty()) {
                myHandler.removeCallbacks(listofTimers.get(jobPosition))
            }
            holder.startLayout?.visibility = View.VISIBLE
            holder.pauseLayout.visibility = View.GONE
            holder.stopLayout?.visibility = View.VISIBLE
        }


        holder.startLayout?.setOnClickListener(
            object : View.OnClickListener {
                override fun onClick(view: View) {
                    // AppConstants.TrackingEvent(AnalyticsConstant.job,AnalyticsConstant.pause, AnalyticsConstant.pauseJobClicked + jobList.getId())
                    AlertDialog.showDialogWithoutHeaderTwoButtonLaunchWake(
                        context,
                        "Please confirm you wish to start the timer.",
                        object : OnItemClickListener {
                            override fun onItemClick(o: Any?, position: Int) {
                                if (position == 0) {
                                    /*AppConstant.TrackingEvent(
                                        AnalyticsConstant.job,
                                        AnalyticsConstant.continueJob,
                                        AnalyticsConstant.continueJobClicked + jobList.getId()
                                    )*/
                                    is_paused = false
                                    /*NetworkManager.getInstance().startOrPauseJob(
                                        ModelManager.getInstance().getLogin().getAuthorization(),
                                        jobList.getId().toString() + "",
                                        true,
                                        ""
                                    )*/
                                    holder.mViewModel.startJob(jobLists[jobPosition].id.toString(),"")
                                    holder.startLayout.setVisibility(View.GONE)
                                    holder.stopLayout.setVisibility(View.VISIBLE)
                                    holder.pauseLayout.setVisibility(View.VISIBLE)
                                    //completeJobLayout.setVisibility(View.GONE);
                                    val timeText: String = jobLists[jobPosition].timer.timer
                                    val times =timeText.split(":").toTypedArray()

                                    /**CONVERT TIME TO SECOND */
                                    val second = times[0].toInt() * 60 * 60 + times[1]
                                        .toInt() * 60 + times[2].toInt()

                                    /**CONVERT TIME TO millisecond */
                                    val millisecond = second * 1000
                                    startTime = SystemClock.uptimeMillis() - millisecond
                                    myHandler.postDelayed(listofTimers.get(jobPosition), 0)
                                } else {
                                }
                            }
                        })
                }
            })
        holder.stopLayout?.setOnClickListener(
            object : View.OnClickListener {
                override fun onClick(view: View) {
                    holder.pauseLayout.setVisibility(View.GONE)
                    holder.startLayout.setVisibility(View.VISIBLE)
                    holder.stopLayout.setVisibility(View.VISIBLE)
                    //completeJobLayout.setVisibility(View.GONE);
                    is_paused = true
                    if (jobLists[jobPosition].timer.running.equals("false") ) {
                        /**
                         * dont call pause again because the job already in pause mode other wise it will be duplicate break
                         */
                    } else {
                       // loading.visible()

                        mViewModel.PauseJob(jobLists[jobPosition].id.toString(),"")
                        /*NetworkManager.getInstance().pauseTimer(
                            ModelManager.getInstance().getLogin().getAuthorization(),
                            jobList.getId().toString(),
                            false, ""
                        )*/
                    }
                    if(!listofTimers.isEmpty()) {
                        myHandler.removeCallbacks(listofTimers.get(jobPosition))
                    }
                    AlertDialog.showDialogWithHeaderTwoButton(
                        context,
                        "Is the job completed?",
                        "Press OK to submit",
                        object : OnItemClickListener {
                            override fun onItemClick(o: Any?, position: Int) {
                                if (position == 0) {
                                    holder.sign_layout.visible()
                                    /*NetworkManager.getInstance().doCompleteJob(
                                        ModelManager.getInstance().getLogin().getAuthorization(),
                                        jobList.getId().toString()
                                    )*/
                                    /* AppConstant.TrackingEvent(
                                         AnalyticsConstant.job,
                                         AnalyticsConstant.complete,
                                         AnalyticsConstant.completeJobClicked + jobList.getId()
                                     )*/
                                } else {
                                    //cancel do nothing
                                    holder.pauseLayout.setVisibility(View.GONE)
                                    holder.startLayout.setVisibility(View.VISIBLE)
                                    holder.stopLayout.setVisibility(View.VISIBLE)
                                    //completeJobLayout.setVisibility(View.GONE);
                                }
                            }
                        })
                }
            })
        holder.pauseLayout.setOnClickListener(
            object : View.OnClickListener {
                override fun onClick(view: View) {
                    /*AppConstant.TrackingEvent(
                        AnalyticsConstant.job,
                        AnalyticsConstant.pause,
                        AnalyticsConstant.pauseJobClicked + jobList.getId()
                    )*/
                    AlertDialog.showDialogWithoutHeaderTwoButtonLaunchWakeBreak(
                        context,
                        "Are you taking break?",
                        object : OnItemClickListener {
                            override fun onItemClick(o: Any?, position: Int) {
                                if (position == 0) {
                                    is_paused = true


                                    //completeJobLayout.setVisibility(View.GONE);
                                    mViewModel!!.PauseJob(jobLists.get(position).id.toString(),AlertDialog.etName.text.toString())
                                    myHandler.removeCallbacks(listofTimers.get(jobPosition))
                                    /*if (AlertDialog.etName.getText().length() > 0) {
                                        NetworkManager.getInstance().startOrPauseJob(
                                            ModelManager.getInstance().getLogin().getAuthorization(),
                                            jobList.getId().toString() + "",
                                            false,
                                            AlertDialog.etName.getText().toString()
                                        )
                                    } else {
                                        NetworkManager.getInstance().startOrPauseJob(
                                            ModelManager.getInstance().getLogin().getAuthorization(),
                                            jobList.getId().toString() + "",
                                            false,
                                            ""
                                        )
                                    }*/
                                    if (AlertDialog.imm.isAcceptingText()) { // verify if the soft keyboard is open
                                        /*AlertDialog.imm.hideSoftInputFromWindow(
                                            quantumMainActivity.getCurrentFocus().getWindowToken(), 0
                                        )*/
                                    }
                                } else {
                                    if (AlertDialog.imm.isAcceptingText()) { // verify if the soft keyboard is open
                                        /*AlertDialog.imm.hideSoftInputFromWindow(
                                            quantumMainActivity.getCurrentFocus().getWindowToken(), 0
                                        )*/
                                    }
                                }
                            }
                        })
                    if (is_paused) {
                        holder.pauseLayout.setVisibility(View.GONE)
                        holder.startLayout.setVisibility(View.VISIBLE)
                        holder.stopLayout.setVisibility(View.VISIBLE)
                    }
                }
            })


    }
}